<?php 
/*
 * @Author       : ZengHao
 * @Date         : 2023-12-23 14:14:26
 * @LastEditors  : ZengHao
 * @LastEditTime : 2024-01-11 12:49:54
 * @FilePath     : /sidebar.php
 * @Description  : 
 * Copyright 2023 www.exehub.com, All Rights Reserved. 
 * 2023-12-23 14:14:26
 */
if (!el_is_show_sidebar() || wp_is_mobile()) {
	return;
}?>
<div class="el-sidebar">
	<?php
	if (function_exists('dynamic_sidebar')) {
		dynamic_sidebar('all_sidebar_top');
		if (is_home()) {
			dynamic_sidebar('home_sidebar');
		} elseif (is_category()||is_tax( 'topics' )) {
			dynamic_sidebar('cat_sidebar');
		} elseif (is_tag()) {
			dynamic_sidebar('tag_sidebar');
		} elseif (is_search()) {
			dynamic_sidebar('search_sidebar');
		} elseif (is_single()) {
			dynamic_sidebar('single_sidebar');
		} elseif (is_page_template('pages/sidebar.php')) {
			dynamic_sidebar('page_sidebar');
		}
		dynamic_sidebar('all_sidebar_bottom');
	}
	?>
</div>