<?php
/*
 *                   江城子 . 程序员之歌
 * 
 *               十年生死两茫茫，写程序，到天亮。
 *                   千行代码，Bug何处藏。
 *               纵使上线又怎样，朝令改，夕断肠。
 * 
 *               领导每天新想法，天天改，日日忙。
 *                   相顾无言，惟有泪千行。
 *               每晚灯火阑珊处，夜难寐，加班狂。
 * 
 */

/*
 * @Author       : ZengHao
 * @Date         : 2023-12-26 16:28:07
 * @LastEditors  : ZengHao
 * @LastEditTime : 2024-01-02 16:06:25
 * @FilePath     : \inc\inc.php
 * @Description  : 
 * Copyright 2023 www.exehub.net, All Rights Reserved. 
 * 2023-12-26 16:28:07
 */

//定义常量
define('EL_ENV', 'DEV'); // 是否是本地开发环境，用来区别一些输出
define('NEXT_WEAK', 6800); 
define('CACHE_TIME', 1800); // 缓存时间，缓存半小时
define('EL_THEME_URI', get_template_directory_uri());
define('EL_ASSETS', EL_THEME_URI . '/assets/');
//本主题的路径
define('EL_THEME_DIR', dirname(__DIR__) . '/');
//主题设置
define('EL_OPT', "el_options");
define('EL_CACHE', "el_theme_cache");
define('AJAX_URL', esc_url(admin_url('admin-ajax.php')));
define('LAZY_COVER',_opz('lazyload_def_cover')?_opz('lazyload_def_cover'):EL_ASSETS.'img/def_cover.webp');

//主题的版本号
$theme_data = wp_get_theme();
$_version = $theme_data['Version'];
$_themeName = get_template();
if (!defined('EL_VERSION')) {
    define('EL_VERSION', $_version);
}
if (!defined('EL_NAME')) {
    define('EL_NAME', $_themeName);
}

//php版本判断
if (PHP_VERSION_ID < 70000) {
    wp_die('PHP 版本过低，请先升级php版本到7.0及以上版本，当前php版本为：' . PHP_VERSION);
}

//载入文件
$require_once = array(
    'inc/core/core.php',
    'inc/action/action.php',
    'inc/codestar-framework/autoload.php',
    'inc/options/options.php',
    'inc/functions/functions.php',
    '/inc/widgets/widgets.php',
);

foreach ($require_once as $require) {
    require get_theme_file_path('/' . $require);
}


/**
 * @func: _opz
 * @description: 获取及设置主题配置参数
 * @param {*} $name
 * @param {*} $default
 * @param {*} $subname
 * @return {*}
 * @example: 
 * 20231212:开启主题函数设置函数
 * 20240102:增加缓存机制，缓存一个小时
 */
function _opz($name, $default = false, $subname = '')
{
    //声明静态变量，加速获取
    static $options = null;
    // $options_cache = wp_cache_get(EL_OPT,EL_CACHE);
    $options_cache = ECache::get(EL_OPT);
    if (false === $options_cache  || $options === null ) {
        $options = get_option(EL_OPT);
        // echo '我进来了';
        ECache::set(EL_OPT,$options);
    }
    $options = $options_cache?$options_cache:$options;
    // echo("目前的设置");
    // print_r($options);


    // if ($options === null) {
    //     $options = get_option(EL_OPT);
    // }

    if (isset($options[$name])) {
        if ($subname) {
            return isset($options[$name][$subname]) ? $options[$name][$subname] : $default;
        } else {
            return $options[$name];
        }
    }
    return $default;
}


/**
 * @func: el_res
 * @description: 全局返回结果格式化
 * @param {*} $error 是否错误 1-是，0-否
 * @param {*} $msg 提示消息
 * @param {*} $data 数据体
 * @return {*}
 * @example: 
 */
function el_res($error, $msg = '', $data = array())
{
    return json_encode(array(
        'error' => $error,
        'msg' => $msg,
        'data' => $data
    ));
}

// 缓存类
class ECache
{
    // 设置缓存
    /**
     * @func: 
     * @description: 设置缓存
     * @param {*} $key key值
     * @param {*} $value 缓存值
     * @param {*} $expire 过期时间，默认600
     * @param {*} $group 缓存组，默认el_theme_cache
     * @return {*}
     * @example: 
     */    
    public static function  set($key, $value, $group = EL_CACHE,$expire = YEAR_IN_SECONDS)
    {
        return wp_cache_set($key, $value, $group, $expire);
    }

    // 获取缓存
    public static function get($key, $group = EL_CACHE)
    {
        return wp_cache_get($key, $group);
    }
    
    // 清除缓存
    public static function delete($key, $group = EL_CACHE)
    {
        return wp_cache_delete($key, $group);
    }

    // TODO:刷新缓存
    public static function refresh($key, $group = EL_CACHE)
    {

    }
}