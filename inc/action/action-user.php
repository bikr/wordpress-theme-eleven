<?php 
/*
 * @Author       : ZengHao
 * @Date         : 2024-01-08 20:03:05
 * @LastEditors  : ZengHao
 * @LastEditTime : 2024-01-08 20:03:06
 * @FilePath     : /inc/action/action-user.php
 * @Description  : 
 * Copyright 2024 www.exehub.com, All Rights Reserved. 
 * 2024-01-08 20:03:05
 */
/**
 * @func: 
 * @description: 前端修改用户时调用这个ajax
 * @return {*}
 * @example: 
 */
function el_ajax_user_edit_datas()
{
    $cuid = get_current_user_id();

    if (!$cuid) {
        echo (json_encode(array('error' => 1, 'ys' => 'danger', 'msg' => '权限不足')));
        exit;
    }

    //用户名判断
    el_ajax_username_judgment('name', true);
    $_POST['name'] = trim($_POST['name']);
    $_POST['url']  = trim($_POST['url']);

    if (!empty($_POST['desc'])) {
        if (el_new_strlen($_POST['desc']) < 4) {
            echo (json_encode(array('error' => 1, 'ys' => 'danger', 'msg' => '签名过短，不能低于4个字符')));
            exit();
        }
        if (el_new_strlen($_POST['desc']) > 60) {
            echo (json_encode(array('error' => 1, 'ys' => 'danger', 'msg' => '签名过长，不能超过60个字符')));
            exit();
        }
    }

    //昵称、签名合规性判断
    if (_opz('audit_user_desc')) {
        ZibAudit::ajax_text($_POST['desc'] . $_POST['name'], '昵称或签名');
    }

    if ($_POST['url'] && (!el_is_url($_POST['url']))) {
        echo (json_encode(array('error' => 1, 'ys' => 'danger', 'msg' => '网址格式错误')));
        exit();
    }

    if ($_POST['url'] && !$_POST['url_name']) {
        echo (json_encode(array('error' => 1, 'ys' => 'danger', 'msg' => '请输入个人网站名称')));
        exit();
    }

    if ($_POST['url_name'] && !$_POST['url']) {
        echo (json_encode(array('error' => 1, 'ys' => 'danger', 'msg' => '请输入个人网站链接')));
        exit();
    }

    if ($_POST['address'] && el_new_strlen($_POST['address']) > 50) {
        echo (json_encode(array('error' => 1, 'ys' => 'danger', 'msg' => '居住地格式错误')));
        exit();
    }

    if ($_POST['weibo'] && (!el_is_url($_POST['weibo']) || el_new_strlen($_POST['weibo']) > 50)) {
        echo (json_encode(array('error' => 1, 'ys' => 'danger', 'msg' => '微博格式错误')));
        exit();
    }

    if ($_POST['github'] && (!el_is_url($_POST['github']) || el_new_strlen($_POST['github']) > 50)) {
        echo (json_encode(array('error' => 1, 'ys' => 'danger', 'msg' => 'GitHub格式错误')));
        exit();
    }

    if ($_POST['qq'] && !preg_match("/^[1-9]\d{4,16}$/", $_POST['qq'])) {
        echo (json_encode(array('error' => 1, 'ys' => 'danger', 'msg' => 'QQ格式错误')));
        exit();
    }

    if ($_POST['weixin'] && el_new_strlen($_POST['weixin']) > 20) {
        echo (json_encode(array('error' => 1, 'ys' => 'danger', 'msg' => '微信字数过长，限制在30字内')));
        exit();
    }

    if ($_POST['desc']) {
        update_user_meta($cuid, 'description', trim($_POST['desc']));
    }

    if ($_POST['qq']) {
        update_user_meta($cuid, 'qq', trim($_POST['qq']));
    }

    if ($_POST['weixin']) {
        update_user_meta($cuid, 'weixin', trim($_POST['weixin']));
    }

    if ($_POST['weibo']) {
        update_user_meta($cuid, 'weibo', trim($_POST['weibo']));
    }

    if ($_POST['github']) {
        update_user_meta($cuid, 'github', trim($_POST['github']));
    }

    if ($_POST['url_name']) {
        update_user_meta($cuid, 'url_name', trim($_POST['url_name']));
    }

    if ($_POST['gender']) {
        update_user_meta($cuid, 'gender', trim($_POST['gender']));
    }

    if ($_POST['address']) {
        update_user_meta($cuid, 'address', trim($_POST['address']));
    }

    if ($_POST['privacy']) {
        update_user_meta($cuid, 'privacy', trim($_POST['privacy']));
    }

    $datas = array('ID' => $cuid);
    if ($_POST['url']) {
        $datas['user_url'] = $_POST['url'];
    }

    if ($_POST['name']) {
        $datas['display_name'] = $_POST['name'];
        $datas['nickname']     = $_POST['name'];
    }

    $status = wp_update_user($datas);

    if (is_wp_error($status)) {
        echo (json_encode(array('error' => 1, 'ys' => 'danger', 'msg' => $status->get_error_message())));
        exit();
    }

    echo (json_encode(array('error' => 0, 'ys' => '', 'msg' => '用户资料修改成功')));
    exit();
}
// add_action('wp_ajax_user_edit_datas', 'el_ajax_user_edit_datas');

/**
 * @func: 
 * @description: 修改用户头像
 * @return {*}
 * @example: 
 */
function el_ajax_user_upload_avatar()
{
    $cuid = get_current_user_id();
    if (!$cuid) {
        echo (json_encode(array('error' => 1, 'ys' => 'danger', 'msg' => '权限不足')));
        exit;
    }
    if (!wp_verify_nonce($_POST['upload_avatar_nonce'], 'upload_avatar')) {
        echo (json_encode(array('error' => 1, 'ys' => 'danger', 'msg' => '安全验证失败，请稍候再试')));
        exit();
    }

    if (empty($_FILES['file'])) {
        echo (json_encode(array('error' => 1, 'ys' => 'danger', 'msg' => '请选择图像')));
        exit();
    }

    $img_id = el_php_upload();
    if (!empty($img_id['error'])) {
        echo (json_encode(array('error' => 1, 'ys' => 'danger', 'msg' => $img_id['msg'])));
        exit();
    }

    $image_url = wp_get_attachment_image_src($img_id, 'thumbnail');
    $lao_id    = get_user_meta($cuid, 'custom_avatar_id', true);
    if ($lao_id) {
        wp_delete_attachment($lao_id, true);
    }
    update_user_meta($cuid, 'custom_avatar_id', $img_id);
    update_user_meta($cuid, 'custom_avatar', $image_url[0]);

    do_action('user_save_custom_avatar', $cuid, $img_id, $image_url[0]);

    echo (json_encode(array('error' => 0, 'hide_modal' => true, 'msg' => '头像修改成功', 'replace_img' => '.avatar-id-' . $cuid, 'img_id' => $img_id, 'url' => $image_url[0])));
    exit();
}
// add_action('wp_ajax_user_upload_avatar', 'el_ajax_user_upload_avatar');