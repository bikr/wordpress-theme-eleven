<?php
/*
 * @Author       : ZengHao
 * @Date         : 2024-01-08 19:46:10
 * @LastEditors  : ZengHao
 * @LastEditTime : 2024-01-17 23:44:20
 * @FilePath     : /inc/action/action.php
 * @Description  : 
 * Copyright 2024 www.exehub.com, All Rights Reserved. 
 * 2024-01-08 19:46:10
 */

 $actions = array(
    'post',
    'comment'
);

foreach ($actions as $action) {
    $path = 'inc/action/action-' . $action . '.php';
    require get_theme_file_path($path);
}
