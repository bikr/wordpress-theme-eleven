<?php
/*
 * @Author       : ZengHao
 * @Date         : 2024-02-23 16:27:10
 * @LastEditors  : ZengHao
 * @LastEditTime : 2024-02-25 18:12:33
 * @FilePath     : /inc/options/metabox-options.php
 * @Description  : 
 * Copyright 2024 www.exehub.net, All Rights Reserved. 
 * 2024-02-23 16:27:10
 */

 
 //页面扩展
// CSF::createMetabox('page_main', array(
//     'title'     => '页面扩展',
//     'post_type' => array('page'),
//     'context'   => 'side',
//     'data_type' => 'unserialize',
// ));
// CSF::createSection('page_main', array(
//     'fields' => array(
//         array(
//             'id'      => 'show_layout',
//             'type'    => 'radio',
//             'title'   => '显示布局',
//             'default' => '',
//             'options' => array(
//                 ''              => '跟随主题',
//                 'no_sidebar'    => '无侧边栏',
//                 'sidebar_left'  => '侧边栏靠左',
//                 'sidebar_right' => '侧边栏靠右',
//             ),
//         ),
//     ),
// ));

// CSF::createMetabox('archives_templates', array(
//     'title'          => '归档列表页面设置',
//     'post_type'      => 'page',
//     'context'        => 'side',
//     'data_type'      => 'unserialize',
//     'page_templates' => 'pages/Archive.php', // Spesific page template
// ));

// CSF::createSection('archives_templates', array(
//     'fields' => array(
//         array(
//             'title'      => '页面标题',
//             'subtitle'   => '正文最上方显示',
//             'id'         => 'page_archives_title',
//             'default'    => '文章归档',
//             'type'       => 'text',
//         ),
//         array(
//             'title'      => '页面描述',
//             'subtitle'   => '正文最上方显示',
//             'id'         => 'page_archives_desc',
//             'default'    => '不知道写什么描述',
//             'type'       => 'text',
//         ),
//     )
// ));

//链接列表模板
// function el_cfs_link_category()
// {
//     $options_linkcats     = array();
//     $options_linkcats[0]  = '全部选择';
//     $options_linkcats_obj = get_terms(['taxonomy' => 'link_category'], ['hide_empty' => false]);
//     foreach ($options_linkcats_obj as $tag) {
//         $options_linkcats[$tag->term_id] = $tag->name;
//     }
//     return $options_linkcats;
// }

// CSF::createMetabox('links_templates', array(
//     'title'          => '链接列表页面设置',
//     'post_type'      => 'page',
//     'context'        => 'side',
//     'data_type'      => 'unserialize',
//     'page_templates' => 'pages/Links.php', // Spesific page template
// ));

// CSF::createSection('links_templates', array(
//     'fields' => array(
//         array(
//             'type'    => 'submessage',
//             'style'   => 'info',
//             'content' => '用于显示链接的页面，支持链接提交模块，可用于创建‘友情链接’、‘链接导航’等页面 ',
//         ),
//         array(
//             'id'      => 'page_links_orderby',
//             'title'   => '链接排序方式',
//             'default' => 'name',
//             'type'    => 'select',
//             'options' => array(
//                 'name'    => __('名称排序'),
//                 'updated' => __('更新时间'),
//                 'rating'  => __('链接评分'),
//                 'rand'    => __('随机排序'),
//             ),
//         ),
//         array(
//             'id'       => 'page_links_order',
//             'title'    => ' ',
//             'subtitle' => ' ',
//             'default'  => 'ASC',
//             'class'    => 'compact',
//             'inline'   => true,
//             'type'     => 'radio',
//             'options'  => array(
//                 'ASC'  => __('升序'),
//                 'DESC' => __('降序'),
//             ),
//         ),
//         array(
//             'title'   => '限制数量',
//             'class'   => 'compact',
//             'id'      => 'page_links_limit',
//             'default' => '-1',
//             'type'    => 'spinner',
//             'min'     => -1,
//             'step'    => 5,
//             'unit'    => '个',
//             'desc'    => '最多显示多少个链接，“-1”则为不限制',
//         ),
//         array(
//             'id'      => 'page_links_category',
//             'title'   => '显示分类',
//             'default' => '0',
//             'type'    => 'select',
//             'options' => 'el_cfs_link_category',
//         ),
//         array(
//             'title'   => __('提交链接模块', 'el_language'),
//             'id'      => 'page_links_submit_s',
//             'type'    => "switcher",
//             'default' => false,
//         ),
//         array(
//             'dependency' => array('page_links_submit_s', '!=', ''),
//             'title'      => ' ',
//             'subtitle'   => '提交链接模块：标题',
//             'id'         => 'page_links_submit_title',
//             'class'      => 'compact',
//             'default'    => '提交链接',
//             'type'       => 'text',
//         ),
//         array(
//             'dependency' => array('page_links_submit_s', '!=', ''),
//             'title'      => ' ',
//             'subtitle'   => '提交链接模块：副标题',
//             'id'         => 'page_links_submit_subtitle',
//             'class'      => 'compact',
//             'default'    => '提交链接',
//             'type'       => 'text',
//         ),
//         array(
//             'dependency' => array('page_links_submit_s', '!=', ''),
//             'id'         => 'page_links_submit_dec',
//             'title'      => ' ',
//             'subtitle'   => '提交链接模块：提交说明',
//             'class'      => 'compact',
//             'default'    => '<p>
// <li>您的网站已稳定运行，且有一定的文章量 </li>
// <li>原创、技术、设计类网站优先考虑</li>
// <li>不收录有反动、色情、赌博等不良内容或提供不良内容链接的网站</li>
// <li>您需要将本站链接放置在您的网站中</li>
// </p>
// <p><b>本站信息示例：</b></p>
// <ul>
// <li>名称：' . get_bloginfo('name') . '</li>
// <li>简介：' . get_bloginfo('description') . '</li>
// <li>链接：' . home_url() . '</li>
// <li>图标：' . _opz('favicon') . '</li>
// </ul>',
//             'attributes' => array(
//                 'rows' => 6,
//             ),
//             'sanitize'   => false,
//             'type'       => 'textarea',
//         ),
//     ),
// ));


// //导航列表模板

// CSF::createMetabox('nav_templates', array(
//     'title'          => '链接列表页面设置',
//     'post_type'      => 'page',
//     'context'        => 'side',
//     'data_type'      => 'unserialize',
//     'page_templates' => 'pages/Nav.php', // Spesific page template
// ));

// CSF::createSection('nav_templates', array(
//     'fields' => array(
//         array(
//             'title'   => __('显示页面内容', 'el_language'),
//             'id'      => 'page_nav_content_s',
//             'type'    => "switcher",
//             'default' => false,
//         ),
//         array(
//             'dependency' => array('page_nav_content_s', '!=', ''),
//             'id'         => 'page_nav_content_position',
//             'title'      => ' ',
//             'subtitle'   => '显示位置',
//             'default'    => 'top',
//             'class'      => 'compact',
//             'inline'     => true,
//             'type'       => 'radio',
//             'options'    => array(
//                 'top'    => __('链接列表上面'),
//                 'bottom' => __('链接列表下面'),
//             ),
//         ),
//         array(
//             'id'      => 'page_links_orderby',
//             'title'   => '链接排序方式',
//             'default' => 'name',
//             'type'    => 'select',
//             'options' => array(
//                 'name'    => __('名称排序'),
//                 'updated' => __('更新时间'),
//                 'rating'  => __('链接评分'),
//                 'rand'    => __('随机排序'),
//             ),
//         ),
//         array(
//             'id'       => 'page_links_order',
//             'title'    => ' ',
//             'subtitle' => ' ',
//             'default'  => 'ASC',
//             'class'    => 'compact',
//             'inline'   => true,
//             'type'     => 'radio',
//             'options'  => array(
//                 'ASC'  => __('升序'),
//                 'DESC' => __('降序'),
//             ),
//         ),
//         array(
//             'title'   => '限制数量',
//             'class'   => 'compact',
//             'id'      => 'page_links_limit',
//             'default' => '-1',
//             'type'    => 'spinner',
//             'min'     => -1,
//             'step'    => 5,
//             'unit'    => '个',
//             'desc'    => '最多显示多少个链接，“-1”则为不限制',
//         ),
//         array(
//             'id'      => 'page_links_category',
//             'title'   => '显示分类',
//             'default' => '0',
//             'type'    => 'select',
//             'options' => 'el_cfs_link_category',
//         ),
//         array(
//             'title'   => __('提交链接模块', 'el_language'),
//             'id'      => 'page_links_submit_s',
//             'type'    => "switcher",
//             'default' => false,
//         ),
//         array(
//             'dependency' => array('page_links_submit_s', '!=', ''),
//             'title'      => ' ',
//             'subtitle'   => '提交链接模块：标题',
//             'id'         => 'page_links_submit_title',
//             'class'      => 'compact',
//             'default'    => '提交链接',
//             'type'       => 'text',
//         ),
//         array(
//             'dependency' => array('page_links_submit_s', '!=', ''),
//             'title'      => ' ',
//             'subtitle'   => '提交链接模块：副标题',
//             'id'         => 'page_links_submit_subtitle',
//             'class'      => 'compact',
//             'default'    => '提交链接',
//             'type'       => 'text',
//         ),
//         array(
//             'dependency' => array('page_links_submit_s', '!=', ''),
//             'id'         => 'page_links_submit_dec',
//             'title'      => ' ',
//             'subtitle'   => '提交链接模块：提交说明',
//             'class'      => 'compact',
//             'default'    => '<p>
// <li>您的网站已稳定运行，且有一定的文章量 </li>
// <li>原创、技术、设计类网站优先考虑</li>
// <li>不收录有反动、色情、赌博等不良内容或提供不良内容链接的网站</li>
// <li>您需要将本站链接放置在您的网站中</li>
// </p>
// <p><b>本站信息示例：</b></p>
// <ul>
// <li>名称：' . get_bloginfo('name') . '</li>
// <li>简介：' . get_bloginfo('description') . '</li>
// <li>链接：' . home_url() . '</li>
// <li>图标：' . _opz('favicon') . '</li>
// </ul>',
//             'attributes' => array(
//                 'rows' => 6,
//             ),
//             'sanitize'   => false,
//             'type'       => 'textarea',
//         ),
//     ),
// ));