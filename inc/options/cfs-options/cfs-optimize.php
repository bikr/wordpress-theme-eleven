<?php
/*
 * @Author       : ZengHao
 * @Date         : 2023-12-23 14:14:26
 * @LastEditors  : ZengHao
 * @LastEditTime : 2024-02-22 10:49:37
 * @FilePath     : \inc\options\cfs-options\cfs-optimize.php
 * @Description  : 
 * Copyright 2023 www.exehub.com, All Rights Reserved. 
 * 2023-12-23 14:14:26
 */

CSF::createSection($prefix, array(
    'parent'      => 'optimize',
    'title'       => '展示优化',
    'icon'        => 'fa fa-hdd-o',
    'description' => '',
    'fields'      => array(
        // 这是总开关，打开后才去判断下面的子项目是否需要进行优化
        array(
            'id'      => 'el_clean_dash',
            'type'    => 'switcher',
            'title'   => '清理后台无用项',
            'label'   => '还后台清爽干净的后台',
            'default' => false
        ),
        array(
            'dependency' => array('el_clean_dash', '==', 'true'),
            'id'      => 'el_clean_toolbar',
            'type'    => 'switcher',
            'title'   => '顶部工具栏',
            'label'   => '去除WordPress顶部工具栏无用项目',
            'default' => true
        ),
        array(
            'dependency' => array('el_clean_dash', '==', 'true'),
            'id'      => 'el_clean_widget',
            'type'    => 'switcher',
            'title'   => '小工具',
            'label'   => '清除外观->小工具下面使用率低的小工具，展示混乱！只保留主题自带',
            'default' => false
        ),
        array(
            'dependency' => array('el_clean_dash|el_clean_toolbar', '==', 'true|true'),
            'id'         => 'el_clean_toolbar_items',
            'type'       => 'checkbox',
            'title'      => '顶部工具栏移除项目',
            'options'    => array(
                'wp-logo' => 'WordPress LOGO',
                'site-name' => '网站名称',
                'updates' => '更新提醒',
                'comments' => '评论提醒',
                'new-content' => '新建文件',
                'top-secondary' => '用户信息',
            ),
            'default'    => array('wp-logo', 'site-name', 'updates', 'comments', 'new-content', 'top-secondary')
        ),
        array(
            'dependency' => array('el_clean_dash', '==', 'true'),
            'id'      => 'el_remove_dashboard_widgets',
            'type'    => 'switcher',
            'title'   => '移除仪表盘无用项',
            'label'   => '去除WordPress移除仪表盘无用模块清理',
            'default' => true
        ),
        array(
            'dependency' => array('el_clean_dash|el_remove_dashboard_widgets', '==', 'true|true'),
            'id'         => 'el_remove_dashboard_widgets_items',
            'type'       => 'checkbox',
            'title'      => '移除仪表盘项目',
            'options'    => array(
                'welcome_panel' => '"WordPress 欢迎面板" 模块',
                'dashboard_quick_press' => '"快速发布" 模块',
                'dashboard_incoming_links' => '"引入链接" 模块',
                'dashboard_plugins' => '"插件" 模块',
                'dashboard_recent_comments' => '"近期评论" 模块',
                'dashboard_recent_drafts' => '"近期草稿" 模块',
                'dashboard_primary' => '"WordPress 开发日志" 模块',
                'dashboard_secondary' => '"其它 WordPress 新闻" 模块',
                'dashboard_right_now' => '"概况" 模块',
                'dashboard_site_health' => '"站点健康" 模块',
                'dashboard_activity' => '"活动" 模块',
                'dashboard_php_nag' => '"PHP版本检测" 模块',
            ),
            'default'    => array(
                'dashboard_quick_press',
                'dashboard_incoming_links', 
                'dashboard_plugins', 
                'dashboard_recent_comments', 
                'dashboard_recent_drafts', 
                'dashboard_primary', 
                'dashboard_secondary',
                'dashboard_right_now',
                'welcome_panel',
                'dashboard_site_health',
                'dashboard_activity',
                'dashboard_php_nag',
            )
        ),
        array(
            'dependency' => array('el_clean_dash', '==', 'true'),
            'id'      => 'el_clean_help',
            'type'    => 'switcher',
            'title'   => '移除帮助按钮',
            'label'   => '移除后台仪表盘菜单：帮助',
            'default' => true
        ),
        array(
            'dependency' => array('el_clean_dash', '==', 'true'),
            'id'      => 'hide_admin_bar',
            'type'    => 'switcher',
            'title'   => '关闭顶部admin_bar',
            'default' => true
        ),
    ),
));

CSF::createSection($prefix, array(
    'parent'      => 'optimize',
    'title'       => '功能优化',
    'icon'        => 'fa fa-gavel',
    'description' => '',
    'fields'      => array(
        array(
            'id'      => 'el_optimize_dash',
            'type'    => 'switcher',
            'title'   => '后台功能优化',
            'label'   => '禁用或优化一些无用的功能',
            'default' => false
        ),
        array(
            'dependency' => array('el_optimize_dash', '==', 'true'),
            'id'      => 'remove_emoji',
            'type'    => 'switcher',
            'title'   => '删除WordPress自带Emoji',
            'default' => true
        ),
        array(
            'dependency' => array('el_optimize_dash', '==', 'true'),
            'id'      => 'remove_open_sans',
            'type'    => 'switcher',
            'title'   => '禁用Google字体',
            'default' => true
        ),
        array(
            'dependency' => array('el_optimize_dash', '==', 'true'),
            'id'      => 'remove_other_meta',
            'type'    => 'switcher',
            'title'   => '清理多于的头部Meta标签',
            'default' => true
        ),
        array(
            'dependency' => array('el_optimize_dash', '==', 'true'),
            'id'      => 'disable_wp_update',
            'type'    => 'switcher',
            'title'   => '禁止WordPress检测更新',
            'label'   => '开启后在将禁用WordPress自动更新',
            'default' => true
        ),
        array(
            'dependency' => array('el_optimize_dash', '==', 'true'),
            'id'      => 'disable_gtb_editor',
            'type'    => 'switcher',
            'title'   => '禁用古腾堡编辑器',
            'label'   => '使用本主题，建议关闭',
            'default' => true
        ),
        array(
            'dependency' => array('el_optimize_dash', '==', 'true'),
            'id'      => 'disable_auto_emded',
            'type'    => 'switcher',
            'title'   => '禁用WordPress的自动嵌入功能',
            'label'   => '国内用不上，直接关闭',
            'default' => true
        ),
        array(
            'dependency' => array('el_optimize_dash', '==', 'true'),
            'id'      => 'clean_user_setting',
            'type'    => 'switcher',
            'title'   => '关闭后台用户编辑多余的选项',
            'label'   => '开启后在后台编辑用户资料时将不显示无用的多余选项',
            'default' => true
        ),
        array(
            'dependency' => array('el_optimize_dash', '==', 'true'),
            'id'      => 'remove_feed',
            'type'    => 'switcher',
            'title'   => '移除文章头部feed',
            'label'   => 'RSS订阅，容易被采集，可以关闭',
            'default' => true
        ),
        array(
            'dependency' => array('el_optimize_dash', '==', 'true'),
            'id'      => 'remove_dashicons',
            'type'    => 'switcher',
            'title'   => '移除Dashicons',
            'label'   => '移除前台加载的 Dashicons 资源，第三方主题基本用不上',
            'default' => true
        ),
        array(
            'dependency' => array('el_optimize_dash', '==', 'true'),
            'id'      => 'remove_googlemap',
            'type'    => 'switcher',
            'title'   => '移除谷歌地图API',
            'label'   => '国内基本不会使用，建议禁止',
            'default' => true
        ),
        array(
            'dependency' => array('el_optimize_dash', '==', 'true'),
            'id'      => 'remove_contact_form',
            'type'    => 'switcher',
            'title'   => '移除WordPress自带联系表单',
            'label'   => '联系表单CSS和JS文件是加载到网站的每个页面，建议禁止',
            'default' => true
        ),
        array(
            'dependency' => array('el_optimize_dash', '==', 'true'),
            'id'      => 'remove_translations_api',
            'type'    => 'switcher',
            'title'   => '禁用translations_api',
            'label'   => '进入设置后，会访问WordPress.org查询翻译，非常慢，建议禁用',
            'default' => true
        ),
        array(
            'dependency' => array('el_optimize_dash', '==', 'true'),
            'id'      => 'remove_pingback',
            'type'    => 'switcher',
            'title'   => '屏蔽 Trackbacks/Pingback',
            'label'   => '别人复制了你的网站内容，对方也是WordPress的话，你会收到一条系统留言',
            'default' => true
        ),
        array(
            'dependency' => array('el_optimize_dash', '==', 'true'),
            'id'      => 'remove_xmlrpc',
            'type'    => 'switcher',
            'title'   => '关闭XML-RPC接口',
            'label'   => 'WordPress对接第三方应用接口，没有可以关闭',
            'default' => true
        ),
    ),
));

CSF::createSection($prefix, array(
    'parent'      => 'optimize',
    'title'       => '系统工具',
    'icon'        => 'fa fa-safari',
    'description' => '',
    'fields'      => array(
        array(
            'id'      => 'el_remove_cat_url',
            'type'    => 'switcher',
            'title'   => '分类url去除category',
            'label'   => '开启后有利于SEO，建议在建站时设置好，后期不轻易修改',
            'desc'    => '该功能和no-category插件作用相同，不能同时使用',
            'default' => false
        ),
        array(
            'id'      => 'newfilename',
            'type'    => 'switcher',
            'title'   => '上传文件自动重命名',
            'desc'    => '由于上传文件时候系统会查询重名文件，开启此功能后可减少重名文件，从而提高上传效率',
            'default' => false
        ),
        array(
            'dependency' => array('newfilename', '!=', ''),
            'title'      => ' ',
            'subtitle'   => __('重命名格式', 'el_language'),
            'id'         => 'newfilename_type',
            'default'    => "random",
            'class'      => 'compact',
            'type'       => "radio",
            'options'    => array(
                'random' => __('随机字串符', 'el_language'),
                'time'   => __('时间+原文件名', 'el_language'),
            ),
        ),
        array(
            'title'   => __('框架文件CDN托管', 'el_language'),
            'id'      => 'js_outlink',
            'default' => "no",
            'desc'    => '将核心框架JS文件和CSS文件托管到CDN，对于部分地区的服务器可提高加载速度。如果页面显示不正常，请关闭！',
            'type'    => "radio",
            'options' => array(
                'no'         => __('不托管', 'el_language'),
                'staticfile' => __('七牛云', 'el_language'),
                'bootcdn'    => __('BootCDN', 'el_language'),
                'cdnjs'    => __('CDNJS', 'el_language'),
                'original'         => __('框架来源站点', 'el_language'),
            ),
        ),
    ),
));

// CSF::createSection($prefix, array(
//     'parent'      => 'optimize',
//     'title'       => '系统安全',
//     'icon'        => 'fa fa-puzzle-piece',
//     'description' => '',
//     'fields'      => array(
//         array(
//             'id'      => 'test1',
//             'type'    => 'text',
//             'title'   => 'Text',
//             'default' => 'Hello world.'
//         ),
//     ),
// ));
