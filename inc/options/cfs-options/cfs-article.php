<?php
/*
 * @Author       : ZengHao
 * @Date         : 2023-12-23 14:14:26
 * @LastEditors  : ZengHao
 * @LastEditTime : 2024-02-24 20:13:19
 * @FilePath     : /inc/options/cfs-options/cfs-article.php
 * @Description  : 
 * Copyright 2023 www.exehub.com, All Rights Reserved. 
 * 2023-12-23 14:14:26
 */

CSF::createSection($prefix, array(
    'parent' => 'article',
    'title' => '缩略图设置',
    'icon' => 'fa fa-file-image-o',
    'description' => '',
    'fields' => array(
        array(
            'id' => 'post_def_cover',
            'title' => __('文章封面图', 'el_language'),
            'subtitle' => __('默认封面图，没有设置文章封面时才会取用'),
            'default' => $imagepath . 'user_t.jpg',
            'library' => 'image',
            'type' => 'upload',
        ),
        array(
            'id' => 'cat_def_cover',
            'title' => __('分类封面图', 'el_language'),
            'subtitle' => __('默认封面图，没有设置分类封面时才会取用'),
            'default' => $imagepath . 'user_t.jpg',
            'library' => 'image',
            'type' => 'upload',
        ),
        array(
            'id' => 'topics_def_cover',
            'title' => __('专题封面图', 'el_language'),
            'subtitle' => __('默认封面图，没有设置专题封面时才会取用'),
            'default' => $imagepath . 'user_t.jpg',
            'library' => 'image',
            'type' => 'upload',
        ),
        // array(
        //     'id'      => 'thumb_postfirstimg_size',
        //     'title'   => '缩略图大小',
        //     'default' => 'medium',
        //     'desc'    => '此处的三个尺寸均可在<a href="' . admin_url('options-media.php') . '">WP后台-媒体设置</a>中修改，建议此处选择中尺寸，并将中尺寸的尺寸设置为800x550效果最佳',
        //     'type'    => "radio",
        //     'inline'  => true,
        //     'options' => array(
        //         'thumbnail' => __('小尺寸', 'el_language'),
        //         'medium'    => __('中尺寸', 'el_language'),
        //         'large'     => __('大尺寸', 'el_language'),
        //         'full'      => __('原图', 'el_language'),
        //     ),
        // ),
    ),
)
);

CSF::createSection($prefix, array(
    'parent' => 'article',
    'title' => '文章列表',
    'icon' => 'fa fa-list',
    'description' => '',
    'fields' => array(
        //TODO:Ajax翻页
        // array(
        //     'id'      => 'paging_ajax_s',
        //     'title'   => '列表翻页模式',
        //     'default' => '1',
        //     'type'    => "radio",
        //     'inline'  => true,
        //     'desc'    => '您可以在<a href="' . esc_url(admin_url('options-reading.php')) . '">WP设置-阅读-博客页面至多显示</a>，以调整单页加载数量',
        //     'options' => array(
        //         '1' => __('AJAX追加列表翻页', 'el_language'),
        //         '0' => __('数字翻页按钮', 'el_language'),
        //     ),
        // ),
        array(
            'dependency' => array('paging_ajax_s', '==', '1'),
            'title'      => ' ',
            'subtitle'   => 'AJAX自动加载',
            'class'      => 'compact',
            'id'         => 'paging_ajax_ias_s',
            'type'       => 'switcher',
            'label'      => '页面滚动到列表尽头时，自动加载下一页',
            'default'    => true,
        ),
        array(
            'dependency' => array('paging_ajax_s|paging_ajax_ias_s', '!=|!=', '0|'),
            'title'      => ' ',
            'subtitle'   => '自动加载页数',
            'desc'       => 'AJAX自动加载最多加载几页（为0则不限制,直到加载全部列表）',
            'id'         => 'ias_max',
            'class'      => 'compact',
            'default'    => 3,
            'max'        => 10,
            'min'        => 0,
            'step'       => 1,
            'unit'       => '页',
            'type'       => 'spinner',
        ),
        array(
            'dependency' => array('paging_ajax_s', '==', '1'),
            'title'      => 'AJAX翻页',
            'subtitle'   => '翻页按钮文字',
            'id'         => 'ajax_trigger',
            'default'    => '<i class="fa fa-angle-right"></i>加载更多',
            'attributes' => array(
                'rows' => 1,
            ),
            'sanitize'   => false,
            'type'       => 'textarea',
        ),
        array(
            'dependency' => array('paging_ajax_s', '==', '1'),
            'title'      => ' ',
            'id'         => 'ajax_nomore',
            'class'      => 'compact',
            'subtitle'   => '列表全部加载完毕 文案',
            'default'    => '没有更多内容了',
            'desc'       => '支持HTML代码，请注意代码规范及标签闭合<br>您可以在<a href="' . esc_url(admin_url('options-reading.php')) . '">WP设置-阅读-博客页面至多显示</a>，以调整单页加载数量',
            'attributes' => array(
                'rows' => 1,
            ),
            'sanitize'   => false,
            'type'       => 'textarea',
        ),
        array(
            'id'      => 'home_list_type',
            'title'   => '首页列表模式',
            'default' => '1',
            'type'    => "radio",
            'inline'  => true,
            'options' => array(
                '3' => __('封面模式', 'el_language'),
                '2' => __('卡片模式', 'el_language'),
                '1' => __('图文模式', 'el_language'),
                '0' => __('随机模式', 'el_language'),
            ),
            'desc'  => '只随机封面和图文模式，后同'
        ),
        array(
            'id'      => 'cat_list_type',
            'title'   => '分类页列表模式',
            'default' => '1',
            'type'    => "radio",
            'inline'  => true,
            'options' => array(
                '3' => __('封面模式', 'el_language'),
                '2' => __('卡片模式', 'el_language'),
                '1' => __('图文模式', 'el_language'),
                '0' => __('随机模式', 'el_language'),
            ),
        ),
        array(
            'id'      => 'tag_list_type',
            'title'   => '标签列表模式',
            'default' => '1',
            'type'    => "radio",
            'inline'  => true,
            'options' => array(
                '3' => __('封面模式', 'el_language'),
                '2' => __('卡片模式', 'el_language'),
                '1' => __('图文模式', 'el_language'),
                '0' => __('随机模式', 'el_language'),
            ),
        ),
        array(
            'id'      => 'search_list_type',
            'title'   => '搜索页列表模式',
            'default' => '1',
            'type'    => "radio",
            'inline'  => true,
            'options' => array(
                '3' => __('封面模式', 'el_language'),
                '2' => __('卡片模式', 'el_language'),
                '1' => __('图文模式', 'el_language'),
                '0' => __('随机模式', 'el_language'),
            ),
        ),
    ),
)
);

CSF::createSection($prefix, array(
    'parent' => 'article',
    'title' => '文章页面',
    'icon' => 'fa fa-columns',
    'description' => '',
    'fields' => array(
        // array(
        //     'title'   => '代码高亮',
        //     'id'      => 'highlight_kg',
        //     'type'    => 'switcher',
        //     'default' => true,
        //     'label'   => '全局开关，不会影响古腾堡块-代码高亮块',
        // ),
        // array(
        //     'title'    => ' ',
        //     'subtitle' => '代码高亮显示行号',
        //     'id'       => 'highlight_hh',
        //     'type'     => 'switcher',
        //     'class'    => 'compact',
        //     'default'  => false,
        // ),

        // array(
        //     'title'    => ' ',
        //     'subtitle' => '代码高亮显示扩展按钮',
        //     'id'       => 'highlight_btn',
        //     'type'     => 'switcher',
        //     'class'    => 'compact',
        //     'label'    => '显示切换高亮、复制、新窗口打开三个扩展按钮',
        //     'default'  => false,
        // ),

        // array(
        //     'title'    => '默认主题',
        //     'subtitle' => '日间亮色模式下->默认主题',
        //     'id'       => 'highlight_zt',
        //     'type'     => 'select',
        //     'class'    => 'compact',
        //     'default'  => 'enlighter',
        //     'options'  => array(
        //         'enlighter'  => __('浅色: Enlighter'),
        //         'bootstrap4' => __('浅色：Bootstrap'),
        //         'classic'    => __('浅色：Classic'),
        //         'beyond'     => __('浅色：Beyond'),
        //         'mowtwo'     => __('浅色：Mowtwo'),
        //         'eclipse'    => __('浅色：Eclipse'),
        //         'droide'     => __('浅色：Droide'),
        //         'minimal'    => __('浅色：Minimal'),
        //         'rowhammer'  => __('浅色：Rowhammer'),
        //         'godzilla'   => __('浅色：Godzilla'),
        //         'dracula'    => __('深色：Dracula'),
        //         'atomic'     => __('深色：Atomic'),
        //         'monokai'    => __('深色：Monokai'),
        //     ),
        // ),

        // array(
        //     'id'       => 'highlight_dark_zt',
        //     'title'    => ' ',
        //     'subtitle' => '夜间深色模式下->默认主题',
        //     'type'     => 'select',
        //     'class'    => 'compact',
        //     'default'  => 'dracula',
        //     'desc'     => '此为默认设置，古腾堡编辑器中可单独设置主题<br>主题预览地址： https://enlighterjs.org/Theme.Enlighter.html',
        //     'options'  => array(
        //         'dracula'    => __('深色：Dracula'),
        //         'atomic'     => __('深色：Atomic'),
        //         'monokai'    => __('深色：Monokai'),
        //         'enlighter'  => __('浅色: Enlighter'),
        //         'bootstrap4' => __('浅色：Bootstrap'),
        //         'classic'    => __('浅色：Classic'),
        //         'beyond'     => __('浅色：Beyond'),
        //         'mowtwo'     => __('浅色：Mowtwo'),
        //         'eclipse'    => __('浅色：Eclipse'),
        //         'droide'     => __('浅色：Droide'),
        //         'minimal'    => __('浅色：Minimal'),
        //         'rowhammer'  => __('浅色：Rowhammer'),
        //         'godzilla'   => __('浅色：Godzilla'),
        //     ),
        // ),

        // array(
        //     'title'    => ' ',
        //     'subtitle' => '代码高亮最大高度',
        //     'id'       => 'highlight_maxheight',
        //     'class'    => 'compact',
        //     'desc'     => __('设置为0则不限制高度', 'el_language'),
        //     'default'  => 400,
        //     'max'      => 2000,
        //     'min'      => 0,
        //     'step'     => 25,
        //     'unit'     => 'PX',
        //     'type'     => 'spinner',
        // ),
        // array(
        //     'dependency' => array('highlight_maxheight', '==', 0),
        //     'type'       => 'submessage',
        //     'style'      => 'success',
        //     'content'    => '已设置代码高亮<b>不限制最大高度</b>',
        // ),
        array(
            'title'   => '图片灯箱',
            'id'      => 'imagelightbox',
            'type'    => 'switcher',
            'default' => true,
            'label'   => '',
        ),
        array(
            'title'   => '文章图片懒加载',
            'id'      => 'lazy_posts_content',
            'type'    => 'switcher',
            'default' => false,
            'label'   => '可能影响SEO',
        ),
        array(
            'title'   => '文章图片自动alt',
            'id'      => 'post_img_auto_alt',
            'type'    => 'switcher',
            'default' => false,
            'label'   => '自动添加alt，增强SEO',
        ),
        array(
            'title'   => '文章链接外部打开',
            'id'      => 'go_link_post',
            'type'    => 'switcher',
            'default' => false,
            'label'   => '文章内的链接在go页面打开',
        ),
    ),
)
);
// CSF::createSection($prefix, array(
//     'parent' => 'article',
//     'title' => '评论设置',
//     'icon' => 'fa fa-comments-o',
//     'description' => '',
//     'fields' => array(
//         array(
//             'id' => 'test1',
//             'type' => 'text',
//             'title' => 'Text',
//             'default' => 'Hello world.'
//         ),
//     ),
// )
// );