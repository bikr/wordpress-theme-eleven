<?php
/*
 * @Author       : ZengHao
 * @Date         : 2023-12-23 14:14:26
 * @LastEditors  : ZengHao
 * @LastEditTime : 2024-02-22 21:30:34
 * @FilePath     : /inc/options/cfs-options/cfs-home.php
 * @Description  : 
 * Copyright 2023 www.exehub.com, All Rights Reserved. 
 * 2023-12-23 14:14:26
 */

CSF::createSection($prefix, array(
    'parent'      => 'home',
    'title'       => '导航栏设置',
    'icon'        => 'fa fa-bars',
    'description' => '',
    'fields'      => array(
        // array(
        //     'title'   => __('电脑端导航布局', 'el_language'),
        //     'id'      => 'header_layout',
        //     'default' => "1",
        //     'type'    => "image_select",
        //     'options' => array(
        //         '1' => $f_imgpath . 'header_layout_1.png',
        //         '2' => $f_imgpath . 'header_layout_2.png',
        //         '3' => $f_imgpath . 'header_layout_3.png',
        //     ),
        // ),
        // array(
        //     'title'   => __('移动端导航布局', 'el_language'),
        //     'id'      => 'mobile_header_layout',
        //     'default' => "center",
        //     'type'    => "image_select",
        //     'options' => array(
        //         'center' => $f_imgpath . 'mobile_header_layout_center.png',
        //         'left'   => $f_imgpath . 'mobile_header_layout_left.png',
        //     ),
        // ),
        array(
            'id'      => 'mobile_header_layout',
            'default' => 'center',
            'type'    => 'radio',
            'title'   => '移动端导航布局',
            'inline'  => true,
            'options' => array(
                'center'   => __('Logo居中', 'el_language'),
                'left'  => __('Logo靠左', 'el_language'),
                'right' => __('Logo靠右', 'el_language'),
            ),
        ),
        // array(
        //     'id'      => 'mobile_navbar_align',
        //     'default' => 'left',
        //     'type'    => 'radio',
        //     'title'   => '移动端菜单弹出方向',
        //     'inline'  => true,
        //     'options' => array(
        //         'top'   => __('顶部', 'el_language'),
        //         'left'  => __('左边', 'el_language'),
        //         'right' => __('右边', 'el_language'),
        //     ),
        // ),
        // array(
        //     'title'    => __('导航浮动', 'el_language'),
        //     'subtitle' => __('导航一直固定在顶部'),
        //     'id'       => 'nav_fixed',
        //     'type'     => 'switcher',
        //     'default'  => true,
        // ),
    ),
));

// CSF::createSection($prefix, array(
//     'parent'      => 'home',
//     'title'       => '首页配置',
//     'icon'        => 'fa fa-home',
//     'description' => '',
//     'fields'      => array(
//         array(
//             'id'      => 'test1',
//             'type'    => 'text',
//             'title'   => 'Text',
//             'default' => 'Hello world.'
//         ),
//     ),
// ));

CSF::createSection($prefix, array(
    'parent'      => 'home',
    'title'       => '底部页脚',
    'icon'        => 'fa fa-lemon-o',
    'description' => '',
    'fields'      => array(
        array(
            'id'         => 'footer_show',
            'type'       => 'radio',
            'title'      => '显示模块',
            'desc'      => '只控制在移动端的显示',
            'options'    => array(
                'about' => '靠左关于模块',
                'nav' => '居中链接',
                'qrcode' => '靠右二维码模块',
            ),
            'default'    => 'about'
        ),
        array(
            'title' => __('页脚Logo/图像', 'el_language'),
            'subtitle' => __('最大高度66px', 'el_language'),
            'id' => 'footer_logo_src',
            'class' => 'compact',
            'default' => $imagepath . 'logo_dark.png',
            'preview' => true,
            'library' => 'image', 'type' => 'upload',
        ),
        array(
            'id'      => 'footer_logo_des',
            'type'    => 'textarea',
            'title'   => '页脚网站描述',
            'default' => 'Eleven是一款快速的自媒体主题'
        ),
        array(
            'id'        => 'footer_custom_link',
            'type'      => 'group',
            'title'     => '页脚链接',
            'fields'    => array(
                array(
                    'id'    => 'title',
                    'type'  => 'text',
                    'title' => '标题',
                ),
                array(
                    'id'    => 'url',
                    'type'  => 'text',
                    'title' => '链接地址',
                ),
                array(
                    'id'    => 'target',
                    'type'  => 'switcher',
                    'title' => '新标签打开',
                    'default' => true
                ),
            ),
        ),
        array(
            'id'        => 'footer_custom_icon',
            'type'      => 'group',
            'title'     => '页脚图标',
            'desc'      => '可以设置社交信息等，支持配置弹出图片，弹出文字和链接',
            'fields'    => array(
                array(
                    'id'    => 'icon',
                    'type'  => 'text',
                    'title' => '图标',
                ),
                array(
                    'type'    => 'notice',
                    'style'   => 'success',
                    'content' => '本站支持<a href="https://fontawesome.com/v4/icons/" target="_blank">Font Awesome 4</a>以及<a href="feathericons.com" target="_blank">feathericons</a>，选择这两个请填写完整html标签，如果是其他图标，请填写svg的html标签',
                ),
                array(
                    'id'         => 'poptype',
                    'type'       => 'radio',
                    'title'      => '弹出模式',
                    'options'    => array(
                        'img' => '图片',
                        'text' => '文字',
                    ),
                    'default'    => 'text'
                ),
                array(
                    'dependency' => array('poptype', '==', 'text'),
                    'id'    => 'title',
                    'type'  => 'text',
                    'title' => '标题',
                ),
                array(
                    'dependency' => array('poptype', '==', 'text'),
                    'id'    => 'url',
                    'type'  => 'text',
                    'title' => '链接地址',
                ),
                array(
                    'dependency' => array('poptype', '==', 'text'),
                    'id'    => 'target',
                    'type'  => 'switcher',
                    'title' => '新标签打开',
                    'default' => true
                ),
                array(
                    'dependency' => array('poptype', '==', 'img'),
                    'title' => __('弹出图像', 'el_language'),
                    'id' => 'image',
                    'preview' => true,
                    'library' => 'image', 'type' => 'upload',
                ),
            ),
        ),
        array(
            'id'        => 'footer_mini_imgs',
            'type'      => 'group',
            'title'     => '页脚二维码',
            'fields'    => array(
                array(
                    'title' => __('二维码图像', 'el_language'),
                    'id' => 'image',
                    'preview' => true,
                    'library' => 'image', 'type' => 'upload',
                ),
                array(
                    'id'    => 'desc',
                    'type'  => 'text',
                    'title' => '图像描述',
                ),
            ),
        ),
    ),
));

// CSF::createSection($prefix, array(
//     'parent'      => 'home',
//     'title'       => '其他页面',
//     'icon'        => 'fa fa-map-o',
//     'description' => '',
//     'fields'      => array(
//         array(
//             'id'      => 'test1',
//             'type'    => 'text',
//             'title'   => 'Text',
//             'default' => 'Hello world.'
//         ),
          
//     ),
// ));
