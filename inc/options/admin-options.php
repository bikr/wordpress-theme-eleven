<?php
/*
 * @Author       : ZengHao
 * @Date         : 2023-12-23 14:14:26
 * @LastEditors  : ZengHao
 * @LastEditTime : 2024-02-22 10:50:33
 * @FilePath     : \inc\options\admin-options.php
 * @Description  : 
 * Copyright 2023 www.exehub.com, All Rights Reserved. 
 * 2023-12-23 14:14:26
 */

 $prefix = EL_OPT;

function eleven_csf_admin_options()
{
    // 限制只有后台才能执行主题初始化代码
    if (!is_admin()) {
        return;
    }
    $prefix = EL_OPT;
    // 图片资源
    $f_imgpath = EL_ASSETS . 'img/options/';
    $imagepath = EL_ASSETS . 'img/';
    //开始构建
    CSF::createOptions(
        $prefix,
        array(
            'menu_title' => 'Eleven主题设置',
            'menu_slug' => 'eleven_options',
            'menu_icon' => 'dashicons-coffee',
            'framework_title' => 'Eleven 自媒体主题',
            'show_in_customizer' => true,
            'footer_text' => '<a href="" target="_blank">Eleven自媒体主题</a> 用文字书写科技，用图片记录生活',
            'footer_credit' => '<i class="fa fa-fw fa-bell-o" aria-hidden="true"></i> 感谢您使用强大的 <a href="https://wordpress.org">WordPress</a> 搭配 <a href="" target="_blank">Eleven自媒体主题</a> 使用',
            'theme' => 'light',
        )
    );

    CSF::createSection(
        $prefix,
        array(
            'id' => 'basic',
            'title' => '基础设置',
            'icon' => 'fa fa-cogs',
        )
    );

    CSF::createSection(
        $prefix,
        array(
            'id' => 'home',
            'title' => '首页设置',
            'icon' => 'fa fa-fw fa-envira',
        )
    );

    CSF::createSection(
        $prefix,
        array(
            'id' => 'article',
            'title' => '文章设置',
            'icon' => 'fa fa-fw fa-pencil-square-o',
        )
    );

    CSF::createSection(
        $prefix,
        array(
            'id' => 'optimize',
            'title' => '系统优化',
            'icon' => 'fa fa-fw fa-microchip',
        )
    );

    CSF::createSection(
        $prefix,
        array(
            'title' => '主题备份',
            'icon' => 'fa fa-server',
            'fields'      => array(
                array(
                    'type' => 'backup',
                ),
            ),
        )
    );

    // CSF::createSection($prefix, array(
    //     'title'       => '主题授权',
    //     'icon'        => 'fa fa-coffee',
    //     'description' => '',
    //     'fields'      => array(
    //         array(
    //             'type'    => 'submessage',
    //             'style'   => 'warning',
    //             'content' => '放一些简单的介绍，支持html标签',
    //         ),
    //         EL_CFS_Module::theme_aut(),
    //     ), 
    // ));

    $update_icon = ' ';
    if (!get_option('is_skip_update') && get_option('theme_is_update')) {
        $update_icon = ' c-red';
    }
    CSF::createSection($prefix, array(
        'title'       => '主题更新',
        'icon'        => 'fa fa-fw fa-cloud-upload' . $update_icon,
        'description' => '',
        'fields'      => EL_CFS_Module::theme_update(),
    ));

    CSF::createSection($prefix, array(
        'title'       => '文档教程',
        'icon'        => 'fa fa-file-word-o',
        'description' => '',
        'fields'      => array(
            array(
                'type'    => 'submessage',
                'style'   => 'warning',
                'content' => '使用下列的文档，你可以更好的了解WordPress和本主题，以及一些优化设置',
            ),
            EL_CFS_Module::theme_doc(),
        ), 
    ));

    //载入分类设置文件，方便管理
    $cfs_options = array(
        'cfs-basic',
        'cfs-home',
        'cfs-article',
        'cfs-optimize',
    );


    foreach ($cfs_options as $cfs) {
        $path = 'inc/options/cfs-options/' . $cfs . '.php';
        include EL_THEME_DIR . $path;
    }
}
eleven_csf_admin_options();