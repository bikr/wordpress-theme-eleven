<?php
/*
 * @Author       : ZengHao
 * @Date         : 2024-01-08 15:19:25
 * @LastEditors  : ZengHao
 * @LastEditTime : 2024-01-08 16:02:35
 * @FilePath     : \inc\options\profile-options.php
 * @Description  : 
 * Copyright 2024 www.exehub.net, All Rights Reserved. 
 * 2024-01-08 15:19:25
 */

//个人资料设置
CSF::createProfileOptions('user_meta_main', array(
    'data_type' => 'unserialize',
));
CSF::createSection('user_meta_main', array(
    'fields' => array(
        array(
            'id'      => 'custom_avatar',
            'type'    => 'upload',
            'library' => 'image',
            'title'   => '自定义头像',
        ),
        array(
            'id'      => 'cover_image',
            'type'    => 'upload',
            'library' => 'image',
            'title'   => '自定义封面',
        ),
        array(
            'id'      => 'gender',
            'type'    => 'select',
            'title'   => '性别',
            'options' => array(
                '保密' => '保密',
                '男'  => '男',
                '女'  => '女',
            ),
            'default' => '保密',
        ),
        array(
            'id'          => 'qq',
            'type'        => 'text',
            'title'       => 'QQ',
            'placeholder' => '请输入QQ号',
        ),
        array(
            'id'          => 'weixin',
            'type'        => 'text',
            'title'       => '微信',
            'placeholder' => '请输入微信号',
        ),
        array(
            'id'          => 'weibo',
            'type'        => 'text',
            'title'       => '微博',
            'placeholder' => '请输入微博地址',
        ),
        array(
            'id'          => 'github',
            'type'        => 'text',
            'title'       => 'GitHub',
            'placeholder' => '请输入GitHub地址',
        ),
        array(
            'id'          => 'address',
            'type'        => 'text',
            'title'       => '住址',
            'placeholder' => '请输入住址',
        ),
        array(
            'id'          => 'phone_number',
            'type'        => 'text',
            'title'       => '绑定手机号',
            'placeholder' => '请输入手机号',
            'attributes'  => array(
                'data-readonly-id' => 'user_phone_number',
                'readonly'         => 'readonly',
            ),
            'desc'        => '手机号涉及到用户登录的功能，不建议后台修改<br><a href="javascript:;" class="but c-yellow remove-readonly" readonly-id="user_phone_number">修改用户手机号</a>',
        ),
    ),
));
