<?php
/*
 * @Author       : ZengHao
 * @Date         : 2023-12-23 17:30:51
 * @LastEditors  : ZengHao
 * @LastEditTime : 2024-02-22 10:25:32
 * @FilePath     : \inc\options\action.php
 * @Description  : 
 * Copyright 2023 www.exehub.com, All Rights Reserved. 
 * 2023-12-23 17:30:51
 */

/**
 * @description: 后台AJAX发送测试邮件
 * @param {*}
 * @return {*}
 */
function el_test_send_mail()
{
    if (empty($_POST['email'])) {
        echo el_res(1,"请输入邮箱账号",null);
        exit();
    }
    if (!filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)) {
        echo el_res(1,"邮箱格式错误",null);
        exit();
    }
    $headers = "MIME-Version: 1.0\n" . "Content-Type: text/html;";
    $blog_name = get_bloginfo('name');
    $blog_url  = get_bloginfo('url');
    $title     = '[' . $blog_name . '] 测试邮件';

    $message = '您好！ <br />';
    $message .= '这是一封来自' . $blog_name . '[' . $blog_url . ']的测试邮件<br />';
    $message .= '该邮件由网站后台发出，如果非您本人操作，请忽略此邮件 <br />';
    $message .= current_time("Y-m-d H:i:s");
    try {
        $test = wp_mail($_POST['email'], $title, $message,$headers);
    } catch (\Exception $e) {
        echo el_res(1,"邮件发送失败",$e->getMessage());
        exit();
    }

    if ($test) {
        echo el_res(0,"邮件发送成功",EL_ENV === 'DEV' ? $test : null);
        exit();
    } else {
        echo el_res(1,"邮件发送失败",EL_ENV === 'DEV' ? $test : null);
        exit();
    }
    exit();
}
add_action('wp_ajax_test_send_mail', 'el_test_send_mail');

function el_admin_detect_update()
{
    $version = json_decode(el_get_ThemeVersion());
    if ($version->error == 0) {
        update_option('is_skip_update', '1');
        update_option('theme_is_update', '0');
        echo el_res(0,"检测成功，主题更新已更新至：" . $version->data->version,null);
        exit();
    } else {
        update_option('is_skip_update', '1');
        update_option('theme_is_update', '0');
        echo el_res(1,"更新失败！" . $version->msg,null);
        exit();
    }
}
add_action('wp_ajax_admin_detect_update', 'el_admin_detect_update');


function el_admin_skip_update()
{
    $theme_check_time = time(); // 当前检测时间
    $theme_next_check_time = get_option('theme_next_check_time'); // 下次检测时间
    $is_skip_update = get_option('is_skip_update'); // 是否跳过更新
    if ($is_skip_update && $theme_next_check_time > $theme_check_time) {
        echo el_res(1,'你已经跳过，无需再次跳过，下次自动检测更新时间：' . date("Y-m-d H:i:s", get_option('theme_next_check_time')),null);
        exit();
    } else {
        update_option('theme_next_check_time', time() + NEXT_WEAK);
        update_option('is_skip_update', '1');
        echo el_res(0,'跳过成功，下次自动检测更新时间：' . date("Y-m-d H:i:s", get_option('theme_next_check_time')),null);
        exit();
    }
}
add_action('wp_ajax_admin_skip_update', 'el_admin_skip_update');