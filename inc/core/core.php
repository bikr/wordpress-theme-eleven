<?php
/*
 * @Author       : ZengHao
 * @Date         : 2023-12-26 12:20:40
 * @LastEditors  : ZengHao
 * @LastEditTime : 2024-01-19 00:04:50
 * @FilePath     : /inc/core/core.php
 * @Description  : 核心文件引入
 * Copyright 2023 www.exehub.com, All Rights Reserved. 
 * 2023-12-26 12:20:40
 */

//载入文件
$cores = array(
    'ELAut',
    'user',
);

foreach ($cores as $core) {
    $path = 'inc/core/' . $core . '.php';
    require get_theme_file_path($path);
}

// TODO:网站内容更新时，刷新主题设置的对应的缓存

// 添加钩子函数，当菜单保存后触发
add_action('wp_update_nav_menu', 'el_menu_saved_clean_cache');
function el_menu_saved_clean_cache($menu_id) {
    // 检查是否传递了有效的菜单ID
    if (!empty($menu_id)) {
        ECache::delete('theme_nav_top');
        ECache::delete('theme_nav_top1');
    }
}

// 在主题设置保存后执行的自定义代码
function el_after_theme_options_saved() {
    ECache::delete(EL_OPT);
}
add_action('cfs_el_options_save_after', 'el_after_theme_options_saved');