<?php 
/*
 * @Author       : ZengHao
 * @Date         : 2023-12-28 15:06:42
 * @LastEditors  : ZengHao
 * @LastEditTime : 2023-12-28 15:06:51
 * @FilePath     : \inc\core\http.php
 * @Description  : 
 * Copyright 2023 www.exehub.net, All Rights Reserved. 
 * 2023-12-28 15:06:42
 */
class BIK_HTTP
{
    /**
     * @author 北忘山 <www.beiwangshan.com>
     * @param $url string 请求地址
     * @param $args array 请求参数
     */
    public static function doGet($url, $args = array())
    {
        $args = wp_parse_args($args, array(
            'body'                => array(),
            'timeout'            => 5, //放弃之前的等待时间
            'method'            => 'GET',
            'sslverify'            => false,
            'redirection'            => 5,   //跳转之前的等待时间
            'blocking'            => true,
            'headers' => array(),
            'cookies' => array(),
        ));
        try {
            $response = wp_remote_get($url, $args);
            $body = wp_remote_retrieve_body($response);
            $body_json_decoded = json_decode($body, true);
            if (is_null($body_json_decoded)) {
                $body = wp_remote_retrieve_body($response);
            } else {
                $body = $body_json_decoded;
            }
            $http_code = wp_remote_retrieve_response_code($response);
        } catch (\Exception $e) {
            echo array('error' => 1, 'msg' => $e->getMessage());
            exit();
        }
        // $data = array(
        //     'code' => $http_code,
        //     'body' => $body
        // );
        return bik_res_data($http_code, '获取成功', $body);
        // return json_encode($data);
    }

    /**
     * @author 北忘山 <www.beiwangshan.com>
     * @param $url string 请求地址
     * @param $args array 请求参数
     */
    public static function doPost($url, $args = array())
    {
        $args = wp_parse_args($args, array(
            'body'                => array(),
            'timeout'            => 5, //放弃之前的等待时间
            'method'            => 'POST',
            'sslverify'            => false,
            'redirection'            => 5,   //跳转之前的等待时间
            'blocking'            => true,
            'headers' => array(),
            'cookies' => array(),
        ));
        try {
            $response = wp_remote_post($url, $args);
            $body = wp_remote_retrieve_body($response);
            $body_json_decoded = json_decode($body, true);
            if (is_null($body_json_decoded)) {
                $body = wp_remote_retrieve_body($response);
            } else {
                $body = $body_json_decoded;
            }
            $http_code = wp_remote_retrieve_response_code($response);
        } catch (\Exception $e) {
            echo array('error' => 1, 'msg' => $e->getMessage());
            exit();
        }

        // $data = array(
        //     'code' => $http_code,
        //     'body' => $body
        // );
        return bik_res_data($http_code, '获取成功', $body);
        // return json_encode($data);
    }
}


function bik_res_data($code=300, $msg = '', $body = array())
{
    $data = array(
        'code' => $code,
        'msg' => $msg,
        'body' => $body
    );
    return json_encode($data);
}