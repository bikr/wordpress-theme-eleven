<?php
/*
 * @Author       : ZengHao
 * @Date         : 2024-02-25 23:21:57
 * @LastEditors  : ZengHao
 * @LastEditTime : 2024-02-25 23:21:58
 * @FilePath     : /inc/core/ELAut copy.php
 * @Description  : 
 * Copyright 2024 www.exehub.com, All Rights Reserved. 
 * 2024-02-25 23:21:57
 */
/*
 * @Author       : ZengHao
 * @Date         : 2023-12-26 11:17:07
 * @LastEditors  : ZengHao
 * @LastEditTime : 2023-12-26 15:42:15
 * @FilePath     : \inc\core\ELAut.php
 * @Description  : 授权文件，到时候需要加密
 * Copyright 2023 www.exehub.net, All Rights Reserved. 
 * 2023-12-26 11:17:07
 */
final class ELAut
{
    // 判断是否是本地环境
    public static function is_local()
    {
        $flag = false;
        if ($_SERVER['HTTP_HOST'] === 'localhost' || $_SERVER['HTTP_HOST'] === '127.0.0.1') {
            $flag = true;
        }
        if ($_SERVER['SERVER_NAME'] === 'localhost' || $_SERVER['SERVER_NAME'] === '127.0.0.1') {
            $flag = true;
        }else{
            $flag = false;
        }
        // 检查WordPress安装路径是否包含本地路径
        $wordpress_path = ABSPATH;
        if (strpos($wordpress_path, '/localhost/') !== false || strpos($wordpress_path, '/127.0.0.1/') !== false) {
            $flag = true;
        }else{
            $flag = false;
        }
        return $flag;
    }
}
