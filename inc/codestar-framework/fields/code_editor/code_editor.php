<?php if (!defined('ABSPATH')) {
/*
 * @Author       : ZengHao
 * @Date         : 2024-01-18 00:23:19
 * @LastEditors  : ZengHao
 * @LastEditTime : 2024-02-25 18:38:04
 * @FilePath     : /inc/codestar-framework/fields/code_editor/code_editor.php
 * @Description  : 
 * Copyright 2024 www.exehub.com, All Rights Reserved. 
 * 2024-01-18 00:23:19
 */
  die;
}
/**
 *
 * Field: code_editor
 *
 * @since 1.0.0
 * @version 1.0.0
 *
 */
if (!class_exists('CSF_Field_code_editor')) {
  class CSF_Field_code_editor extends CSF_Fields
  {

    public $version = '5.62.2';
    public $cdn_url = 'https://cdn.bootcdn.net/ajax/libs/codemirror/';

    public function __construct($field, $value = '', $unique = '', $where = '', $parent = '')
    {
      parent::__construct($field, $value, $unique, $where, $parent);
    }

    public function render()
    {

      $default_settings = array(
        'tabSize'       => 2,
        'lineNumbers'   => true,
        'theme'         => 'default',
        'mode'          => 'htmlmixed',
        'cdnURL'        => $this->cdn_url . $this->version,
      );

      $settings = (!empty($this->field['settings'])) ? $this->field['settings'] : array();
      $settings = wp_parse_args($settings, $default_settings);

      echo $this->field_before();
      echo '<textarea name="' . esc_attr($this->field_name()) . '"' . $this->field_attributes() . ' data-editor="' . esc_attr(json_encode($settings)) . '">' . $this->value . '</textarea>';
      echo $this->field_after();
    }

    public function enqueue()
    {

      $page = (!empty($_GET['page'])) ? sanitize_text_field(wp_unslash($_GET['page'])) : '';

      // Do not loads CodeMirror in revslider page.
      if (in_array($page, array('revslider'))) {
        return;
      }

      if (!wp_script_is('csf-codemirror')) {
        wp_enqueue_script('csf-codemirror', get_template_directory_uri() . '/inc/codestar-framework/assets/libs/codemirror/lib/codemirror.min.js', array('csf'), $this->version, true);
        wp_enqueue_script('csf-codemirror-loadmode', get_template_directory_uri() . '/inc/codestar-framework/assets/libs/codemirror/addon/mode/loadmode.min.js', array('csf-codemirror'), $this->version, true);

      
      }

      if (!wp_style_is('csf-codemirror')) {
        wp_enqueue_style('csf-codemirror', get_template_directory_uri() . '/inc/codestar-framework/assets/libs/codemirror/lib/codemirror.min.css', array(), $this->version);
      }
    }
  }
}
