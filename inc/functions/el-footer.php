<?php
/*
 * @Author       : ZengHao
 * @Date         : 2023-12-27 19:02:14
 * @LastEditors  : ZengHao
 * @LastEditTime : 2024-02-25 20:14:19
 * @FilePath     : /inc/functions/el-footer.php
 * @Description  : 页脚方法
 * Copyright 2023 www.exehub.com, All Rights Reserved. 
 * 2023-12-27 19:02:14
 */

add_action('admin_footer', 'el_win_console', 99);
add_action('wp_footer', 'el_win_console', 99);
function el_win_console()
{
?>
    <script type="text/javascript">
        console.log("数据库查询：<?php echo get_num_queries(); ?>次 | 页面生成耗时：<?php echo timer_stop(0, 6) . '秒'; ?>");
    </script>
<?php
}

add_action('wp_footer', 'el_footer_code', 98);
function el_footer_code()
{
    $code = '';
    if (_opz('footcode')) {
        $code .= "<!--FOOTER_CODE_START-->\n" . _opz('footcode') . "\n<!--FOOTER_CODE_END-->\n";
    }
    if (_opz('javascriptcode')) {
        $code .= "<!--JSCODE_START-->\n";
        $code .= '<script type="text/javascript">' . _opz('javascriptcode') . '</script>';
        $code .= "\n<!--JSCODE_END-->\n";
    }
    echo $code;
}

add_action('wp_footer', 'el_win_var');
function el_win_var()
{
    $views_record = false;
    $likes_record = false;
    $highlight_dark_zt      = _opz("highlight_dark_zt", 'dracula');
    $highlight_white_zt     = _opz("highlight_zt", 'enlighter');
    // $highlight_theme        = el_get_theme_mode() == 'dark-theme' ? $highlight_dark_zt : $highlight_white_zt;
    $highlight_theme = $highlight_dark_zt;
    if (is_singular()) {
        global $post;
        $post_ID = $post->ID;
        if ($post_ID) {
            $views_record = $post_ID;
            $likes_record = $post_ID;
        }
    }

?>
    <script type="text/javascript">
        window._win = {
            views: '<?php echo $views_record; ?>',
            likes: '<?php echo $likes_record; ?>',
            ver: '<?php echo EL_VERSION; ?>',
            www: '<?php echo esc_url(home_url()); ?>',
            uri: '<?php echo esc_url(EL_THEME_URI); ?>',
            highlight_kg: '<?php echo (bool) _opz("highlight_kg"); ?>',
            highlight_hh: '<?php echo _opz("highlight_hh", true); ?>',
            highlight_btn: '<?php echo _opz("highlight_btn"); ?>',
            highlight_zt: '<?php echo $highlight_theme; ?>',
            highlight_white_zt: '<?php echo $highlight_white_zt; ?>',
            highlight_dark_zt: '<?php echo $highlight_dark_zt; ?>',
            ajax_url: '<?php echo esc_url(set_url_scheme(admin_url('admin-ajax.php'))); ?>',
        }
    </script>
<?php
}

//-----底部页脚内容------
// if (_opz('fcode_template') == 'template_1') {
//     add_action('el_footer_conter', 'el_footer_con');
// }
function el_mobile_menu_items($location = 'mobilemenu', $echo = false)
{
    $args = array(
        'container' => false,
        'container_class' => '',
        'echo' => false,
        'fallback_cb' => false,
        'items_wrap' => '<ul class="mobile-sidebar-inner">%3$s</ul>',
        'theme_location' => $location,
        'depth' => 0,
    );

    // 增加缓存机制
    $menu = null;
    $options_cache = ECache::get('theme_nav_top1');
    if (false === $options_cache) {
        $menu = wp_nav_menu($args);
        ECache::set('theme_nav_top1', $menu);
    } else {
        $menu = $options_cache;
    }
    if ($echo) {
        echo $menu;
    } else {
        return $menu;
    }
}
function el_footer_menu_items($location = 'footermenu', $echo = false)
{
    $args = array(
        'container' => false,
        'container_class' => '',
        'echo' => false,
        'fallback_cb' => false,
        'items_wrap' => '<ul class="footer-menu">%3$s</ul>',
        'theme_location' => $location,
        'depth' => 0,
    );

    // 增加缓存机制
    $menu = null;
    $options_cache = ECache::get('theme_nav_top1');
    if (false === $options_cache) {
        $menu = wp_nav_menu($args);
        ECache::set('theme_nav_top1', $menu);
    } else {
        $menu = $options_cache;
    }
    if ($echo) {
        echo $menu;
    } else {
        return $menu;
    }
}

add_action('el_footer_conter', 'el_footer_con');
function el_footer_con()
{
    $html = '';
    $html .= site_footer_wapper();
    $html .= site_footer_nav();
    echo $html;
}

function site_footer_wapper()
{
    $is_mobile = wp_is_mobile();
    $is_show_section = _opz('footer_show');
    $html = '';
    $html .= '<div class="site-footer">';
    $html .= '<div class="wrapper">';
    $html .= '<div class="site-footer-widget-in">';
    if (!$is_mobile) {
        $html .= '<section class="about-section">';
        $html .= '<div class="footer-widget widget-about">';
        $html .= '<a href="' . esc_url(get_bloginfo('url')) . '" target="_blank"><img class="footer-logo" src="' . esc_url(_opz('footer_logo_src')) . '"></a>';
        $html .= '<div class="about-widget-content">' . _opz('footer_logo_des', '请在后台设置描述') . '</div>';
        $html .= '</div>';
        $html .= '</section>';
        $html .= '<section class="nav-section">';
        $html .= site_footer_nav_custom();
        $html .= el_footer_menu_items();
        $html .= site_footer_social();
        $html .= '</section>';
        $html .= site_footer_qrcode();
    } else {
        if ($is_show_section == 'about') {
            $html .= '<section class="about-section">';
            $html .= '<div class="footer-widget widget-about">';
            $html .= '<a href="' . esc_url(get_bloginfo('url')) . '" target="_blank"><img class="footer-logo" src="' . esc_url(_opz('footer_logo_src')) . '"></a>';
            $html .= '<div class="about-widget-content">' . _opz('footer_logo_des', '请在后台设置描述') . '</div>';
            $html .= '</div>';
            $html .= '</section>';
        }
        if ($is_show_section == 'nav') {
            $html .= '<section class="nav-section">';
            $html .= site_footer_nav_custom();
            $html .= el_footer_menu_items();
            $html .= site_footer_social();
            $html .= '</section>';
        }
        if ($is_show_section == 'qrcode') {
            $html .= site_footer_qrcode();
        }
    }
    $html .= '</div>';
    $html .= '</div>';
    $html .= '</div>';

    return $html;
}

function site_footer_nav_custom()
{
    $html = '';
    $footer_links = _opz('footer_custom_link') ? _opz('footer_custom_link') : array();
    $link_h = '';
    if (count($footer_links) > 0) {
        foreach ($footer_links as $link) {
            $target = $link['target'] ? 'target="_blank"' : "";
            $link_h .= '<li class="menu-item"><a ' . $target . ' href="' . $link['url'] . '">' . $link['title'] . '</a></li>';
        }
    }
    $html .= '<div class="footer-menu">' . $link_h . '</div>';
    return $html;
}

function site_footer_qrcode()
{
    $html = '';
    $icon_h = '';
    $footer_imgs = _opz('footer_mini_imgs') ? _opz('footer_mini_imgs') : array();
    if (count($footer_imgs) > 0) {
        foreach ($footer_imgs as $img) {
            $icon_h .= '<div class="footer-miniimg" data-bs-toggle="tooltip" title="' . $img['desc'] . '">';
            $icon_h .= '<p><img class=" lazyloaded" src="' . $img['image'] . '" data-src="" alt="' . $img['desc'] . '-' . get_bloginfo('name') . '"></p>';
            $icon_h .= '<span class="qrcode-desc">' . $img['desc'] . '</span>';
            $icon_h .= '</div>';
        }
    }

    $html .= '<section class="qrcode-section">';
    $html .= '<div class="qrcode-inner">';
    $html .= $icon_h;
    $html .= '</div>';
    $html .= '</section>';
    return $html;
}

function site_footer_social()
{
    $html = '';
    $icon_h = '';
    $footer_icons = _opz('footer_custom_icon') ? _opz('footer_custom_icon') : array();
    if (count($footer_icons) > 0) {
        foreach ($footer_icons as $icon) {
            // TODO:增加弹出图片
            $target = $icon['target'] ? 'target="_blank"' : "";
            $icon_h .= '<a ' . $target . ' data-bs-toggle="tooltip" title="' . $icon['title'] . '" class="social-icon" href="' . $icon['url'] . '">' . $icon['icon'] . '</a>';
        }
    }
    $html .= '<div class="footer-social">';
    // $html .= '<a class="social-icon" href="javascript:;">'.el_icon('airplay').'</a>';
    $html .= $icon_h;
    $html .= '</div>';
    return $html;
}



function site_footer_nav()
{
    $html = '';
    $html .= '<div class="site-footer-nav">';
    $html .= '<div class="wrapper">';
    $html .= '<div class="footer-bottom">';
    $html .= '<div class="footer-bottom-left">';
    $html .= '<div class="copyright">Copyright © 2024<a href="' . esc_url(get_bloginfo('url')) . '" rel="home">&nbsp;' . get_bloginfo('name') . '</a> <a href="https://github.com/blikr/wordpress-theme-eleven" target="_blank" rel="theme">Theme By Eleven</a></div>';
    $html .= '<div class="beian">';
    $html .= '<span class="b2-dot">・</span><a rel="nofollow" target="_blank" href="https://beian.miit.gov.cn">' . _opz('beianh', '请在后台设置备案号') . '</a>';
    $html .= '</div></div>';
    $html .= '<div class="footer-bottom-right">';
    $html .= '查询 ' . get_num_queries() . ' 次，耗时 ' . timer_stop(0, 6) . ' 秒 </div>';
    $html .= '</div></div>';

    return $html;
}

add_action('el_footer_conter', 'search_modal');
function search_modal()
{
    $html = '';
    $html .= '<div class="search-popup" id="searchModal">';
    $html .= '<div class="search-popup-overlay" id="searchOverlay"></div>';
    $html .= '<div class="search-popup-inner ">';
    $html .= ' <div class="search-popup-content block p-3 p-xl-4">';
    $html .= '<form role="search" class="search-popup-form" method="get" action="' . home_url() . '">';
    $html .= '<input id="SearchInput" class="search-popup-input form-control" type="text" placeholder="请输入搜索关键词" name="s">';
    $html .= '</form>';
    // $html .= '<ul class="search-tags mt-3">';
    // $html .= '<li class="hot">';
    // $html .= '<a href="" target="_blank" title="GRACE主题">';
    // $html .= '<div class="category-icon iconfont icon-biaoqian me-1"></div>';
    // $html .= '<div class="category-name"> GRACE主题<small class="font-number">16</small></div>';
    // $html .= '</a></li>';
    // $html .= '</ul>';
    $html .= '</div>';
    // $html .= '<button class="search-popup-close btn btn-icon" id="searchHide"><i data-feather="x-circle"></i></button>';
    $html .= '<div class="search-popup-close " id="searchHide"><i data-feather="x"></i></div>';
    $html .= '</div></div>';
    echo $html;
}

add_action('el_footer_conter', 'mobile_menu_modal');
function mobile_menu_modal()
{
    $html = '';
    $html .= '<div class="mobile-overlay"></div>';
    $html .= '<div class="mobile-sidebar">';
    $html .= '<div class="mobile-sidebar-inner">';
    $html .= '<div class="mobile-sidebar-header text-end p-3">';
    $html .= '<span data-bs-theme-value="dark" class="change-mode mode-dark"><i data-feather="moon"></i></span>';
    $html .= '<span data-bs-theme-value="light" class="change-mode mode-light d-none"><i data-feather="sun"></i></span>';
    $html .= '<span class="close-mobile_menu"><i data-feather="x"></i></span>';
    $html .= '</div>';
    $html .= '<div class="mobile-sidebar-menu">';
    $html .= el_mobile_menu_items();
    $html .= '</div>';
    $html .= '<div class="py-5"></div>';
    $html .= '</div></div>';
    echo $html;
}

add_action('el_footer_conter', 'el_share_modal');
function el_share_modal()
{
    $post_link = false;
    if (is_singular()) {
        global $post;
        $permalink = get_permalink($post->ID);
        if ($permalink) {
            $post_link = $permalink;
        }
    }
    $post_ID       = get_queried_object_id();
    $current_url = home_url(add_query_arg(array()));
    $post_title = get_the_title();
    $html = '';

    $html .= '<div class="el-popup share-popup el-popup-center el-popup-md">';
    $html .= '<div class="el-popup-overlay share-btn"></div>';
    $html .= '<div class="el-popup-body">';
    $html .= '<div class="el-popup-close share-btn"><i data-feather="x"></i></div>';
    $html .= '<div class="el-popup-content">';
    $html .= '<div class="row g-4 text-center justify-content-center py-2 py-md-4">';

    $html .= '<div class="col-6 col-md-3">';
    $html .= '<a href="javascript:;" data-clipboard-text="' . $post_link . '" class="copy-permalink btn btn-light btn-icon btn-md btn-rounded mb-1">';
    $html .= '<span><i data-feather="copy"></i></span>';
    $html .= '</a>';
    $html .= '<div class="text-secondary text-xs mt-1"> 复制链接 </div>';
    $html .= '</div>';

    // $html .= '<div class="col-6 col-md-3">';
    // $html .= '<a href="javascript:;" id="btn-bigger-cover" class="btn-bigger-cover btn btn-light btn-icon btn-md btn-rounded mb-1" data-id="1960">';
    // $html .= '<span><i data-feather="image"></i></span>';
    // $html .= '</a>';
    // $html .= '<div class="text-secondary text-xs mt-1"> 文章海报</div>';
    // $html .= '</div>';

    $html .= '<div class="col-6 col-md-3 share-weibo" data-share-title="'.$post_title.'" date-share-url="'.$current_url.'">';
    $html .= '<a href="" target="_blank" class="btn btn-light btn-icon btn-md btn-rounded btn-weibo mb-1">';
    $html .= '<span><i data-feather="hash"></i></span>';
    $html .= '</a>';
    $html .= '<div class="text-secondary text-xs mt-1"> 新浪微博</div>';
    $html .= '</div>';

    // $html .= '<div class="col-6 col-md-3 share-wechat">';
    // $html .= '<a href="javascript:;" class="weixin single-popup btn btn-light btn-icon btn-md  btn-rounded btn-weixin mb-1">';
    // $html .= '<span><i data-feather="aperture"></i></span>';
    // $html .= '</a>';
    // $html .= '<div class="text-secondary text-xs mt-1"> 微信</div>';
    // $html .= '</div>';

    $html .= '<div class="col-6 col-md-3 share-qq" data-share-title="'.$post_title.'" date-share-url="'.$current_url.'">';
    $html .= '<a href="javascript:;" class="weixin single-popup btn btn-light btn-icon btn-md  btn-rounded btn-weixin mb-1">';
    $html .= '<span><svg t="1708853906000" class="icon" viewBox="0 0 1024 1024" version="1.1" xmlns="http://www.w3.org/2000/svg" p-id="4205" width="36" height="36"><path d="M512 64C264.6 64 64 264.6 64 512s200.6 448 448 448 448-200.6 448-448S759.4 64 512 64z m210.5 612.4c-11.5 1.4-44.9-52.7-44.9-52.7 0 31.3-16.2 72.2-51.1 101.8 16.9 5.2 54.9 19.2 45.9 34.4-7.3 12.3-125.6 7.9-159.8 4-34.2 3.8-152.5 8.3-159.8-4-9.1-15.2 28.9-29.2 45.8-34.4-35-29.5-51.1-70.4-51.1-101.8 0 0-33.4 54.1-44.9 52.7-5.4-0.7-12.4-29.6 9.4-99.7 10.3-33 22-60.5 40.2-105.8-3.1-116.9 45.3-215 160.4-215 113.9 0 163.3 96.1 160.4 215 18.1 45.2 29.9 72.8 40.2 105.8 21.7 70.1 14.6 99.1 9.3 99.7z" p-id="4206"></path></svg></span>';
    $html .= '</a>';
    $html .= '<div class="text-secondary text-xs mt-1"> QQ</div>';
    $html .= '</div>';

    $html .= '</div>';
    $html .= '</div></div></div>';
    echo $html;
}

add_action('el_footer_conter', 'el_share_cover');
function el_share_cover()
{
    $html = '';
    $html .= '<div class="el-popup el-popup-center cover-popup">';
    $html .= '<div class="el-popup-overlay share-cover" style="background:rgba(0,0,0,0.5);"></div>';
    $html .= '<div class="el-popup-body">';
    $html .= '<div class="el-popup-close share-cover"><i data-feather="x"></i></div>';
    $html .= '<div class="el-popup-content">';
    $html .= '<div class="cover-image">';
    $html .= '<img src="">';
    $html .= '</div>';
    $html .= '<div class="cover-share">';
    $html .= '<a class="weibo" href="" data-toggle="tooltip" target="_blank" title="分享至微博">' . el_icon('archive') . '</a>';
    $html .= '<a class="qq" href="" data-bs-toggle="tooltip" target="_blank" title="分享至 QQ">' . el_icon('aperture') . '</a>';
    $html .= '<a href="" download="" data-bs-toggle="tooltip" title="下载海报"></a>';
    $html .= '</div>';
    $html .= '<div class="cover-text">分享朋友圈请先下载海报</div>';
    $html .= '</div></div></div>';
    echo $html;
}
if (_opz('slider_btn')) {
    add_action('el_footer_conter', 'el_aside_bar');
}
function el_aside_bar()
{
    $html = '';
    $html .= '<div class="aside-container">';
    $html .= '<div class="aside-bar">';
    $html .= '<div class="bar-middle">';

    $html .= el_custom_slider_btn();

    $html .= '<div class="bar-footer">';
    if (_opz('quick_search_btn')) {
        $html .= '<div class="bar-item">' . el_icon('search') . '<span class="bar-item-desc">快捷搜索</span></div>';
    }
    if (_opz('go_top_btn')) {
        $html .= '<div class="bar-item gotop">' . el_icon('corner-right-up') . '<span class="bar-item-desc">返回顶部</span></div>';
    }
    $html .= '</div>';

    $html .= '</div>';
    $html .= '</div>';
    $html .= '</div>';
    echo $html;
}

function el_custom_slider_btn()
{
    $btn_html ='';
    $custom_btn = _opz('slider_custom_btn')?_opz('slider_custom_btn'):array();
    if (count($custom_btn) > 0) {
        foreach ($custom_btn as $btn) {
            $taget = $btn['target']?'target="_blank"':'';
            // print_r($btn['icon']);
            $btn_html .= '<div class="bar-item">' . el_icon($btn['icon']) . '<a href="'.$btn['url'].'" '.$taget.'><span  class="bar-item-desc">'.$btn['title'].'</span></a></div>';
        }
    }
    $html = '<div class="bar-normal">';
    $html .= $btn_html;
    $html .= '</div>';
    // print_r($custom_btn);
    return $html;
}
