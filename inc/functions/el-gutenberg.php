<?php
// 禁用WP Editor Google字体css
function el_remove_gutenberg_styles($translation, $text, $context, $domain)
{
    if ('Google Font Name and Variants' != $context || 'Noto Serif:400,400i,700,700i' != $text) {
        return $translation;
    }
    return 'off';
}
add_filter('gettext_with_context', 'el_remove_gutenberg_styles', 10, 4);
// 古腾堡编辑器扩展
function el_block()
{
    wp_register_script(
        'el_block',
        EL_ASSETS . 'js/gutenberg-extend.min.js',
        array('wp-blocks', 'wp-element', 'wp-rich-text'), EL_VERSION
    );

    wp_register_style(
        'el_block',
        EL_ASSETS . 'css/editor-style.min.css',
        array('wp-edit-blocks'), EL_VERSION
    );

    wp_register_style(
        'font_awesome',
        EL_ASSETS . 'css/font-awesome.min.css',
        array('el_block'), EL_VERSION
    );

    register_block_type('eleven/block', array(
        'editor_script' => 'el_block',
        'editor_style'  => 'el_block',
    ));
}

if (function_exists('register_block_type')) {
    add_action('init', 'el_block');
    $wp_version = get_bloginfo('version', 'display');

    if (version_compare('5.7.9', $wp_version) == -1) {
        add_filter('block_categories_all', function ($categories, $post) {
            return array_merge(
                array(
                    array(
                        'slug'  => 'el_block_cat',
                        'title' => __('Eleven 主题模块', 'el-blocks'),
                    ),
                ),
                $categories
            );
        }, 10, 2);
    } else {
        add_filter('block_categories', function ($categories, $post) {
            return array_merge(
                array(
                    array(
                        'slug'  => 'el_block_cat',
                        'title' => __('Eleven 主题模块', 'el-blocks'),
                    ),
                ),
                $categories
            );
        }, 10, 2);
    }
}