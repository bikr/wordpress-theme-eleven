<?php
/**
 * @param $name
 * @param $callback callable
 * @param $public
 * @return void
 */
function pk_ajax_register($name, $callback, $public = false)
{
    add_action('wp_ajax_' . $name, $callback);
    if ($public) {
        add_action('wp_ajax_nopriv_' . $name, $callback);
    }
}

function pk_ajax_url($action,$args=[]){
    $url = admin_url('admin-ajax.php?action='.$action);
    if(!empty($args)){
        $url .= '&'.http_build_query($args);
    }
    return $url;
}

// 评论验证码
// request url: {host}/wp-admin/admin-ajax.php?action=puock_comment_captcha
function pk_captcha()
{
    $type = $_GET['type'] ?? '';
    if (!in_array($type, ['comment', 'login', 'register', 'forget-password'])) {
        wp_die();
    }
    $width = $_GET['w'];
    $height = $_GET['h'];
    include_once EL_THEME_DIR.'/inc/core/captcha.php';
    $captcha = new CaptchaBuilder();
    $captcha->initialize([
        'width' => intval($width),     // 宽度
        'height' => intval($height),     // 高度
        'curve' => true,   // 曲线
        'noise' => 1,   // 噪点背景
        'fonts' => [EL_THEME_DIR . '/assets/fonts/G8321-Bold.ttf']       // 字体
    ]);
    $result = $captcha->create();
    $text = $result->getText();
    pk_session_call(function () use ($text, $type) {
        $_SESSION[$type . '_vd'] = $text;
    });
    $result->output();
    wp_die();
}

pk_ajax_register('pk_captcha', 'pk_captcha', true);

function pk_captcha_url($type, $width = 100, $height = 40)
{
    return admin_url() . 'admin-ajax.php?action=pk_captcha&type=' . $type . '&w=' . $width . '&h=' . $height;
}

function pk_captcha_validate($type, $val, $success_clear = true)
{
    $res = false;
    pk_session_call(function () use ($type, $val, $success_clear, &$res) {
        if (isset($_SESSION[$type . '_vd']) && $_SESSION[$type . '_vd'] == $val) {
            $res = true;
            if ($success_clear) {
                unset($_SESSION[$type . '_vd']);
            }
        }
    });
    return $res;
}

function pk_open_session()
{
    session_start();
}

function pk_wclose_session()
{
    session_write_close();
}


function pk_session_call($function)
{
    pk_open_session();
    try {
        $function();
    } finally {
        session_write_close();
    }
}
function pk_skeleton($type = 'default', $line = 1)
{
    switch ($type) {
        case 'comment':
        {
            $out = '<div class="pk-skeleton _comment"><div class="_h"><div class="_avatar"></div>
                <div class="_info"><div class="_name"></div><div class="_date"></div></div></div><div class="_text"><div></div><div></div><div></div></div></div>';
            break;
        }
        default:
        {
            $out = '<div class="pk-skeleton _default"><div></div><div></div><div></div><div></div></div>';
        }
    }
    if ($line > 1) {
        $out = str_repeat($out, $line);
    }
    return $out;
}

function pk_comment_callback($comment, $args, $depth)
{
    global $authordata;
    $GLOBALS['comment'] = $comment;
    $author_cat_comment = get_post_meta($comment->comment_post_ID, 'author_cat_comment', true) == 'true';
    $is_author = $authordata->ID == get_current_user_id();
    if ($comment->comment_parent == 0) {
        $pccci_key = 'pk_comment_callback_cur_id';
        $pccci = @$GLOBALS[$pccci_key];
        if (!empty($pccci)) {
            echo '</div>';
        }
        $GLOBALS[$pccci_key] = $comment->comment_ID;
    }
    ?>
<div id="comment-<?php comment_ID() ?>" class="post-comment">
    <div class="info">
        <?php if (_opz('comment_show_avatar',true)): ?>
            <div>
                <?php if (_opz('basic_img_lazy_a')): ?>
                    <img src="<?php echo get_avatar_url($comment->comment_author_email, 64);?>"
                    data-original="<?php echo get_avatar_url($comment->comment_author_email, 64); ?>"
                         class="avatar avatar-64 photo md-avatar lazy" width="60" height="60">
                <?php else: ?>
                    <?php echo get_avatar($comment, 64, '', '', array('class' => 'md-avatar')) ?>
                <?php endif; ?>
            </div>
        <?php endif; ?>
        <div class="<?php if (_opz('comment_show_avatar',true)) {
            echo 'ml-2';
        } ?> two-info">
            <div class="puock-text ta3b">
                <span class="t-md puock-links"><?php pk_comment_author_url() ?></span>
                <?php if (_opz('comment_level')) {
                    pk_the_author_class();
                } ?>
            </div>
            <div class="t-sm c-sub">
                <span><?php comment_date('Y-m-d H:i:s') ?></span>
                <?php if ($comment->comment_approved == '1' && (!$author_cat_comment || $is_author)) : ?>
                    <a id="comment-reply-<?php comment_ID() ?>" data-id="<?php comment_ID() ?>"
                       class="hide-info animated bounceIn c-sub-a t-sm ml-1 comment-reply"
                       href="javascript:void(0)" title="回复此评论"><i class="fa fa-share-from-square"></i>
                        <span class="comment-reply-text">回复</span></a>
                <?php endif; ?>
            </div>
        </div>
    </div>
    <div class="content">
        <div class="content-text t-md mt10 puock-text" <?php if (!_opz('comment_show_avatar',true)) {
            echo 'style="margin-left:0"';
        } ?>>
            <?php comment_text() ?>
            <?php if ($comment->comment_approved == '0') : ?>
                <p class="c-sub mt-1"><i class="fa fa-warning mr-1"></i>您的评论正在等待审核！</p>
            <?php endif; ?>

            <div class="comment-os c-sub">
                <?php
                if (_opz('comment_show_ua', false)):
                    $commentUserAgent = \donatj\UserAgent\parse_user_agent($comment->comment_agent);
                    $commentOsIcon = pk_get_comment_ua_os_icon($commentUserAgent['platform']);
                    $commentBrowserIcon = pk_get_comment_ua_os_icon($commentUserAgent['browser']);
                    echo "<span class='mt10' title='{$commentUserAgent['platform']}'><i class='$commentOsIcon'></i>&nbsp;<span>{$commentUserAgent['platform']}&nbsp;</span></span>";
                    echo "<span class='mt10' title='{$commentUserAgent['browser']} {$commentUserAgent['version']}'><i class='$commentBrowserIcon'></i>&nbsp;<span>{$commentUserAgent['browser']}</span></span>";
                endif;
                ?>
                <?php
                if (_opz('comment_show_ip', false)) {
                    if (!_opz('comment_dont_show_owner_ip') || (_opz('comment_dont_show_owner_ip') && $comment->user_id != 1)) {
                        $ip = pk_get_ip_region_str($comment->comment_author_IP);
                        echo "<span class='mt10' title='IP'><i class='fa-solid fa-location-dot'></i>&nbsp;$ip</span>";
                    }
                }
                ?>
            </div>
        </div>
        <div class="comment-box-reply d-none" id="comment-box-<?php comment_ID() ?>"></div>
    </div>
    <?php
}

//评论者链接
function pk_comment_author_url($comment_ID = 0)
{
    global $comment;
    $attr = '';
    if (!empty($comment) && $comment->user_id != 0) {
        $url = get_author_posts_url($comment->user_id);
    } else {
        $url = get_comment_author_url($comment_ID);
        $attr = "target='_blank' rel='external nofollow'";
    }
    $author = get_comment_author($comment_ID);
    echo empty($url) ? $author : "<a " . $attr . " href='" . pk_go_link($url) . "' class='url'>$author</a>";
}

//评论回复通知
if (_opz('comment_mail_notify')) {
    add_action('comment_unapproved_to_approved', 'comment_mail_notify');
    add_action('comment_post', 'comment_mail_notify');
}

function pk_the_author_class($echo = true, $in_comment = null)
{
    global $wpdb, $comment;
    if (!$comment) {
        $comment = $in_comment;
    }
    if ($comment->user_id == '1') {
        $res = '<span class="t-sm text-danger"><i class="fa-regular fa-gem mr-1"></i>' . __('博主', "el_language") . '</span>';
    } else {
        $comment_author_email = $comment->comment_author_email;
        $cache_key = sprintf('author_comments_%s', md5($comment_author_email));
        $author_count = ECache::get($cache_key);
        if (false === $author_count) {
            $query = $wpdb->prepare("SELECT count(1) as c FROM $wpdb->comments WHERE comment_author_email = %s", $comment_author_email);
            $author_count = $wpdb->get_results($query)[0]->c;
            ECache::set($cache_key, $author_count);
        }
        $res = pk_the_author_class_out($author_count);
    }
    if (!$echo) {
        return $res;
    }
    echo $res;
}

//获取评论等级
function pk_the_author_class_out($count)
{
    if ($count <= 0) {
        return '';
    }
    switch ($count) {
        case $count >= 1 && $count < 20:
            $level = 1;
            break;
        case $count >= 20 && $count < 40:
            $level = 2;
            break;
        case $count >= 40 && $count < 60:
            $level = 3;
            break;
        case $count >= 60 && $count < 80:
            $level = 4;
            break;
        case $count >= 120 && $count < 120:
            $level = 5;
            break;
        case $count >= 140 && $count < 140:
            $level = 6;
            break;
        case $count >= 160 && $count < 160:
            $level = 7;
            break;
        default:
            return '';
    }
    return '<span class="t-sm c-sub"><i class="fa-regular fa-gem mr-1"></i>' . __('评论达人', "el_language") . ' LV.' . $level . '</span>';
}

//跳转链接
function pk_go_link($url, $name = '')
{
    if (pk_is_cur_site($url)) {
        return $url;
    }
    $url = EL_THEME_URI . '/inc/go.php?to=' . base64_encode($url);
    if (!empty($name)) {
        $url .= '&name=' . base64_encode($name);
    }
    return $url;
}
//检测链接是否属于本站
function pk_is_cur_site($url)
{
    if (strpos($url, home_url()) === 0) {
        return true;
    }
    return false;
}

function pk_get_ip_region_str($ip)
{
    $ip2_instance = @$GLOBALS['ip2_region'];
    if (!$ip2_instance) {
        $ip2_instance = new \Ip2Region();
        $GLOBALS['ip2_region'] = $ip2_instance;
    }
    try {
        $s = $ip2_instance->memorySearch($ip);
    } catch (Exception $e) {
        return '未知';
    }
    if (str_contains($s['region'], '内网IP')) {
        return '内网IP';
    }
    $region = explode('|', $s['region']);
    $res = '';
    foreach ($region as $item) {
        if (str_starts_with($item, '0')) {
            continue;
        }
        $res .= $item;
    }
    return $res;
}

function pk_oauth_platform_count()
{
    $count = 0;
    // $oauth_list = pk_oauth_list();
    $oauth_list = array();
    foreach ($oauth_list as $key => $val) {
        if (_opz('oauth_' . $key)) {
            $count++;
        }
    }
    return apply_filters('pk_oauth_platform_count', $count);
}