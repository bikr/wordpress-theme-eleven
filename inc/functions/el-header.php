<?php
/*
 * @Author       : ZengHao
 * @Date         : 2024-01-10 08:36:19
 * @LastEditors  : ZengHao
 * @LastEditTime : 2024-02-21 23:46:04
 * @FilePath     : /inc/functions/el-header.php
 * @Description  : 
 * Copyright 2024 www.exehub.net, All Rights Reserved. 
 * 2024-01-10 08:36:19
 */

// 注册菜单位置
if (function_exists('register_nav_menus')) {
    register_nav_menus(
        array(
            'topmenu' => __('PC端顶部菜单', 'el_language'),
            'mobilemenu' => __('移动端菜单', 'el_language'),
            'footermenu' => __('页脚菜单', 'el_language'),
        )
    );
}

function el_menu_items($location = 'topmenu', $echo = false)
{
    $args = array(
        'container' => false,
        'container_class' => '',
        'echo' => false,
        'fallback_cb' => false,
        'items_wrap' => '<ul class="navbar-nav">%3$s</ul>',
        'theme_location' => $location,
    );
    if (!wp_is_mobile()) {
        $args['depth'] = 0;
    }

    // $menu = wp_nav_menu($args);
    $menu = null;
    // 增加缓存机制
    $options_cache = ECache::get('theme_nav_top');
    if (false === $options_cache) {
        $menu = wp_nav_menu($args);
        ECache::set('theme_nav_top', $menu);
    } else {
        $menu = $options_cache;
    }

    if (!$menu && is_super_admin()) {
        $menu = '<ul class="flex items-center"><li><a target="_blank" href="' . admin_url('nav-menus.php') . '" class="text-gray-900 dark:text-white hover:underline">添加导航菜单</a></li></ul>';
    }
    if ($echo) {
        echo $menu;
    } else {
        return $menu;
    }
}

function el_header()
{
    $html = '';
    $mobile_header_layout = _opz('mobile_header_layout','center');
    $html .= '<header class="site-navbar mb-3"><nav class="navbar navbar-expand-xl navbar-top '.$mobile_header_layout.'">';
    $html .= '<div class="container">';
    $html .= '<div class="navbar-header">';
    $html .= navbar_html();
    $html .= '<button class="navbar-toggler nav-menu p-2" type="button"><i data-feather="menu"></i></button>';
    $html .= '<button class="navbar-toggler nav-search p-2" type="button"><i data-feather="search"></i></button>';
    $html .= '</div>';
    $html .= '<div class="collapse navbar-collapse">';
    $html .= el_menu_items();
    $html .= header_right();
    $html .= '</div>';
    $html .= '</div></nav></header>';
    echo $html;
}


function navbar_html(){
    $ttitle    = _opz('hometitle') ? _opz('hometitle') : get_bloginfo('name') . (get_bloginfo('description') ? _get_delimiter() . get_bloginfo('description') : '');
    $html ='<div class="navbar-brand">';
    $html .= '<a href="'.esc_url(get_bloginfo('url')) .'" title="'.$ttitle.'" rel="home">';
    $html .= '<img src="'.esc_url(_opz('logo_src')).'" class="logo-light" alt="'.$ttitle.'">';
    $html .= '<img src="'.esc_url(_opz('logo_src_dark')).'" class="logo-dark d-none" alt="'.$ttitle.'">';
    $html .= '</a></div>';
    return $html;
}

function header_right(){
    $html ='<ul class="nav-right navbar-toggl">';
    $html .= '<li class="change-mode mode-dark" data-bs-theme-value="dark" data-bs-toggle="tooltip" data-bs-placement="bottom" title="切换夜间模式"><i data-feather="moon"></i></li>';
    $html .= '<li class="change-mode mode-light d-none" data-bs-theme-value="light"  data-bs-toggle="tooltip" data-bs-placement="bottom" title="切换日间模式"><i data-feather="sun"></i></li>';
    $html .= '<li id="searchToggle"><i data-feather="search"></i></li>';
    $html .= '</ul>';
    return $html;
}