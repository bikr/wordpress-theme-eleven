<?php 
/*
 * @Author       : ZengHao
 * @Date         : 2023-12-23 14:14:26
 * @LastEditors  : ZengHao
 * @LastEditTime : 2024-02-01 23:13:36
 * @FilePath     : /inc/functions/functions.php
 * @Description  : 
 * Copyright 2023 www.exehub.com, All Rights Reserved. 
 * 2023-12-23 14:14:26
 */
//载入文件
$functions = array(
    'el-utils',
    'el-theme',
    'el-optimize',
    'el-header',
    'el-footer',
    'el-seo',
    'el-email',
    'el-post',
    'el-single',
    'el-widget',
    'el-comment',
    'el-editor',
);

foreach ($functions as $function) {
    $path = 'inc/functions/' . $function . '.php';
    require get_theme_file_path($path);
}

function el_icon($feather=''){
    return '<i data-feather="'.$feather.'"></i>';
}

/**
 * 无序化数组
 */
function random_array( &$array ) {
    if ( ! is_array( $array ) || empty( $array ) ) {
        return false;
    }

    $keys = array_keys( $array );
    shuffle( $keys );

    $new = array();
    foreach ( (array) $keys as $key ) {
        $new[ $key ] = $array[ $key ];
    }

    $array = $new;

    return true;
}

//数组按一个值从新排序
function arraySort($arrays, $sort_key, $sort_order = SORT_DESC, $sort_type = SORT_NUMERIC)
{
    if (is_array($arrays)) {
        foreach ($arrays as $array) {
            $key_arrays[] = $array->$sort_key;
        }
    } else {
        return false;
    }
    array_multisort($key_arrays, $sort_order, $sort_type, $arrays);
    return $arrays;
}


/**
 * 设置rel
 */
function set_rel_attribut() {
    $rel = _opz('auto_tag_rel')=='nofollow'?'no-nofollow':'tag';
    if ( ! empty( $rel ) ) {
        $rel = 'rel="' . $rel . '"'; // Add HTML Tag
    }

    return $rel;
}
function el_yiyan($class = 'yiyan-box', $before = '', $after = '')
{
    $yiyan = '<div class="mb-3 ' . $class . '">' . $before . '<div data-bs-toggle="tooltip" title="点击切换一言" class="yiyan"></div>' . $after . '</div>';
    echo $yiyan;
}
