<?php

// https://5i21.cn/index.php/archives/4381/
//文章页主内容
function el_single()
{
    el_single_header();
    do_action('el_single_before'); //添加钩子
    echo '<article class="article mb-3">';
    el_single_content();
    echo '</article>';
    do_action('el_single_after'); //添加钩子
}

/**
 * @description: 获取文章的顶部封面
 * @param {*}
 * @return {*}
 */
function el_single_header()
{
    $breadcrumbs = el_get_breadcrumbs();
    $cover = el_single_cover();
    // echo $cover ? $cover : $breadcrumbs;
    echo $breadcrumbs . $cover;
}

// 导航菜单
function el_get_breadcrumbs()
{
    if (!is_single() || !_opz('breadcrumbs_single_s', true)) {
        return;
    }
    $lin = '<ul class="breadcrumb"><li><a href="' . get_bloginfo('url') . '"><span class="home-icons"><svg xmlns="http://www.w3.org/2000/svg" width="12px" height="12px" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-home"><path d="M3 9l9-7 9 7v11a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2z"></path><polyline points="9 22 9 12 15 12 15 22"></polyline></svg></span>' . (!_opz('breadcrumbs_home_text', true) ? get_bloginfo('name') : '首页') . '</a></li>';
    $categorys = get_the_category();
    if ($categorys) {
        $category = $categorys[0];
        $lin      .= '<li>' . get_category_parents($category->term_id, true, ' </li>');
    }
    $lin .= '<li> ' . (!_opz('breadcrumbs_single_text', true) ? get_the_title() : '正文') . '</li>';
    $lin .= '</ul>';
    return $lin;
}

function el_single_cover()
{
    $html = '';
    $post_cover = el_post_thumbnail();
    if ($post_cover) {
        $html .= '<div class="article-cover">';
        $html .= '<img fetchpriority="high" data-original="' . $post_cover  . '" src="' . esc_url(LAZY_COVER) . '" class="lazyload"/>';
        // $html .= '<img src="' . $post_cover . '" alt=""/>';
        $html .= '</div>';
    }
    return $html;
}

function el_single_content()
{
    el_single_box_header();
    do_action('el_single_box_content_before'); //添加钩子
    el_single_box_content();
    do_action('el_single_box_content_after'); //添加钩子
}

/**
 * @description: 文章页文章头部
 * @param {*}
 * @return {*}
 */
function el_single_box_header($post = null)
{
    if (!is_object($post)) {
        $post = get_post($post);
    }
    if (!isset($post->ID)) {
        return;
    }
    $user_id = $post->post_author;

    $user = get_userdata($user_id);
    $author_name = '';
    $author_url = el_get_user_home_url($user_id);
    $dot = '<span class="dot">•</span>';
    if (isset($user->display_name)) {
        $author_name = '<li class="meta-author">';
        $author_name .= '<span class="post-icons"><svg xmlns="http://www.w3.org/2000/svg" width="16px" height="16px"viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round"stroke-linejoin="round" class="feather feather-user"><path d="M20 21v-2a4 4 0 0 0-4-4H8a4 4 0 0 0-4 4v2"></path><circle cx="12" cy="7" r="4"></circle></svg></span>';
        $author_name .= '<span class="sr-only">博主：</span><a target="_blank" class="meta-value" href="' . $author_url . '" rel="author">' . $user->display_name . '</a></li>';
    }
    // 分类信息
    $category_h = '';
    $categorys = get_the_category();
    if ($categorys) {
        $category = $categorys[0];
        $category_h .= '<li class="meta-categories">';
        $category_h .= '<span class="post-icons"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-book-open"><path d="M2 3h6a4 4 0 0 1 4 4v14a3 3 0 0 0-3-3H2z"></path><path d="M22 3h-6a4 4 0 0 0-4 4v14a3 3 0 0 1 3-3h7z"></path></svg></span>';
        $category_h .= '<span class="sr-only">分类：</span>';
        $category_h .= '<span class="meta-value" id="post_category"><a target="_blank" href="' . get_category_link($category->term_id) . '">' . $category->name . '</a></span></li>';
    }
    // 阅读量
    $meta = '<li class="meta-views">';
    $meta .= '<span class="post-icons"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-eye"><path d="M1 12s4-8 11-8 11 8 11 8-4 8-11 8-11-8-11-8z"></path><circle cx="12" cy="12" r="3"></circle></svg></span>';
    $meta .= '<span class="meta-value">' . get_post_view_count('', '', $post->ID) . '&nbsp;次浏览</span></li>';
    // 评论信息
    $comment_num = get_comments_number($post->ID) == 0 ? '暂无评论' : get_comments_number($post->ID);
    $comment = '<li class="meta-comments">';
    $comment .= '<span class="post-icons"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-message-circle"><path d="M21 11.5a8.38 8.38 0 0 1-.9 3.8 8.5 8.5 0 0 1-7.6 4.7 8.38 8.38 0 0 1-3.8-.9L3 21l1.9-5.7a8.38 8.38 0 0 1-.9-3.8 8.5 8.5 0 0 1 4.7-7.6 8.38 8.38 0 0 1 3.8-.9h.5a8.48 8.48 0 0 1 8 8v.5z"></path></svg></span>';
    $comment .= '<a class="meta-value" href="#comments">' . $comment_num . '</a></li>';

    $html = '<div class="article-header mb-3">';
    $html .= '<h1 class="article-title"> <a href="' . get_permalink() . '">' . get_the_title() . '</a></h1>';
    $html .= '<ul class="entry-meta text-muted list-inline post-head-icon">';
    $html .= $author_name;
    $html .= el_get_post_time_tooltip();
    $html .= $category_h;
    $html .= $meta;
    $html .= $comment;
    $html .= '</ul></div>';
    $html .= '<div class="end-line"><span class="end-text"> 正文开始 </span></div>';
    echo $html;
}

//获取文章时间的显示
function el_get_post_time_tooltip($post = null)
{
    $modified_time = get_the_modified_time('U', $post);
    $time          = get_the_time('U', $post);
    $time_html = '<li class="meta-date">';
    $time_html .= '<span class="post-icons"><svg xmlns="http://www.w3.org/2000/svg" width="16px" height="16px" viewBox="0 0 24 24"fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"class="feather feather-clock"><circle cx="12" cy="12" r="10"></circle><polyline points="12 6 12 12 16 14"></polyline></svg></span>';
    if ($modified_time > $time) {
        //更新时间大于发布时间
        $time_html .= '<span data-toggle="tooltip" data-placement="bottom" title="' . date('Y年m月d日 H:i', $time) . '发布" class="sr-only">更新时间：</span><time class="meta-value">' . el_get_time_ago($modified_time) . '</time>';
    } else {
        $time_html .= '<span  data-toggle="tooltip" data-placement="bottom" title="' . date('Y年m月d日 H:i', $time) . '发布" class="sr-only">发布时间：</span><time class="meta-value">' . el_get_time_ago($time) . '</time>';
    }
    $time_html .= '</li>';

    return $time_html;
}

/**
 * @description: 文章页内容
 * @param {*}
 * @return {*}
 */
function el_single_box_content()
{
    global $post;
    $show_nav = el_is_show_posts_nav();
    $is_max_height = get_post_meta($post->ID, "article_maxheight_xz", true);
    $max_height_style = '';
    $max_height_class = '';
    $show_nav_data = '';
    if ($show_nav) {
        $show_nav_data .= 'data-nav="posts"';
    }
    //文章高度限制
    if (_opz('article_maxheight_kg') || $is_max_height) {
        $max_height_class .= ' limit-height';
        $max_height = (int) _opz('article_maxheight');
        $max_height = $max_height ?: 1000;
        $max_height_style = ' style="max-height:' . $max_height . 'px;" data-maxheight="' . ($max_height - 80) . '"';
    }
?>
    <div class="article-content">
        <?php el_single_content_header(); ?>
        <?php echo _opz('post_front_content'); ?>
        <div <?php echo $show_nav_data; ?><?php echo $max_height_style; ?>class="theme-box wp-posts-content<?php echo $max_height_class; ?>">
            <?php

            do_action('el_posts_content_before', $post); //添加钩子
            the_content();

            //文章分页
            wp_link_pages(
                array(
                    'before' => '<p class="text-center post-nav-links radius8 padding-6">',
                    'after' => '</p>',
                )
            );
            do_action('el_posts_content_after', $post); //添加钩子
            echo _opz('post_after_content');
            ?>
        </div>
        <?php el_single_content_footer($post); ?>
    </div>
    <?php
    el_single_content_footer_action();
    ?>
<?php }

function el_single_content_header()
{
    if (_opz('yiyan_single_content_header')) {
        el_yiyan('article-yiyan theme-box text-center radius8 main-shadow yiyan-box');
    }
}

/**
 * @description: 文章页文字导航显示判断
 * @param {*}
 * @return {*}
 */
function el_is_show_posts_nav()
{
    global $post;
    $show_nav = get_post_meta($post->ID, "no_article-navs", true);
    if (_opz('article_nav') && !($show_nav)) {
        return true;
    }
    return false;
}

// 获取分类标签
function el_get_cat_tags($class = 'but', $before = '', $after = '', $count = 0)
{
    $category = get_the_category();
    $cat      = '';
    if (!empty($category[0])) {
        $ii = 0;
        foreach ($category as $category1) {
            $ii++;
            $cls = array('c-blue', 'c-yellow', 'c-green', 'c-purple', 'c-red', 'c-blue', 'c-yellow', 'c-green', 'c-purple', 'c-red', 'c-blue', 'c-yellow', 'c-green', 'c-purple', 'c-red', 'c-blue', 'c-yellow', 'c-green', 'c-purple', 'c-red', 'c-blue', 'c-yellow', 'c-green', 'c-purple', 'c-red', 'c-blue', 'c-yellow', 'c-green', 'c-purple', 'c-red', 'c-blue', 'c-yellow', 'c-green', 'c-purple', 'c-red');
            $cat .= '<a class="' . $class . ' ' . $cls[$ii - 1] . '" title="查看更多分类文章" href="' . get_category_link($category1->term_id) . '">' . $before . $category1->cat_name . $after . '</a>';
            if ($count && $ii == $count) {
                break;
            }
        }
    }
    return $cat;
}

// 获取文章标签
function el_get_posts_tags($class = 'but', $before = '', $after = '', $count = 0)
{
    global $post;
    $tags = get_the_tags($post->ID);
    return el_get_tags($tags, $class, $before, $after, $count);
}

// 获取标签
function el_get_tags($tags, $class = 'but', $before = '', $after = '', $count = 0, $ajax_replace = false)
{
    $html = '';
    if (!empty($tags[0])) {
        $ii     = 0;
        $tags_s = arraySort($tags, 'count');
        if (!empty($tags_s[0])) {
            foreach ($tags_s as $tag_id) {
                $ii++;
                $url = get_tag_link($tag_id);
                $tag = get_tag($tag_id);
                $html .= '<a href="' . $url . '"' . ($ajax_replace ? ' ajax-replace="true"' : '') . ' title="查看此标签更多文章" class="' . $class . '">' . $before . $tag->name . $after . '</a>';
                if ($count && $count == $ii) {
                    break;
                }
            }
        }
    }
    return $html;
}

/**
 * @description: 文章页文章底部
 * @param {*}
 * @return {*}
 */
function el_single_content_footer($post)
{
    // $cat = el_get_topics_tags('', 'but ml6 radius', '<i class="fa fa-cube" aria-hidden="true"></i>');
    $cat = el_get_cat_tags('but ml6 radius', '<i class="fa fa-folder-open-o" aria-hidden="true"></i>');
    $tags = el_get_posts_tags('but ml6 radius', '# ');

    // if (_opz('yiyan_single_content_footer')) {
    //     el_yiyan('article-yiyan theme-box text-center radius8 main-shadow yiyan-box');
    // }

    // do_action('el_article_content_after', $post);
    $user_id = $post->post_author;
    $post_link = get_permalink();
    $author_url = el_get_user_home_url($user_id);
    $user = get_userdata($user_id);
    if (_opz('post_copyright_s', true)) {
        $copyright =  '<blockquote class="content-copyright"><p class="content-copyright">版权属于：<a href="' . $author_url . '" title="" target="_blank" >' . $user->display_name . '</a></p>';
        $copyright .= '<p class="content-copyright">本文链接：<a class="content-copyright" title="" href="' . $post_link . '" target="_blank">' . $post_link . '</a></p>';
        $copyright .= '<p class="content-copyright">权责声明：凡本站署名内容，均须提前联系经站方同意后，注明出处及本声明，方可转载。</p></blockquote>';
        echo $copyright;
    }

    echo '<div class="end-line"><span class="end-text"> 全文结束 </span></div>';

    if ($cat || $tags) {
        echo '<div class="article-tags">' . $cat . '<br>' . $tags . '</div>';
    }

    // echo '<div class="em09 muted-3-color"><div><span>©</span> 版权声明</div><div class="posts-copyright">' . _opz('post_copyright') . '</div></div>';
}

function el_single_content_footer_action()
{
    // $user_id         = get_the_author_meta('ID');
    // $favorite_button = el_get_post_favorite('action action-favorite');

    // echo '<div class="text-center muted-3-color box-body em09">' . _opz('post_button_toptext', '喜欢就支持一下吧') . '</div>';
    // echo '<div class="text-center post-actions">';
    // if (_opz('post_like_s')) {
    //     echo el_get_post_like('action action-like');
    // }
    // if (_opz('post_rewards_s')) {
    //     echo el_get_rewards_button($user_id, 'action action-rewards');
    // }
    // if (_opz('share_s')) {
    //     echo el_get_post_share_btn(null, 'action action-share');
    // }
    $html = support_author();
    echo $html;
}
// https://blog.china52.net/12/41.html
function support_author()
{
    global $post;
    $html = '<div class="support-author">';
    // 点赞数量
    $html .= '<a href="javascript:;" data-action="like" class="action action-like text-muted" data-pid="'.$post->ID.'"><i data-feather="thumbs-up"></i><text>点赞</text><count></count></a>';
    $html .= '<a href="javascript:;"  data-bs-toggle="modal" data-bs-target="#rewordModal" data-remote="" class="rewards action action-rewards text-muted"><i data-feather="gift"></i><text>赞赏</text></a>';
    $html .= '<a href="javascript:;" data-action="favorite" class="action action-favorite text-muted" data-pid="'.$post->ID.'"><i data-feather="star"></i><text>收藏</text><count></count></a>';
    $html .= '<a href="javascript:;" data-action="share" class="action action-share text-muted" data-pid="'.$post->ID.'"><i data-feather="share"></i><text>分享</text></a>';
    $html .= '<p class="text-muted">别忘了点赞或赞赏，让我知道创作的路上有你陪伴。</p>';
    $html .= '</div>';
    $html .= reword_modal();
    return $html;
}

function reword_modal()
{
    $html = '';
    $html .= '<div class="modal fade in" id="rewordModal"tabindex="-1" role="dialog" style="display: none">';
    $html .= '<div class="modal-dialog modal-mini rewards-popover" style="margin-top: 114.5px" role="document">';
    $html .= '<div class="modal-content">';
    $html .= '<div class="modal-header">';
    $html .= '<button type="button" class="close" data-bs-dismiss="modal"  aria-label="Close"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-x"><line x1="18" y1="6" x2="6" y2="18"></line><line x1="6" y1="6" x2="18" y2="18"></line></svg></button>';
    $html .= '<h4 class="modal-title">赞赏作者</h4>';
    $html .= '</div>';
    $html .= '<div class="modal-body">';
    $html .= '<ul class="flex jse mb10 text-center rewards-box">';
    $html .= '<li><p class="muted-2-color">微信扫一扫</p>';
    $html .= '<div class="rewards-img"><img class="fit-cover" src="http://localhost/wp-content/uploads/2024/01/0d61ed629e20240112173208.png"/></div>';
    $html .= '</li>';
    $html .= '<li><p class="muted-2-color">支付宝扫一扫</p>';
    $html .= '<div class="rewards-img"><img class="fit-cover" src="http://localhost/wp-content/uploads/2024/01/0d61ed629e20240112173208.png"/></div>';
    $html .= '</li></ul>';
    $html .= '<div class="rewards-info">我是一些说明</div>';
    $html .= '</div></div></div>';

    return $html;
}

function el_single_after_box()
{
    if (_opz('yiyan_single_box',true)) {
        el_yiyan('yiyan-box mb20');
    }

    if (_opz('post_authordesc_s')) {
        $args = array(
            'user_id' => get_the_author_meta('ID'),
            'show_button' => false,
            'show_img_bg' => false,
            'class' => 'author',
        );
        el_get_user_card_box($args, true);
    }

    if (_opz('post_prevnext_s', true)) {
        el_posts_prevnext();
    }

    if (_opz('post_related_s', true)) {
        el_posts_related(_opz('related_title','相关阅读'), _opz('post_related_n',6), _opz('post_related_orderby', 'views'));
    }
}
add_action('el_single_after', 'el_single_after_box');

// 上下文章
// 是否显示封面
function el_posts_prevnext()
{
    $current_category = get_the_category();
    $prev_post        = get_previous_post($current_category, '');
    $next_post        = get_next_post($current_category, '');
    if (!empty($prev_post)) :
        $prev_title = $prev_post->post_title;
        $prev_link  = 'href="' . get_permalink($prev_post->ID) . '"';
        $prev_img   = el_post_thumbnail('', 'fit-cover', true, $prev_post);
    else :
        $prev_title = '无更多文章';
        $prev_link  = 'href="javascript:;"';
        $prev_img   = el_post_thumbnail('', 'fit-cover', true, $prev_post);
    endif;
    if (!empty($next_post)) :
        $next_title = $next_post->post_title;
        $next_link  = 'href="' . get_permalink($next_post->ID) . '"';
        $next_img   = el_post_thumbnail('', 'fit-cover', true, $next_post);
    else :
        $next_title = '无更多文章';
        $next_link  = 'href="javascript:;"';
        $next_img   = el_post_thumbnail('', 'fit-cover', true, $next_post);
    endif;
    $prev_arg = array(
        'title' => $prev_title,
        'link' => $prev_link,
        'img' => $prev_img,
        'name' => '上一篇'
    );
    $next_arg = array(
        'title' => $next_title,
        'link' => $next_link,
        'img' => $next_img,
        'name' => '下一篇'
    );
    $html = '';
    $html .= '<div class="prev-post-box"><div class="row">';
    $html .= el_next_html($prev_arg) . el_next_html($next_arg, 'text-end');
    $html .= '</div></div>';
    echo $html;
}

function el_next_html($post_arg, $class = '', $post = null)
{
    $html = '';

    $html .= '<div class="col-12 col-md-6">';
    $html .= '<div class="item-nextprev">';
    $html .= '<div class="media">';
    $html .= '<div class="media-content">';
    $html .= '<img src="' . esc_url(LAZY_COVER) . '" data-original="' . $post_arg['img'] . '" class="lazyload">';
    $html .= '</div></div>';
    $html .= '<a ' . $post_arg['link'] . ' class="item-content">';
    $html .= '<div class="item-body">';
    $html .= '<div class="h-2x">' . $post_arg['title'] . '</div>';
    $html .= '</div>';
    $html .= '<div class="item-meta ' . $class . '">'.$post_arg['name'].'</div>';
    $html .= '</a></div></div>';

    return $html;
}

function el_posts_related($related_title = '相关阅读', $limit = 6, $orderby = 'views')
{
    global $post;
    $categorys = get_the_terms($post, 'category');
    $posts_args = array(
        'showposts'           => $limit,
        'ignore_sticky_posts' => 1,
        'post_type'           => 'post',
        'post_status'         => 'publish',
        'order'               => 'DESC',
        'tax_query'           => array(
            'relation' => 'OR',
            array(
                'taxonomy' => 'category',
                'field'    => 'term_id',
                'terms'    => array_column((array) $categorys, 'term_id'),
            )
        ),
    );
    $posts_lits = '';
    $new_query  = new WP_Query($posts_args);
    while ($new_query->have_posts()) {
        $new_query->the_post();
        $posts_lits .= el_posts_mini_while(array('echo' => false, 'show_number' => false));
    }
    wp_reset_query();
    wp_reset_postdata();
    $html = '';
    // $html .= '<div class="post-related card mt-3"><div class="card-header">';
    // $html .= '<h3 class="d-flex align-items-center text-lg"><i data-feather="coffee"></i>'.$related_title.'</h3>';
    // $html .= '</div>';
    // $html .= '<div class="card-body"><ul>' . $posts_lits . '</ul></div>';
    // $html .= '</div></div>';
    $html .= '<section class="el-widget post-related">';
    $html .= '<h2 class="widget-title">' . $related_title;
    $html .= '<span class="widget-border"></span>';
    $html .= '</h2>';
    $html .= '<div class=""><ul>' . $posts_lits . '</ul></div>';
    $html .= '</section>';
    echo $html;
}
