<?php
/*
 * @Author       : ZengHao
 * @Date         : 2023-12-27 16:04:12
 * @LastEditors  : ZengHao
 * @LastEditTime : 2023-12-28 20:10:48
 * @FilePath     : /inc/functions/el-email.php
 * @Description  : 邮件函数
 * Copyright 2023 www.exehub.net, All Rights Reserved. 
 * 2023-12-27 16:04:12
 */

//邮件smtp设置
//覆盖邮件配置
if (_opz('mail_smtps')) {
    function mail_smtp_set($phpmailer)
    {
        $phpmailer->IsSMTP();
        $phpmailer->FromName   = _opz('mail_showname');
        $phpmailer->Host       = _opz('mail_host', 'smtp.qq.com');
        $phpmailer->Port       = _opz('mail_port', '465');
        $phpmailer->Username   = _opz('mail_name', '1797007941@qq.com');
        $phpmailer->Password   = _opz('mail_passwd', 'uhzqhlozbyhacdbf');
        $phpmailer->From       = _opz('mail_name', '1797007941@qq.com');
        $phpmailer->SMTPAuth   = _opz('mail_smtpauth', true);
        $phpmailer->SMTPSecure = _opz('mail_smtpsecure', 'ssl');
    }
    add_action('phpmailer_init', 'mail_smtp_set');
}
function el_set_from()
{
    return _opz('mail_name', '88888888@qq.com');;
}
add_filter('wp_mail_from', 'el_set_from');

// 邮件错误判断
// add_action( 'wp_mail_failed', function ( $error ) {
//     echo $error->get_error_message();
// } );

//检测是否默认的第三方生成邮箱
function el_email_change_email($email_change_email, $user = null, $userdata = null)
{
    if (el_check_email_is_sysgen($email_change_email['to'])) {
        return null;
    }
    return $email_change_email;
}

add_filter('email_change_email', 'el_email_change_email');
//检测邮箱是否系统生成
function el_check_email_is_sysgen($email)
{
    return preg_match("/^_p_[\w].+@null.null/", $email);
}

//邮件发件人名称
function el_mail_from_name($from_name)
{
    return _opz('mail_showname', get_bloginfo('name'));
}
add_filter('wp_mail_from_name', 'el_mail_from_name');

/**
 * @description: 发送邮件给网站管理员统一接口，会发送给所有超级管理员账号
 * @param {*} $title
 * @param {*} $message
 * @return {*}
 */
// function el_mail_to_admin($title, $message)
// {
//     $emails = el_get_admin_user_emails();
//     if ($emails) {
//         foreach ($emails as $e) {
//             @wp_mail($e, $title, $message);
//         }
//     }
// }

/**
 * @description: 获取所有管理员账号的邮箱
 * @param {*}
 * @return {*}
 */
// function el_get_admin_user_emails()
// {
//     $users = el_get_admin_users();
//     $email = array(get_option('admin_email'));
//     if ($users) {
//         foreach ($users as $user) {
//             if (!empty($user->user_email) && !in_array($user->user_email, $email)) {
//                 $email[] = $user->user_email;
//             }
//         }
//     }
//     return $email;
// }