<?php
/*
 * @Author       : ZengHao
 * @Date         : 2023-12-30 22:49:36
 * @LastEditors  : ZengHao
 * @LastEditTime : 2024-02-26 22:44:53
 * @FilePath     : /inc/functions/el-widget.php
 * @Description  : 
 * Copyright 2023 www.exehub.com, All Rights Reserved. 
 * 2023-12-30 22:49:36
 */

/**
 * 设置项目：
 */
function el_slider_widget_ui($args = array())
{
    // 判断是否有silders
    if (empty($args['slides'])) {
        return;
    }
    // 默认参数
    $defaults = array(
        'hide' => '',
        'direction' => 'horizontal', // 滚动方向 horizontal 左右滚动 / vertical 上下滚动
        'show_btn' => true, // 是否显示按钮，上下滚动不显示
        'autoplay' => true, // 是否自动播放，默认是
        'loop' => true, // 是否循环播放 / 默认是
        'pagination' => true, // 是否显示分页面滚动条
        'effect' => true,
        'interval' => 1000, // 定时切换间隔，默认 1 S
        'slide' => true, // 切换动画  
        'spacebetween' => 15,
        'pc_height' => 400,
        'm_height' => 200,
        'echo' => true, // 是否直接输出幻灯片
    );

    $args = wp_parse_args((array) $args, $defaults);

    // 处理显示规则
    $is_mobile = wp_is_mobile();
    // PC端不显示
    if ('pc' === $args['hide'] && !$is_mobile) {
        return;
    }
    // 移动端不显示
    if ('sm' === $args['hide'] && $is_mobile) {
        return;
    }
    $slide_item = '';
    // 处理高度
    $s_height = 'style="--m-height :' . $args['m_height'] . 'px;--pc-height :' . $args['pc_height'] . 'px;"';
    foreach ($args['slides'] as $slide) {
        $image = isset($slide['image']) ? esc_url($slide['image']) : '';
        $background_video = isset($slide['background_video']) ? $slide['background_video'] : '';
        // $link           = isset($slide['link']) ? $slide['link'] : '';
        $href = isset($slide['link']['url']) ? esc_url($slide['link']['url']) : '';
        $blank = isset($slide['link']['target']) ? true : false;
        // $link_title     = isset($slide['link']['text']) ? $slide['link']['text'] : '';
        $image_layer = isset($slide['image_layer']) ? $slide['image_layer'] : '';
        $text = isset($slide['text']) ? $slide['text'] : '';
        $title = isset($slide['text']['title']) ? $slide['text']['title'] : '';
        $desc = isset($slide['text']['desc']) ? $slide['text']['desc'] : '';
        $text_align = isset($slide['text']['text_align']) ? $slide['text']['text_align'] : 'left-bottom';
        // 构造单个
        $slide_item .= slider_item($blank, $href, $image, $title, $desc, $text_align);
    }
    // 轮播图设置
    $data_effect = $args['effect'] ? 'data-effext=' . $args['effect'] : 'data-effext="slider"';
    $data_interval = $args['interval'] ? 'data-interval=' . $args['interval'] * 100 : 'data-interval=4000';
    $data_speed = $args['interval'] ? 'data-speed=' . $args['interval'] * 100 : 'data-speed=4000';
    $data_direction = $args['direction'] ? 'data-direction=' . $args['direction'] : 'data-direction="horizontal"';
    $data_loop = $args['loop'] ? 'data-loop=' . $args['loop'] : 'data-loop=true';
    $data_autoplay = $args['autoplay'] ? 'data-autoplay=' . $args['autoplay'] : 'data-autoplay=true';
    $data_spacebetween = $args['spacebetween'] ? 'data-spacebetween="' . $args['spacebetween'] . '"' : 'data-spacebetween=15';

    $slides_wapper = '<div class="swiper-wrapper">' . $slide_item . '</div>';
    $swiper = '<div class="el-swiper slide-widget" ';
    $swiper .= $data_effect . ' ' . ' ' . $data_speed . ' ' . $data_interval . ' ' . $data_direction . ' ' . $data_loop . ' ' . $data_autoplay . ' ' . $data_spacebetween;
    $swiper .= $s_height . ">" . $slides_wapper . "</div>";
    $pagination = $args['pagination'] ? '<div class="swiper-pagination"></div>' : '';
    $button = $args['show_btn'] ? '<div class="swiper-button-prev"></div><div class="swiper-button-next"></div>' : '';

    //  这是幻灯片的主体内容部分
    if ($args['echo']) {
        echo '<div class="mb-3 el-slider">' . $swiper . $pagination . $button . '</div>';
    } else {
        return '<div class="mb-3 el-slider">' . $swiper . $pagination . $button . '</div>';
    }
}

function slider_item($target = true, $link, $img, $title, $desc, $text_align)
{
    $slide_item = '';
    $_blank = $target ? '_blank' : '';
    $slide_item .= '<div class="swiper-slide radius8" role="group">';
    $slide_item .= '<a target="' . $_blank . '" href="' . $link . '" data-swiper-parallax="1000" title="' . $title . '">';
    $slide_item .= '<img  class="lazyload" fetchpriority="high"  src="' . $img . '" alt="' . $title . '">';
    $slide_item .= '<div class="absolute" data-swiper-parallax="40" >';
    $slide_item .= '<div class="abs-center  slide-text ' . $text_align . '" style="--text-size-pc:30px;--text-size-m:20px;">';
    $slide_item .= '<div class="slide-title">' . $title . '</div>';
    $slide_item .= '<div class="slide-desc">' . $desc . '</div>';
    $slide_item .= '</div></div></a></div>';

    return $slide_item;
}


// 参考：https://codepen.io/ahmmu15/pen/brRBQm
function el_notice_widget_ui($args = array())
{
    $defaults = array(
        'hide'       => '',
        'color'      => 'c-blue-2',
        'direction'  => 'vertical',
        'notices'    => array(),
        'echo'       => true, // 是否直接输出
    );
    $args = wp_parse_args((array) $args, $defaults);
    // print_r($args);
    // 处理显示规则
    $is_mobile = wp_is_mobile();
    // PC端不显示
    if ('pc' === $args['hide'] && !$is_mobile) {
        return;
    }
    // 移动端不显示
    if ('sm' === $args['hide'] && $is_mobile) {
        return;
    }
    // 处理随机颜色
    $color = $args['color'];
    if ($args['color'] === 'rand') {
        $colors = array('c-blue', 'c-blue-2', 'c-cyan', 'c-yellow', 'c-yellow-2', 'c-green', 'c-green-2', 'c-purple', 'c-purple-2', 'c-red', 'c-red-2');
        // 随机索引
        $randomIndex = rand(0, count($colors) - 1);
        // 取得随机元素
        $color = $colors[$randomIndex];
    }
    $notices_item = '';
    foreach ($args['notices'] as $notice) {
        $blank = isset($notice['link']['target']) ? true : false;
        $link = isset($notice['link']['url']) ? esc_url($notice['link']['url']) : '';
        $con = isset($notice['con']) ? $notice['con'] : '';
        $icon = isset($notice['icon']) ? $notice['icon'] : '';
        $notices_item .= notices_item($blank, $link, $con, $icon);
    }
    $slides_wapper = '<div class="swiper-wrapper">' . $notices_item . '</div>';
    // 处理滚动方向
    $direction = empty($args['direction']) ? '' : 'data-direction=' . $args['direction'];
    $swiper = '<div class="el-notice-swiper slide-widget" ' . $direction . ' >' . $slides_wapper . '</div>';;

    if ($args['echo']) {
        echo '<div class="el-widget radius8 mb-3 ' . $color . ' el-notice-slider">' . $swiper . '</div>';
    } else {
        return '<div class="el-widget radius8 mb-3 ' . $color . ' el-notice-slider">' . $swiper . '</div>';
    }
}
function notices_item($target = true, $link, $con, $icon)
{
    $notices_item = '';
    $_blank = $target ? '_blank' : '';
    $notices_item .= '<div class="swiper-slide notice-slide">';
    $notices_item .= '<a class="text-ellipsis" target="' . $_blank . '" href="' . $link . '">';
    $notices_item .= '<div class="relative bulletin-icon me-1">' . $icon . '</div>';
    $notices_item .= $con;
    $notices_item .= '</a></div>';
    return $notices_item;
}

// 参考：https://themes.getbootstrap.com/preview/?theme_id=92520
function el_tiny_widget_ui($args = array())
{
    $defaults = array(
        'hide' => '',
        'title' => '',
        'type' => '',
        'height_scale' => '',
        'mask_opacity' => '',
        'pc_row' => '',
        'm_row' => '',
        'term_id' => '',
        'target_blank' => '',
        'echo' => true, // 是否直接输出
    );
    $args = wp_parse_args((array) $args, $defaults);
    // print_r($args);
    // 处理显示规则
    $is_mobile = wp_is_mobile();
    // PC端不显示
    if ('pc' === $args['hide'] && !$is_mobile) {
        return;
    }
    // 移动端不显示
    if ('sm' === $args['hide'] && $is_mobile) {
        return;
    }
    // 如果没有选择分类则返回
    if (empty($args['term_id'])) {
        return;
    }
    $target = $args['target_blank'] ? 'target="_blank"' : '';
    //循环 分类ID进行获取
    $terms = get_terms(
        array(
            'include' => $args['term_id'],
            'orderby' => 'include',
        )
    );
    // print_r($terms);
    //准备栏目
    $pc_row = (int) $args['pc_row'];
    $m_row = (int) $args['m_row'];

    $row_class = 'col-sm-' . (int) (12 / $pc_row);
    $row_class .= $m_row > 1 ? ' col-xs-' . (int) (12 / $m_row) : '';
    // html 常量
    $tiny_items = '';
    if ($terms) {
        foreach ($terms as $term) {
            $default_img = '';
            if ($term->taxonomy == 'category') {
                $default_img = esc_url(_opz('cat_def_cover'));
                $icon = '<i class="fa fa-folder-open-o me-1" aria-hidden="true"></i>';
            } elseif ($term->taxonomy == 'topics') {
                $default_img = _opz('topics_def_cover');
                $icon = '<i class="fa fa-cube me-1" aria-hidden="true"></i>';
            }
            $img = el_get_taxonomy_img_url($term->term_id, null, $default_img);
            $name = $term->name;
            $count = (int) $term->count ? (int) $term->count : 0;
            $description = !empty($term->description) ? $term->description : "当前分类暂无描述";
            $href = esc_url(get_term_link($term));
            $tiny_items .= tiny_item($row_class, $img, $name, $count, $description, $href, $target, $args['height_scale'], $args['type'], $args['mask_opacity']);
        }
    }
    // $cards = '<div class="tns-slider gutters-5">'.$tiny_items.'</div>';
    $con = '<div class="row gutters-5">' . $tiny_items . '</div>';
    $widget_title = el_widget_title($args['title']);
    if ($args['echo']) {
        echo '<section class="el-widget mb-3">' . $widget_title . $con . '</section>';
    } else {
        return '<section class="el-widget mb-3">' . $widget_title . $con . '</section>';
    }
}

function tiny_item($row_class, $img, $name, $count, $description, $href, $target, $height_scale, $type, $mask_opacity)
{
    $mask = $mask_opacity ? '<div class="absolute graphic-mask" style="opacity: ' . ((int) $mask_opacity / 100) . ';"></div>' : '';
    $html = '';
    $html .= '<div class="' . $row_class . '">';
    $html .= '<a ' . $target . ' href="' . $href . '" title="' . $name . '">';
    $html .= '<div class="graphic hover-zoom-img mb10 ' . $type . '" style="padding-bottom: ' . (int) $height_scale . '% !important">';
    $html .= '<img class="fit-cover lazyload" data-original="' . $img . '" alt="' . $name . '-分类卡片封面"/>';
    $html .= $mask;
    if ($type === 'style-3') {
        $html .= '<div class="abs-center left-bottom graphic-text text-ellipsis">';
        $html .= '<i class="fa fa-folder-open-o me-1" aria-hidden="true"></i>' . $name . '</div>';
        $html .= '<div class="abs-center left-bottom graphic-text">';
        $html .= '<div class="opacity8 text-s">' . $description . '</div>';
        $html .= '<div class="opacity8 text-s mt-1"><i class="fa me-1 fa-file-text-o"></i>' . $count . '篇文章</div></div></div>';
    } elseif ($type === 'style-1') {
        $html .= '<div class="abs-center left-bottom graphic-text">';
        $html .= '<div class="title-h-left"><b>' . $name . '</b></div>';
        $html .= '<div class="opacity8"><i class="fa fa-folder-open-o me-1" aria-hidden="true"></i>' . $count . '篇文章</div>';
        $html .= '</div></div>';
    } elseif ($type === 'style-2') {
        $html .= '<div class="abs-center conter-conter graphic-text"><div class="title-h-center"><b>' . $name . '</b></div></div>';
        $html .= '<div class="abs-center right-top"><badge class="b-black opacity8"><item data-toggle="tooltip" data-bs-animation="true" title="共' . $count . '篇文章"><i class="fa fa-folder-open-o me-1" aria-hidden="true"></i>' . $count . '</item></badge></div>';
        $html .= '</div>';
    } elseif ($type === 'style-4') {
        $html .= '</div>';
        $html .= '<div class="style-4-text padding-10">';
        $html .= '<div class="text-ellipsis"> <i class="fa fa-folder-open-o me-1" aria-hidden="true"></i>' . $name . '</div>';
        $html .= '<div class="muted-2-color text-ellipsis mt-1"> ' . $description . '</div>';
        $html .= '</div>';
    }
    $html .= '</a></div>';
    return $html;
}

function el_user_widget_ui($args = array())
{
    $defaults = array(
        'hide' => '',
        'card_bg' => '',
        'username' => '',
        'sign_type' => '',
        'signature' => '',
        'show_stat' => true,
        'show_post' => true,
        'term_id' => '',
        'show_soc' => false,
        'target' => false,
        'socialize' => '',
        'echo' => true, // 是否直接输出
    );
    $args = wp_parse_args((array) $args, $defaults);
    // 处理显示规则
    $is_mobile = wp_is_mobile();
    // PC端不显示
    if ('pc' === $args['hide'] && !$is_mobile) {
        return;
    }
    // 移动端不显示
    if ('sm' === $args['hide'] && $is_mobile) {
        return;
    }
    $usercard = user_card_html($args);
    if ($args['echo']) {
        echo '<section class="el-widget mb-3">' . $usercard . '</section>';
    } else {
        return '<section class="el-widget mb-3">' . $usercard . '</section>';
    }
}
function user_card_html($args)
{
    $html = '';
    $html .= '<div class="user-card card">';
    $html .= '<div class="widget-author-cover"><div class="media"><div class="media-content" style="background-image: url(' . $args['card_bg'] . ');"></div></div></div>';
    $html .= '<div class="widget-author-meta">';
    // TODO:用户头像信息设置和获取
    $html .= '<a href="#" class="flex-avatar w-96" target="_blank">';
    $html .= '<img  alt="" data-original="' . EL_ASSETS . 'img/wordpress.svg" class="avatar lazyload" height="80" width="80"/></a>';
    $html .= '<div class="mb-2 text-center">';
    $html .= '<h2 class="text-md"><a href="#" title="' . $args['username'] . '" target="_blank">' . $args['username'] . '</a></h2></div>';
    $html .= '<div class="text-secondary mb-3">' . $args['signature'] . '</div>';
    $html .= $args['show_stat'] ? '<div class="row g-0 mb-3">' . get_site_stat() . '</div>' : '';
    $html .= $args['show_soc'] ? '<div class="user-soc mb-3">' . user_soc_tag($args['socialize']) . '</div>' : '';
    $html .= $args['show_post'] ? '<div class="new-post mb-3">' . get_new_post($args) . '</div>' : '';
    $html .= '</div></div>';
    return $html;
}

function get_site_stat()
{
    $stat = el_get_site_stat();
    $html = '';
    $html .= '<div class="col"><div class="font-number text-xl">' . $stat['count_posts'] . '</div><div class="text-xs text-muted">文章</div></div>';
    $html .= '<div class="col"><div class="font-number text-xl">' . $stat['count_comments'] . '</div><div class="text-xs text-muted">评论</div></div>';
    $html .= '<div class="col"><div class="font-number text-xl">' . $stat['count_categories'] . '</div><div class="text-xs text-muted">分类</div></div>';
    $html .= '<div class="col"><div class="font-number text-xl">' . $stat['count_users'] . '</div><div class="text-xs text-muted">用户</div></div>';
    return $html;
}

function get_new_post($args)
{
    $posts = el_get_recent_posts(7, $args['term_id']);
    // print_r($posts);
    $html = '';
    foreach ($posts as $post) {
        $html .= '<div class="post-item">';
        $html .= '<a href="' . esc_url($post['guid']) . '" target="_blank" title="' . $post['post_title'] . '"><i class="fa fa-file-text-o" aria-hidden="true"></i><span>' . $post['post_title'] . '</span></a>';
        $html .= '</div>';
    }
    wp_reset_query();
    $html .= '<div></div>';
    return $html;
}

function user_soc_tag($socs)
{
    // print_r($socs);
    $html = '';
    $html .= '<div class="soc-tag">';
    foreach ($socs as $soc) {
        $tip_html = "<span class='soc-img'><img data-original='" . esc_url($soc['soc_img']) . "'/></span>";
        $soc_img_tip = ' data-bs-html="true" title="' . esc_html($tip_html) . '"';
        $tip_title = empty($soc['soc_img']) ? ' title="' . $soc['tooltips'] . '"' : $soc_img_tip;
        $html .= '<div class="soc-con" data-toggle="tooltip" ' . $tip_title . '>';
        if (empty($soc['soc_img'])) {
            $html .= '<a href=" target=' . $soc['link']['target'] . '"><p><i class="' . $soc['icon'] . '"></i></p><small class="text-muted">' . $soc['title'] . '</small></a>';
        } else {
            $html .= '<p><i class="' . $soc['icon'] . '"></i></p><small class="text-muted">' . $soc['title'] . '</small>';
        }
        $html .= '</div>';
    }
    $html .= '</div>';
    return $html;
}

function el_widget_title($title)
{
    $html = '';
    if (!empty($title)) {
        $html .= '<h2 class="widget-title">' . $title;
        $html .= '<span class="widget-border"></span>';
        $html .= '</h2>';
    }
    return $html;
}

function el_tags_widget_ui($args = array())
{
    $defaults = array(
        'hide' => '',
        'color' => 'rand',
        'title' => '',
        'max_num' => 20,
        'target_blank' => true,
        'fixed_width' => true,
        'show_num' => false,
        'echo' => true, // 是否直接输出
    );
    $args = wp_parse_args((array) $args, $defaults);
    $target = $args['target_blank'] ? ' target="_blank"' : '';
    $widget_title = el_widget_title($args['title']);
    $tag_args = array(
        'taxonomy' => 'post_tag',
        'orderby' => 'count',
        'order' => 'DESC',
        'number' => $args['max_num'],
        'hide_empty' => true,
        'count' => true,
    );
    $tags = ECache::get('widget_tags');
    if (false === $tags) {
        $tags = get_terms($tag_args);
        ECache::set('widget_tags', $tags);
    }
    $tag_html = '<div class="tag-cloud">';
    if (!empty($tags) && !is_wp_error($tags)) {
        foreach ($tags as $key => $tag) {
            $url = esc_url(get_term_link(intval($tag->term_id), $tag->taxonomy));
            $name = esc_attr($tag->name);
            $cls = array('c-blue', 'c-blue-2', 'c-cyan', 'c-yellow', 'c-yellow-2', 'c-green', 'c-green-2', 'c-purple', 'c-purple-2', 'c-red', 'c-red-2');
            // 随机索引
            $randomIndex = rand(0, count($cls) - 1);
            // 取得随机元素
            $color = $cls[$randomIndex];
            $tag_class = 'but ' . ('rand' != $args['color'] ? $args['color'] : $color);
            $count = $args['show_num'] ? '<span class="em09 tag-count"> (' . esc_attr($tag->count) . ')</span>' : '';
            $tag_html .= '<a' . $target . ' href="' . $url . '" class="text-ellipsis ' . $tag_class . '">' . $name . $count . '</a>';
        }
    }
    $tag_html .= '</div>';
    // print_r($tags);
    if ($args['echo']) {
        echo '<section class="el-widget">' . $widget_title . $tag_html . '</section>';
    } else {
        return '<section class="el-widget">' . $widget_title . $tag_html . '</section>';
    }
}

function el_link_widget_ui($args = array())
{
    $defaults = array(
        'hide' => '',
        'color' => 'rand',
        'title' => '',
        'link_cat' => '',
        'orderby' => 'rand',
        'order' => 'DESC',
        'limit' => '-1',
        'show_type' => 'card',
        'link_icon' => '',
        'image' => '',
        'target_blank' => true,
        'echo' => true, // 是否直接输出
    );
    $args = wp_parse_args((array) $args, $defaults);
    // 处理显示规则
    $is_mobile = wp_is_mobile();
    // PC端不显示
    if ('pc' === $args['hide'] && !$is_mobile) {
        return;
    }
    // 移动端不显示
    if ('sm' === $args['hide'] && $is_mobile) {
        return;
    }
    $target = $args['target_blank'] ? ' target="_blank"' : '';
    // 处理标题
    $widget_title = el_widget_title($args['title']);
    // 获取链接
    $link_args = array(
        'orderby' => $args['orderby'],
        'order' => $args['order'],
        'limit' => (int) $args['limit'],
        'category' => $args['link_cat'],
    );
    $links = ECache::get('widget_links');
    if (false === $links) {
        $links = get_bookmarks($link_args);
        ECache::set('widget_links', $links);
    }
    if (!$links) {
        return false;
    }
    // print_r($links);
    // 组装link
    $link_html = '';
    // 判断展示类型
    if ($args['show_type'] === 'card') {
        // 卡片模式
        foreach ($links as $link) {
            $link = (array) $link;
            $target = empty($link['link_target']) && $args['target_blank'] ? '' : ' target="_blank"';
            //  link_image 的判断，如果没有图片，则使用 $args['image'] 
            $img = empty($link['link_image']) ? esc_url($args['image']) : esc_url($link['link_image']);
            $link_html .= '<div class="link-card">';
            $link_html .= '<a href="' . esc_url($link['link_url']) . '" ' . $target . ' title="' . $link['link_name'] . '">';
            $link_html .= '<img  class="lazyload" data-original="' . $img . '" alt="' . $link['link_name'] . '-网站图">';
            $link_html .= '<div class="link-info">';
            $link_html .= '<h5 class="link-title">' . $link['link_name'] . '</h5>';
            $link_html .= '<p class="link-desc">' . $link['link_description'] . '</p>';
            $link_html .= '</div></a></div>';
        }
    } elseif ($args['show_type'] === 'image') {
        // 图片模式
        foreach ($links as $link) {
            $link = (array) $link;
            $img = empty($link['link_image']) ? esc_url($args['image']) : esc_url($link['link_image']);
            $link_html .= '<a data-toggle="tooltip" class="img-link" href="' . esc_url($link['link_url']) . '" ' . $target . ' title="' . $link['link_name'] . '">';
            $link_html .= '<img class="link-img lazyload" data-original="' . $img . '" alt="' . $link['link_name'] . '"></a>';
        }
    } else {
        // 文字模式
        foreach ($links as $link) {
            $link = (array) $link;
            $link_html .= '<a href="' . esc_url($link['link_url']) . '" ' . $target . ' title="' . $link['link_name'] . '"><i class="' . $args['link_icon'] . '"></i> ' . $link['link_name'] . '</a>';
        }
    }
    $shadow_class = $args['show_type'] == 'text' ? 'theme-shadow' : '';
    $links_html = '<div class="link-box">' . $link_html . '</div>';
    if ($args['echo']) {
        echo '<section class="el-widget mb-3' . $shadow_class . '">' . $widget_title . $links_html . '</section>';
    } else {
        return '<section class="el-widget mb-3' . $shadow_class . ' ">' . $widget_title . $links_html . '</section>';
    }
}

function el_hotpost_widget_ui($args)
{
    $defaults = array(
        'hide'          => '',
        'title'         => '',
        'orderby'       => 'views',
        'limit_day'     => 0,
        'count'         => 6,
        'term_id'       => '',
        'target_blank'  => true,
        'echo' => true, // 是否直接输出
    );
    $args = wp_parse_args((array) $args, $defaults);
    // 处理显示规则
    $is_mobile = wp_is_mobile();
    // PC端不显示
    if ('pc' === $args['hide'] && !$is_mobile) {
        return;
    }
    // 移动端不显示
    if ('sm' === $args['hide'] && $is_mobile) {
        return;
    }
    $target = $args['target_blank'] ? ' target="_blank"' : '';
    // 处理标题
    $widget_title = el_widget_title($args['title']);
    $post_count = (int)$args['count'];

    //准备文章
    $posts_args = array(
        'has_password'        => 0,
        'showposts'           => $args['count'],
        'ignore_sticky_posts' => 1,
        'post_type'           => 'post',
        'post_status'         => 'publish',
        'order'               => 'DESC',
    );

    //文章排序
    $orderby = $args['orderby'];
    if ($orderby !== 'views' && $orderby !== 'favorite' && $orderby !== 'like') {
        $posts_args['orderby'] = $orderby;
    } else {
        $posts_args['orderby']    = 'meta_value_num';
        $posts_args['meta_query'] = array(
            array(
                'key'   => $orderby,
                'order' => 'DESC',
            ),
        );
    }
    //文章限制时间
    if ($args['limit_day'] > 0) {
        $posts_args['date_query'] = array(
            array(
                'after'     => date('Y-m-d H:i:s', strtotime("-" . $args['limit_day'] . " day")),
                'before'    => date('Y-m-d H:i:s'),
                'inclusive' => true,
            ),
        );
    }

    //循环文章内容
    $posts_html = '<div class="widget-body"><ul class="widget-list-ul">';
    $posts_i    = 1;
    // 缓存获取
    $new_query = ECache::get('el_hot_post');
    // print_r($posts_args);
    if (false === $new_query) {
        $new_query  = new WP_Query($posts_args);
        // 缓存1小时
        ECache::set('el_hot_post', $new_query, EL_CACHE, 3600);
    }

    while ($new_query->have_posts()) {
        $new_query->the_post();
        $title          = get_the_title();
        $permalink      = get_permalink();
        $_thumb         = el_post_thumbnail('large', 'fit-cover radius8');
        $time_ago       = '<span class="el-time"><i class="fa fa-clock-o mr3" aria-hidden="true"></i>' . el_get_time_ago(get_the_time('Y-m-d H:i:s')) . '</span>';
        $top_brage      = '<div class="media-overlay overlay-top"> <span class="badge badge-hot font-number">TOP' . $posts_i . '</span></div>';
        $posts_html .= '<li class="list-item mb-3">';
        $posts_html .= '<div class="post-media">';
        $posts_html .= '<a href="' . $permalink . '" ' . $target . ' class="media-content">';
        $posts_html .= '<img title="' . $title . '-封面图" src="' . esc_url(LAZY_COVER) . '"  data-original="' . $_thumb . '" class="post-cover lazyload">';
        $posts_html .= '</a>';
        $posts_html .= $top_brage . '</div>';
        $posts_html .= '<div class="list-content">';
        $posts_html .= '<div class="list-body">';
        $posts_html .= '<a href="' . $permalink . '" ' . $target . ' class="list-title">';
        $posts_html .= $title . '</a></div>';
        $posts_html .= '<div class="list-footer text-muted text-xs">' . $time_ago . '</div>';
        $posts_html .= '</div>';
        $posts_html .= '</li>';
        $posts_i++;
    }
    // print_r($posts_i);
    $posts_html .= '</ul></div>';
    wp_reset_query();
    if ($args['echo']) {
        echo '<section class="el-widget mb-3 ">' . $widget_title . $posts_html . '</section>';
    } else {
        return '<section class="el-widget mb-3 ">' . $widget_title . $posts_html . '</section>';
    }
}

function el_catpost_widget_ui($args = array())
{
    $defaults = array(
        'hide'          => '',
        'title'         => '',
        'right_text'    => '',
        'right_link'    => '',
        'type'          => 0,
        'orderby'       => 'date',
        'order'         => 'DESC',
        'count'         => '',
        'term_id'       => '',
        'target_blank'  => true,
        'echo' => true, // 是否直接输出
    );
    $args = wp_parse_args((array) $args, $defaults);
    // 处理显示规则
    $is_mobile = wp_is_mobile();
    // PC端不显示
    if ('pc' === $args['hide'] && !$is_mobile) {
        return;
    }
    // 移动端不显示
    if ('sm' === $args['hide'] && $is_mobile) {
        return;
    }
    $target = $args['target_blank'] ? ' target="_blank"' : '';
    // 处理标题
    $widget_title = el_widget_title($args['title']);
    // echo json_encode($args['term_id']);
    // 遍历选择的分类，如果没有选择分类，则提示
    if (empty($args['term_id'])) {
        $cat_item = '<div style="padding:10px">请选择分类</div>';
    } else {
        $cat_item = '';
        foreach ($args['term_id'] as $cat_id) {
            // 查询
            $posts_args = array(
                'post_type'      => 'post',
                'posts_per_page' => $args['count'],
                'no_found_rows'  =>   true,
                'cat'            => $cat_id,
                'showposts'           => $args['count'],
                'ignore_sticky_posts' => 1,
                'post_type'           => 'post',
                'post_status'         => 'publish',
            );
            //文章排序
            $orderby = $args['orderby'];
            if ($orderby !== 'views' && $orderby !== 'favorite' && $orderby !== 'like') {
                $posts_args['orderby'] = $orderby;
            } else {
                $posts_args['orderby']    = 'meta_value_num';
                $posts_args['meta_query'] = array(
                    array(
                        'key'   => $orderby,
                        'order'          => $args['order'],
                    ),
                );
            }
            // print_r($posts_args);
            // 获取分类封面
            $_cat_thum = el_get_taxonomy_img_url($cat_id);
            // 获取分类链接，直接加入缓存            
            $cat_name = ECache::get('cat_name_' . $cat_id);
            if (false === $cat_name) {
                $cat_name = get_cat_name($cat_id);
                // 直接缓存一年
                ECache::set('cat_name_' . $cat_id, $cat_name);
            }

            $cat_link = ECache::get('cat_link_' . $cat_id);
            if (false === $cat_link) {
                $cat_link = get_category_link($cat_id);
                // 直接缓存一年
                ECache::set('cat_link_' . $cat_id, $cat_link);
            }

            $cat_desc = ECache::get('cat_desc_' . $cat_id);
            if (false === $cat_desc) {
                $cat_desc = category_description($cat_id);
                // 直接缓存一年
                ECache::set('cat_desc_' . $cat_id, $cat_desc);
            }

            $cat_post_count = ECache::get('cat_post_count_' . $cat_id);
            if (false === $cat_post_count) {
                $cat_post_count = get_category($cat_id)->count;
                // 直接缓存一年
                ECache::set('cat_post_count_' . $cat_id, $cat_post_count);
            }

            // 缓存获取文章
            $new_query = ECache::get('el_cat_post' . $cat_id);
            if (false === $new_query) {
                $new_query  = new WP_Query($posts_args);
                // 缓存1小时
                ECache::set('el_cat_post' . $cat_id, $new_query, EL_CACHE, 3600);
            }
            // print_r($new_query);
            $cat_item .= '<li class="widget-li home-collection-in">';
            $cat_item .= '<div class="home-collection-image">';
            $cat_item .= '<a class="link-block" href="" ' . $target . '"></a>';
            $cat_item .= '<img class="home-collection-thumb lazyload"  alt="' . $cat_name . '-封面图" data-original="' . $_cat_thum . '" src="' . esc_url(LAZY_COVER) . '"></div>';
            $cat_item .= '<div class="collection-info">';
            $cat_item .= '<a title="" href="" ' . $target . '><h2>' . $cat_name . '</h2></a>';
            $cat_item .= '<div class="home-collection-row-1"><span>更新' . $cat_post_count . '篇</span><a href="' . esc_url(get_category_link($cat_id)) . '" ' . $target . ' title="">更多</a></div>';
            $cat_item .= '<div class="home-collection-row-2">';
            while ($new_query->have_posts()) {
                $new_query->the_post();
                $title          = get_the_title();
                $permalink      = get_permalink();
                $_thumb         = el_post_thumbnail('large', 'fit-cover radius8');
                $cat_item .= '<div class="post-item">';
                $cat_item .= '<img class="post-thumb lazyload"  alt="' . $title . '" data-original="' . $_thumb . '" src="' . esc_url(LAZY_COVER) . '">';
                $cat_item .= '<a title="' . $title . '" href="' . $permalink . '" ' . $target . '>' . $title . '</a></div>';
            }
            $cat_item .= '</div></li>';
            // wp_reset_query();
            wp_reset_postdata();
        }
    }

    $cat_html = '<div class="catpost-widget-box"><ul class="widget-list-ul">';
    $cat_html .= $cat_item;
    $cat_html .= '</ul></div>';


    if ($args['echo']) {
        echo '<section class="el-widget mb-3 style2 widget-catpost ">' . $widget_title . $cat_html . '</section>';
    } else {
        return '<section class="el-widget mb-3 style2 widget-catpost">' . $widget_title . $cat_html . '</section>';
    }
}

function el_search_widget_ui($args = array())
{
    $defaults = array(
        'hide'         => '',
        'title'        => '',
        'type'         => 'inner',
        'echo' => true, // 是否直接输出
    );
    $args = wp_parse_args((array) $args, $defaults);
    // 处理显示规则
    $is_mobile = wp_is_mobile();
    // PC端不显示
    if ('pc' === $args['hide'] && !$is_mobile) {
        return;
    }
    // 移动端不显示
    if ('sm' === $args['hide'] && $is_mobile) {
        return;
    }
    $widget_title = el_widget_title($args['title']);
    $search_h = '';
    // if($args['type'] == 'inner'){
    //     $action = esc_url(home_url( '/' ));
    // }else{
    //     $args['type'] == 'baidu'?($action='https://www.baidu.com/');
    // }
    $search_h .= '<form class="global-search-form" action="' . esc_url(home_url('/')) . '" method="get">';
    $search_h .= '<div id="custom-search">';
    $search_h .= '<input id="search-input" name="s" placeholder="输入关键词搜索..."/>';
    $search_h .= '<button id="search-button"><i class="fa fa-search"></i></button>';
    $search_h .= '<div class="spinner"><i class="fa fa-spinner"></i></div>';
    $search_h .= '</div></form>';

    if ($args['echo']) {
        echo '<section class="el-widget">' . $widget_title  . $search_h . '</section>';
    } else {
        return '<section class="el-widget">' . $widget_title . $search_h . '</section>';
    }
}


function el_comment_widget_ui($args = array())
{
    $defaults = array(
        'hide'         => '',
        'title'        => '',
        'count'        => 6,
        'outer'        => '',
        'outpost'      => '',
        'target_blank' => false,
        'echo' => true, // 是否直接输出
    );
    $args = wp_parse_args((array) $args, $defaults);
    // 处理显示规则
    $is_mobile = wp_is_mobile();
    // PC端不显示
    if ('pc' === $args['hide'] && !$is_mobile) {
        return;
    }
    // 移动端不显示
    if ('sm' === $args['hide'] && $is_mobile) {
        return;
    }
    $widget_title = el_widget_title($args['title']);
    $comment_html = '';
    global $wpdb;
    $comm_args = array(
        'orderby'        => 'comment_date',
        'number'         => $args['count'],
        'status'         => 'approve',
        'author__not_in' => preg_split("/,|，|\s|\n/", $args['outer']),
        'post__not_in'   => preg_split("/,|，|\s|\n/", $args['outpost']),
    );

    $comments = get_comments($comm_args);
    // print_r($comments);
    $com_h = '<ul class="comments-list">';
    foreach ($comments as $comment) {
        $cont  = el_comment_filters(get_comment_text($comment->comment_ID), 'noimg');
        $_link = get_comment_link($comment->comment_ID);
        $post_title = $comment->post_title;
        // $post_link = get_the_permalink($comment->ID);
        $time      = el_get_time_ago($comment->comment_date);
        $user_name = get_comment_author($comment->comment_ID);
        $user_id   = $comment->user_id;
        // print_r($post_title);
        if ($user_id) {
            $user_name = '<a target="_blank" rel="user" href="' . el_get_user_home_url($user_id) . '">' . $user_name . '</a>';
            // $user_name = el_get_user_name('id=' . $user_id . '&level=0&class=inflex ac relative-h');
        }
        $com_h .= '<li class="list-item">';
        $com_h .= '<a href="'.$_link.'" target="_blank" rel="comment">';
        $com_h .= '<h4>'.el_icon('file-text').$post_title.'</h4>';
        $com_h .= '<div class="comments-info">';
        $com_h .= '<p class="comments-user"><span class="user">'.el_icon('user').'<strong>'.$user_name.'</strong></span><span class="time">'.el_icon('clock').$time.'</span> 说到:</p>';
        $com_h .= '<p class="comments-con">'.$cont.'</p>';
        $com_h .= '</div></a></li>';
    }
    $com_h .= '</ul>';
    $comment_html .= $com_h;
    if ($args['echo']) {
        echo '<section class="el-widget mb-3">' . $widget_title . $comment_html . '</section>';
    } else {
        return '<section class="el-widget mb-3">' . $widget_title . $comment_html . '</section>';
    }
}


function el_comment_filters($cont, $type = '', $lazy = true)
{
    $cont = convert_smilies($cont);

    $cont = preg_replace('/\[img=(.*?)\]/', '<img class="box-img lazyload" src="$1" alt="评论图片' . el_get_delimiter_blog_name() . '">', $cont);

    if ('noimg' == $type) {
        $cont = preg_replace('/\<img(.*?)\>/', '[图片]', $cont);
        $cont = preg_replace('/\[code]([\s\S]*)\[\/code]/', '[代码]', $cont);
    } else {
        $cont = str_replace('[code]', '<pre><code>', $cont);
        $cont = str_replace('[/code]', '</code></pre>', $cont);
    }

    $cont = wp_kses_post($cont);
    return $cont;
}


function el_stat_widget_ui($args = array())
{
    $defaults = array(
        'hide'         => '',
        'title'        => '',
        'show_mod'     => 6,
        'echo' => true, // 是否直接输出
    );
    $args = wp_parse_args((array) $args, $defaults);
    // 处理显示规则
    $is_mobile = wp_is_mobile();
    // PC端不显示
    if ('pc' === $args['hide'] && !$is_mobile) {
        return;
    }
    // 移动端不显示
    if ('sm' === $args['hide'] && $is_mobile) {
        return;
    }
    $widget_title = el_widget_title($args['title']);
    $stats = el_get_site_stat();
    $html = '<ul class="stat-list">';
    $stat_h ='';
    $stat_dict = array(
        'count_posts'     => '文章总数',
        'count_comments'  => '评论总数',
        'count_tags'      => '标签总数',
        'count_pages'     => '页面总数',
        'count_categories'      => '分类总数',
        'count_users'     => '用户总数',
        'count_links'     => '链接总数',
        'last_update'    => '最后更新',
    );
    foreach ($args['show_mod'] as $mod) {
        $stat_h .= '<li class="list-item">';
        $stat_h .= '<strong>'.$stat_dict[$mod].'：</strong>'.$stats[$mod];
        $stat_h .= '</li>';
    }
    $html .= $stat_h;
    $html .= '</ul>';
    
    if ($args['echo']) {
        echo '<section class="el-widget mb-3">' . $widget_title . $html . '</section>';
    } else {
        return '<section class="el-widget mb-3">' . $widget_title . $html . '</section>';
    }
}