<?php
/*
 * @Author       : ZengHao
 * @Date         : 2024-01-22 18:47:20
 * @LastEditors  : ZengHao
 * @LastEditTime : 2024-02-02 21:57:48
 * @FilePath     : /inc/functions/el-editor.php
 * @Description  : 
 * Copyright 2024 www.exehub.com, All Rights Reserved. 
 * 2024-01-22 18:47:20
 */
//为编辑器添加的allowfullscreen，输出时候移除
// add_filter('the_content', 'el_the_content_remove_contenteditable');
// function el_the_content_remove_contenteditable($content)
// {
//     $content = str_replace(' contenteditable="', ' mce-contenteditable="', $content);
//     return $content;
// }
// 自适应图片删除width和height，by Ludou
function ludou_remove_width_height_attribute($content){
	preg_match_all('/<[img|IMG].*?src=[\'|"](.*?(?:[\.gif|\.jpg|\.png\.bmp]))[\'|"].*?[\/]?>/', $content, $images);
	if(!empty($images)) {
	  foreach($images[0] as $index => $value){
		$new_img = preg_replace('/(width|height)="\d*"\s/', "", $images[0][$index]);
		$content = str_replace($images[0][$index], $new_img, $content);
	  }
	}
	return $content;
  }
  
  // 判断是否是移动设备浏览
  add_filter('the_content', 'ludou_remove_width_height_attribute', 99);
function el_admin_scripts()
{
	wp_enqueue_script(
		'el-admin',
		EL_ASSETS . 'js/tinymce/tiny-mce.js',
		array(),
		EL_VERSION,
		true
	);
}
if (is_admin()) {
	add_action('admin_enqueue_scripts', 'el_admin_scripts');
	add_filter('mce_css', function ($mce_css) {
		$mce_css .= $mce_css ? ',' : '';
		$mce_css .= EL_ASSETS . 'css/tinymce.min.css?ver=' . EL_VERSION . ',' . EL_ASSETS . 'css/font-awesome.min.css?ver=' . EL_VERSION . '';
		return $mce_css;
	});
}

add_action('media_buttons', 'el_shortcode_box_init', 99);
function el_shortcode_box_init()
{
	$shortcodes = el_shortcode_register();
	$output = "";
	$ajax_url = esc_url(admin_url('admin-ajax.php'));
	foreach ($shortcodes as $key => $item) {
		$output .= "<a href='javascript:;' data-action='" . $ajax_url . "' class='add-shortcode button button-small' data-key='{$key}'>{$item['name']}</a>";
	}
	$html = '
	<button type="button" id="insert-shortcode-button" class="button insert-shortcode add_shortcode" data-editor="content">
    <i class="fa fa-diamond" aria-hidden="true"></i> 添加短代码</button>
	';
	$html .= '<div id="insert-shortcode-wrap" class="el-media-wrap" style="display: none">' . $output . '</div>';
	echo $html;
}

/*短代码*/
// $shortCodeColors = array('primary', 'danger', 'warning', 'info', 'success', 'dark');

function el_shortcode_register()
{
	// global $shortCodeColors;
	$list = array(
		'download' => array(
			'name' => '文件下载',
			'content' => '文件地址',
			'attr' => array(
				'file' => 'xxx.zip',
				'size' => '12MB'
			)
		),
		'post' => array('name' => '文章卡片', 'content' => '输入文章ID', 'attr' => array()),
		'cat' => array('name' => '分类卡片', 'content' => '输入分类ID', 'attr' => array()),
		'bilibili' => array(
			'name' => 'B站视频',
			'content' => 'av和bv号只能填一个',
			'attr' => array(
				'av' => 'AV号',
				'bv' => 'BV号',
				'h'  => 400,
				'w'  => '100%',
			)
		),
		'code' => array(
			'name' => '高亮代码',
			'content' => ' '
		),
	);
	// foreach ($shortCodeColors as $sc_tips) {
	// 	$list['t-' . $sc_tips] = array(
	// 		'name' => '提示框' . $sc_tips,
	// 		'attr' => array(
	// 			'icon' => ''
	// 		),
	// 		'content' => '输入内容'
	// 	);
	// }
	return $list;
}

// TinyMce 编辑器
if(is_admin(  )){
	add_action('init', 'custom_tinymce');
}
function custom_tinymce()
{
	add_filter('mce_external_plugins', 'add_plugin');
}
function add_plugin($plugin_array)
{
	$plugin_array['elcode']   = EL_ASSETS . 'js/tinymce/precode.js?ver=' . EL_VERSION . '';
	$plugin_array['ebilibili']   = EL_ASSETS . 'js/tinymce/bilibili.js?ver=' . EL_VERSION . '';
	$plugin_array['elcat']   = EL_ASSETS . 'js/tinymce/elcat.js?ver=' . EL_VERSION . '';
	$plugin_array['epost']   = EL_ASSETS . 'js/tinymce/epost.js?ver=' . EL_VERSION . '';
	$plugin_array['edownload']   = EL_ASSETS . 'js/tinymce/edownload.js?ver=' . EL_VERSION . '';
	return $plugin_array;
}


// 请求分类信息，封装分类卡片
function el_set_shortcode_cat()
{
	$cat_id = !empty($_REQUEST['cat_id']) ? (int) $_REQUEST['cat_id'] : 0;
	if ($cat_id == 0) {
		wp_send_json_error("分类ID 不正确");
	}
	$title = get_cat_name($cat_id);
	$link = get_category_link($cat_id);
	$views = el_get_term_post_views_sum($cat_id, true);

	wp_send_json_success(json_encode(
		array(
			'title' => $title,
			'link'  => $link,
			'views' => $views
		)
	));
	wp_die();
}
add_action('wp_ajax_set_shortcode_cat', 'el_set_shortcode_cat');


function el_admin_get_cat_id()
{
	$args = array(
		'orderby' => 'name',
		'hide_empty' => false, //显示所有分类
		'order' => 'ASC'
	);
	$categories = get_categories($args);
	wp_send_json_success($categories);
	wp_die();
}
add_action('wp_ajax_admin_get_cat_id', 'el_admin_get_cat_id');


function el_set_shortcode_post()
{
	$pid = !empty($_REQUEST['pid']) ? (int) $_REQUEST['pid'] : 0;
	if ($pid == 0) {
		wp_send_json_error(array(
			'文章 ID 不正确"'
		));
	}
	$post = get_post($pid);
	if ($post == null) {
		wp_send_json_error(array(
			'文章未找到！'
		));
	}
	$title = $post->post_title;
	$link = get_permalink($post);
	$views = get_post_view_count('', '', $post->ID);
	$time = get_the_time('Y-m-d H:i:s', $post);
	$time_ago = el_get_time_ago($time);

	wp_send_json_success(json_encode(
		array(
			'title' => $title,
			'link'  => $link,
			'views' => $views,
			'time_ago' => $time_ago
		)
	));
	wp_die();
}
add_action('wp_ajax_set_shortcode_post', 'el_set_shortcode_post');


function el_get_down_tips()
{
	$down_tips = _opz('down_tips') ? _opz('down_tips') : '请在后台设置下载声明';
	wp_send_json_success($down_tips);
	wp_die();
}
add_action('wp_ajax_get_down_tips', 'el_get_down_tips');
