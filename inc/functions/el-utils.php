<?php 
/*
 * @Author       : ZengHao
 * @Date         : 2023-12-28 20:46:52
 * @LastEditors  : ZengHao
 * @LastEditTime : 2024-01-08 16:40:50
 * @FilePath     : \inc\functions\el-utils.php
 * @Description  : 
 * Copyright 2023 www.exehub.com, All Rights Reserved. 
 * 2023-12-28 20:46:52
 */


function el_date($format, $timestamp = null)
{
    if (null === $timestamp) {
        $timestamp    = time();
    } elseif (!is_numeric($timestamp)) {
        return false;
    }

    return date_create('@' . $timestamp)->setTimezone(wp_timezone())->format($format);
}



/**
 * 获取当前分类目录ID
 */
function el_get_current_category_id()
{
    $current_category = single_cat_title('', false); //获得当前分类目录名称
    return get_cat_ID($current_category); //获得当前分类目录 ID
}

/**
 * 获取WordPress搜索关键词
 * @author beiwangshan <beiwangshan.com>
 */
function el_get_search_keywords()
{
    $s = trim(get_search_query()) ? trim(get_search_query()) : 0;
    return $s;
}


// 给分类连接添加SEO
function _get_tax_meta($id = 0, $field = '')
{
    $ops = get_option("_taxonomy_meta_$id");

    if (empty($ops)) {
        return '';
    }

    if (empty($field)) {
        return $ops;
    }

    return isset($ops[$field]) ? $ops[$field] : '';
}

/**
 * @description: AJAX空白内容
 * @param {*}
 * @return {*}
 */
function el_get_ajax_null($text = '暂无内容', $margin = '60', $img = 'null.svg')
{
    $html = el_get_null($text, $margin, $img, 'ajax-item ');
    $html .= '<div class="ajax-pag hide"><div class="next-page ajax-next"><a href="#"></a></div></div>';
    return $html;
}

/**
 * @description: 空白内容
 * @param {*}
 * @return {*}
 */
function el_get_null($text = '暂无内容', $margin = '60', $img = 'null.svg', $class = '', $width = 280, $height = 0)
{
    $text = $text ? '<p style="margin-top:' . $margin . 'px;" class="em09 muted-3-color separator">' . $text . '</p>' : '';
    if ($height) {
        $style = $width ? 'max-width:' . $width . 'px;' : '';
        $style .= $height ? 'height:' . $height . 'px;' : '';
    } else {
        $style = $width ? 'width:' . $width . 'px;' : '';
    }
    $style .= 'opacity: .7;';
    $html = '<div class="text-center ' . $class . '" style="padding:' . $margin . 'px 0;"><img style="' . $style . '" src="' . EL_ASSETS . 'img/' . $img . '">' . $text . '</div>';
    return $html;
}

//时间倒序格式化
function el_get_time_ago($time)
{
    if (is_int($time)) {
        $time = intval($time);
    } else {
        $time = strtotime($time);
    }

    if (!_opz('time_ago_s', true) && _opz('time_format')) {
        return date(_opz('time_format'), $time);
    }
    $ctime = intval(strtotime(current_time('mysql')));
    $t     = $ctime - $time; //时间差 （秒）

    if ($t < 0) {
        return date('Y-m-d H:i', $time);
    }
    $y = intval(date('Y', $ctime) - date('Y', $time)); //是否跨年
    if (0 == $t) {
        $text = '刚刚';
    } elseif ($t < 60) {
        //一分钟内
        $text = $t . '秒前';
    } elseif ($t < 3600) {
        //一小时内
        $text = floor($t / 60) . '分钟前';
    } elseif ($t < 86400) { //一天内
        $text = floor($t / 3600) . '小时前'; // 一天内
    } elseif ($t < 2592000) {
        //30天内
        if ($time > strtotime(date('Ymd', strtotime("-1 day")))) {
            $text = '昨天';
        } elseif ($time > strtotime(date('Ymd', strtotime("-2 days")))) {
            $text = '前天';
        } else {
            $text = floor($t / 86400) . '天前';
        }
    } elseif ($t < 31536000 && 0 == $y) {
        //一年内 不跨年
        $m = date('m', $ctime) - date('m', $time) - 1;

        if (0 == $m) {
            $text = floor($t / 86400) . '天前';
        } else {
            $text = $m . '个月前';
        }
    } elseif ($t < 31536000 && $y > 0) {
        //一年内 跨年
        $text = (12 - date('m', $time) + date('m', $ctime)) . '个月前';
    } else {
        $text = (date('Y', $ctime) - date('Y', $time)) . '年前';
    }

    return $text;
}

function el_get_delimiter_blog_name()
{
    static $_this = null;
    if ($_this) {
        return $_this;
    }
    $_this = _get_delimiter() . get_bloginfo('name');
    return $_this;
}

function _get_delimiter()
{
    return _opz('connector') ? _opz('connector') : '-';
}


//获取文章列表的图片
function el_get_posts_thumb_graphic($class = 'item-thumbnail')
{
    global $post;
    $get_permalink     = get_permalink();
    $post_target_blank = _post_target_blank();

    $_thumb      = '';
    $format_icon = '';

    $video = get_post_meta($post->ID, 'featured_video', true);
    if ($video) {
        $format_icon = '<i class="fa fa-play-circle em12 mt6 c-white opacity8" aria-hidden="true"></i>';
        if (!$_thumb && _opz('list_thumb_video_s')) {
            $img_thumb = el_post_thumbnail('', 'fit-cover radius8');
            $mute_attr = _opz('list_thumb_video_mute_s', true) ? '  data-volume="none"' : '  data-volume="100"';
            $_thumb    = '<div class="video-thumb-box" video-url="' . esc_url($video) . '"' . $mute_attr . '><div class="img-thumb">' . $img_thumb . '</div><div class="video-thumb"></div></div>';
        }
    }

    if (!$_thumb && _opz('list_thumb_slides_s')) {
        $slides_imgs = explode(',', get_post_meta($post->ID, 'featured_slide', true));
        if (!empty($slides_imgs[0])) {
            $format_icon = $format_icon or '<badge class="b-black opacity8 mt6"><i class="fa fa-image mr3" aria-hidden="true"></i>' . count($slides_imgs) . '</badge>';

            $slides_args = array(
                'class'      => $class,
                'button'     => false,
                'pagination' => 1,
                'echo'       => false,
            );

            foreach ($slides_imgs as $slides_img) {
                $background = el_get_attachment_image_src((int) $slides_img, _opz('thumb_postfirstimg_size'));
                $slide      = array(
                    'background' => isset($background[0]) ? $background[0] : '',
                    'link'       => array(
                        'url'    => $get_permalink,
                        'target' => $post_target_blank,
                    ),
                );
                $slides_args['slides'][] = $slide;
            }
            $_thumb = el_new_slider($slides_args, false);
        }
    }

    if (!$_thumb) {
        $_thumb = el_post_thumbnail('', 'fit-cover radius8');
        $_thumb = '<a' . $post_target_blank . ' href="' . $get_permalink . '">' . $_thumb . '</a>';
    }

    if (!$format_icon) {
        $format = get_post_format();
        if (in_array($format, array('image', 'gallery'))) {
            $img_count   = el_get_post_imgs_count($post);
            $format_icon = $img_count > 0 ? '<badge class="b-black opacity8 mr6 mt6"><i class="fa fa-image mr3" aria-hidden="true"></i>' . $img_count . '</badge>' : '';
        } elseif ($format == 'video') {
            $format_icon = '<i class="fa fa-play-circle em12 mr6 mt6 c-white opacity8" aria-hidden="true"></i>';
        }
    }
    $format_icon = $format_icon ? '<div class="abs-center right-top">' . $format_icon . '</div>' : '';

    if (is_sticky()) {
        $sticky = '<badge class="img-badge left jb-red">置顶</badge>';
    } else {
        $sticky = '';
    }

    $html = '<div class="' . $class . '">';
    $html .= $_thumb;
    $html .= $format_icon;
    $html .= $sticky;
    $html .= '</div>';
    return $html;
}

function el_get_svg($name, $viewBox = null, $class = "icon")
{
    $viewBox = $viewBox ? ' data-viewBox="' . $viewBox . '" viewBox="' . $viewBox . '"' : '';
    return '<svg class="' . $class . '" aria-hidden="true"' . $viewBox . '><use xlink:href="#icon-' . $name . '"></use></svg>';
}


//判断是否是蜘蛛爬虫
function el_is_crawler()
{

    static $el_is_crawler = 'is-null';
    if ($el_is_crawler !== 'is-null') {
        return $el_is_crawler;
    }

    $bots = array(
        'Baidu'         => 'baiduspider',
        'Google Bot'    => 'google',
        '360spider'     => '360spider',
        'Sogou'         => 'spider',
        'soso.com'      => 'sosospider',
        'MSN'           => 'msnbot',
        'Alex'          => 'ia_archiver',
        'Lycos'         => 'lycos',
        'Ask Jeeves'    => 'jeeves',
        'Altavista'     => 'scooter',
        'AllTheWeb'     => 'fast-webcrawler',
        'Inktomi'       => 'slurp@inktomi',
        'Turnitin.com'  => 'turnitinbot',
        'Technorati'    => 'technorati',
        'Yahoo'         => 'yahoo',
        'Findexa'       => 'findexa',
        'NextLinks'     => 'findlinks',
        'Gais'          => 'gaisbo',
        'WiseNut'       => 'zyborg',
        'WhoisSource'   => 'surveybot',
        'Bloglines'     => 'bloglines',
        'BlogSearch'    => 'blogsearch',
        'PubSub'        => 'pubsub',
        'Syndic8'       => 'syndic8',
        'RadioUserland' => 'userland',
        'Gigabot'       => 'gigabot',
        'Become.com'    => 'become.com',
        'Yandex'        => 'yandex',
    );
    $useragent      = isset($_SERVER['HTTP_USER_AGENT']) ? addslashes(strtolower($_SERVER['HTTP_USER_AGENT'])) : '';
    $el_is_crawler = false;
    if ($useragent) {
        foreach ($bots as $name => $lookfor) {
            if (!empty($useragent) && (false !== stripos($useragent, $lookfor))) {
                $el_is_crawler = $name;
            }
        }
    }

    return $el_is_crawler;
}
/**
 * @description: 获取用户页面的链接
 * @param {*} $id
 * @return {*}
 */
function el_get_user_home_url($id = 0, $query = false)
{
    if (!$id) {
        $id = get_current_user_id();
    }

    if (!$id) {
        return;
    }

    //先从缓存查询
    $cache = ECache::get($id,'user_home_url');
    if ($cache !== false) {
        $url = $cache;
    } else {
        $url = get_author_posts_url($id);
        ECache::set($id,$url, 'user_home_url');
    }

    if ($query) {
        $url = add_query_arg($query, $url);
    }

    return $url;
}