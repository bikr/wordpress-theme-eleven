<?php
/*
 * @Author       : ZengHao
 * @Date         : 2024-01-04 08:37:01
 * @LastEditors  : ZengHao
 * @LastEditTime : 2024-02-21 20:44:30
 * @FilePath     : /inc/widgets/widget-notice.php
 * @Description  : 
 * Copyright 2024 www.exehub.net, All Rights Reserved. 
 * 2024-01-04 08:37:01
 */

// Control core classes for avoid errors
if (class_exists('CSF')) {
    CSF::createWidget('el_notice_widget_cfs', array(
        'title'       => 'EL-滚动公告',
        'description' => '可做公告栏或者其他滚动显示内容，只显示一行',
        'fields'      => array(
            // 这是全局的设置
            array(
                'id'            => 'option',
                'type'          => 'accordion',
                'title'         => '公告设置',
                'default'   => array(
                    'hide'   => '',
                    'color'  => '#eeeeee'
                ),
                'accordions'    => array(
                    array(
                        'title'     => '公告设置',
                        'icon'      => 'fa fa-fw fa-angle-right',
                        'fields'    => array(
                            $args[] = array(
                                'title'   => __("显示规则", 'el_language') . el_new_badge()['1.1'],
                                'id'      => 'hide',
                                'type'    => "radio",
                                'inline'  => true,
                                'options' => array(
                                    ''   => '全部显示',
                                    'pc' => 'PC端不显示',
                                    'sm'  => '移动端不显示',
                                ),
                                'default' => '',
                            ),
                            array(
                                'title'    => __('主题颜色', 'el_language'),
                                'id'      => 'color',
                                'type'    => 'select',
                                'options' => array(
                                    'rand'       => '随机颜色',
                                    'c-blue'     => '蓝色',
                                    'c-blue-2'   => '深蓝色',
                                    'c-cyan'     => '青色',
                                    'c-yellow'   => '黄色',
                                    'c-yellow-2' => '橙黄色',
                                    'c-green'    => '绿色',
                                    'c-green-2'  => '墨绿色',
                                    'c-purple'   => '紫色',
                                    'c-purple-2' => '深紫色',
                                    'c-red'      => '粉红色',
                                    'c-red-2'    => '红色',
                                ),
                            ),
                            array(
                                'title'    => __('滚动方向', 'el_language'),
                                'id'      => 'direction',
                                'type'    => 'select',
                                'options' => array(
                                    'horizontal' => '左右滚动',
                                    'vertical'   => '上下滚动'
                                ),
                                'default' => 'vertical',
                            ),
                        )
                    )
                )
            ),
            // 这是添加 滚动公告(一列) 公告的地方
            array(
                'id'        => 'el_notices',
                'type'      => 'group',
                'title'     => '公告内容',
                'min'   => '1',
                'accordion_title_number'   => true,
                'accordion_title_auto'   => false,
                'accordion_title_prefix' => '公告内容',
                'button_title' => '添加公告',
                'subtitle' => '添加公告',
                'default'   => array(
                    array(
                        'con'     => '滚动公告',
                        'incon'   => '',
                        'link'    => array(
                            'url' => 'https://eleven.exehub.net/',
                            'target' => '_blank',
                        ),
                    ),
                ),
                'fields'    => array(
                    array(
                        'id'           => 'con',
                        'type'         => 'text',
                        'title'        => '公告内容',
                        'desc'         => '',
                        'default'      => '滚动公告',
                    ),
                    array(
                        'id'           => 'icon',
                        'type'         => 'text',
                        'title'        => '图标',
                        'desc'         => '填写html标签',
                        'default'      => '<i class="abs-center fa fa-home"></i>',
                    ),
                    array(
                        'id'           => 'link',
                        'type'         => 'link',
                        'title'        => '跳转链接',
                        'default'      => array(),
                        'add_title'    => '添加链接',
                        'edit_title'   => '编辑链接',
                        'remove_title' => '删除链接',
                    )
                ),
            ),
        )
    ));

    if (!function_exists('el_notice_widget_cfs')) {
        function el_notice_widget_cfs($args, $instance)
        {
            $notice_option = $instance['option'];
            $notice_con = $instance['el_notices'];
            //判断配置是否为空
            $notice_option['notices'] = $notice_con;
            // print_r($notice_option);
            el_notice_widget_ui($notice_option);
        }
    }
}
