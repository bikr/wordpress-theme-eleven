<?php 
/*
 * @Author       : ZengHao
 * @Date         : 2024-01-04 23:17:52
 * @LastEditors  : ZengHao
 * @LastEditTime : 2024-01-31 21:34:28
 * @FilePath     : /inc/widgets/widget-tags.php
 * @Description  : 
 * Copyright 2024 www.exehub.com, All Rights Reserved. 
 * 2024-01-04 23:17:52
 */

 if (class_exists('CSF')) {
    CSF::createWidget('el_tags_widget_cfs', array(
        'title'       => 'EL-标签云',
        'description' => '展示网站标签内容',
        'fields'      => array(
            array(
                'title'   => __("显示规则", 'el_language') . el_new_badge()['1.1'],
                'id'      => 'hide',
                'type'    => "radio",
                'inline'  => true,
                'options' => array(
                    ''   => '全部显示',
                    'pc' => 'PC端不显示',
                    'sm'  => '移动端不显示',
                ),
                'default' => '',
            ),
            array(
                'title'    => __('主题颜色', 'el_language'),
                'id'      => 'color',
                'type'    => 'select',
                'options' => array(
                    'rand'       => '随机颜色',
                    'c-blue'     => '蓝色',
                    'c-blue-2'   => '深蓝色',
                    'c-cyan'     => '青色',
                    'c-yellow'   => '黄色',
                    'c-yellow-2' => '橙黄色',
                    'c-green'    => '绿色',
                    'c-green-2'  => '墨绿色',
                    'c-purple'   => '紫色',
                    'c-purple-2' => '深紫色',
                    'c-red'      => '粉红色',
                    'c-red-2'    => '红色',
                ),
            ),
            array(
                'id'           => 'title',
                'type'         => 'text',
                'title'        => '标题，可为空',
                'desc'         => '',
                'default'      => '标签云',
            ),
            array(
                'id'           => 'max_num',
                'type'         => 'number',
                'title'        => '最大数量',
                'desc'         => '',
                'default'      => '20',
            ),
            array(
                'id'      => 'target_blank',
                'type'    => 'checkbox',
                'title'   => '',
                'label'   => '新窗口打开',
                'default' => true
            ),
            array(
                'id'      => 'fixed_width',
                'type'    => 'checkbox',
                'title'   => '',
                'label'   => '固定宽度',
                'default' => true
            ),
            array(
                'id'      => 'show_num',
                'type'    => 'checkbox',
                'title'   => '',
                'label'   => '显示标签数量',
                'default' => false
            ),
        )
    ));

    if (!function_exists('el_tags_widget_cfs')) {
        function el_tags_widget_cfs($args, $instance)
        {
            $option = array(
                'hide'         =>'',
                'color'        =>'rand',
                'title'        =>'',

                'max_num'      =>20,
                'target_blank' =>true,
                'fixed_width'  =>true,
                'show_num'     =>false,
            );
            $option = wp_parse_args((array) $instance, $option);
            //判断配置是否为空
            el_tags_widget_ui($option);
        }
    }
}
