<?php 
/*
 * @Author       : ZengHao
 * @Date         : 2023-12-30 18:44:35
 * @LastEditors  : ZengHao
 * @LastEditTime : 2024-02-23 12:39:20
 * @FilePath     : /inc/widgets/widgets.php
 * @Description  : 
 * Copyright 2023 www.exehub.com, All Rights Reserved. 
 * 2023-12-30 18:44:35
 */

 // 关闭5.8 新版小工具编辑器
if (floatval(get_bloginfo('version')) >= 5.8) {
    add_filter('gutenberg_use_widgets_block_editor', '__return_false');
    add_filter('use_widgets_block_editor', '__return_false');
} else {
    add_filter('file_is_displayable_image', 'corepress_webp_file_display', 10, 2);
}

if(_opz('el_clean_widget')){
    add_action('widgets_init', 'unregister_d_widget');
}
function unregister_d_widget()
{
    unregister_widget('WP_Widget_RSS');
    unregister_widget('WP_Widget_Recent_Posts');
    unregister_widget('WP_Widget_Recent_Comments');
    unregister_widget('WP_Widget_Pages');
    unregister_widget('WP_Widget_Search');
    unregister_widget('WP_Widget_Calendar');
    unregister_widget('WP_Widget_Recent_Comments');
    unregister_widget('WP_Widget_Meta');
    unregister_widget('WP_Widget_Categories');
    unregister_widget('WP_Widget_Block');
    unregister_widget('WP_Widget_Archives');
    unregister_widget('WP_Widget_Tag_Cloud');
    unregister_widget('WP_Widget_Text');
    unregister_widget('WP_Widget_Media_Audio');
    unregister_widget('WP_Widget_Media_Video');
}
//TODO:最新评论
$widgets = array(
    'slider',
    'notice',
    'tiny',
    'user',
    'tags',
    'link',
    'hotpost',
    'comment',
    'catpost',
    'search',
    'stat',
);

foreach ($widgets as $widget) {
    $path = 'inc/widgets/widget-' . $widget . '.php';
    require get_theme_file_path('/' . $path);
}

// 注册小工具
if (function_exists('register_sidebar')) {
    $sidebars = array();
    $pags     = array(
        'home'   => '首页',
        'single' => '文章页',
        'cat'    => '分类页',
        'tag'    => '标签页',
        'search' => '搜索页',
        'all'    => '所有页面',
    );

    $poss = array(
        'top_fluid'      => '顶部全宽度',
        'top_content'    => '主内容上面',
        'bottom_content' => '主内容下面',
        'bottom_fluid'   => '底部全宽度',
        'sidebar'        => '侧边栏',
    );

    foreach ($pags as $key => $value) {
        foreach ($poss as $poss_key => $poss_value) {
            $sidebars[] = array(
                'name'        => $value . '-' . $poss_value,
                'id'          => $key . '_' . $poss_key,
                'description' => '显示在 ' . $value . ' 的 ' . $poss_value . ' 位置，由于位置较多，建议使用实时预览管理！',
            );
        }
    }

    foreach ($sidebars as $value) {
        register_sidebar(array(
            'name'          => $value['name'],
            'id'            => $value['id'],
            'description'   => $value['description'],
            'before_widget' => '<div class="zib-widget %2$s">',
            'after_widget'  => '</div>',
            'before_title'  => '<h3>',
            'after_title'   => '</h3>',
        ));
    };
}

add_action('wp_ajax_el_get_widget_sentence', 'el_get_widget_sentence');
add_action('wp_ajax_nopriv_el_get_widget_sentence', 'el_get_widget_sentence');
function el_get_widget_sentence($type)
{
    $data = [];
    $api = 'https://v1.hitokoto.cn/' . $type;
    $data = el_HTTP::doGet($api);

    // return json_encode($data);
    return $data;
}