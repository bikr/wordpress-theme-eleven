<?php 
/*
 * @Author       : ZengHao
 * @Date         : 2024-01-07 12:32:55
 * @LastEditors  : ZengHao
 * @LastEditTime : 2024-02-21 21:05:19
 * @FilePath     : /inc/widgets/widget-catpost.php
 * @Description  : 
 * Copyright 2024 www.exehub.com, All Rights Reserved. 
 * 2024-01-07 12:32:55
 */

 if (class_exists('CSF')) {
    CSF::createWidget('el_catpost_widget_cfs', array(
        'title'       => 'EL-分类文章',
        'description' => '多样式显示分类文章，建议放非侧边栏位置',
        'fields'      => array(
            array(
                'title'   => __("显示规则", 'el_language') . el_new_badge()['1.1'],
                'id'      => 'hide',
                'type'    => "radio",
                'inline'  => true,
                'options' => array(
                    ''   => '全部显示',
                    'pc' => 'PC端不显示',
                    'sm'  => '移动端不显示',
                ),
                'default' => '',
            ),
            array(
                'id'           => 'title',
                'type'         => 'text',
                'title'        => '标题，可为空',
                'desc'         => '',
                'default'      => '分类文章',
            ),
            // array(
            //     'title'   => __("显示样式", 'el_language'),
            //     'id'      => 'type',
            //     'type'    => 'radio',
            //     'default' => 'style-1',
            //     'options' => array(
            //         'style-1'    => '样式1',
            //         'style-2'    => '样式2',
            //     ),
            // ),
            array(
                'id'      => 'orderby',
                'default' => 'date',
                'title'   => '文章排序',
                'type'    => "radio",
                'options' => array(
                    'random'        => '随机排序',
                    'views'         => '按阅读量排序',
                    'date'          => '按发布日期排序',
                    'comment_count' => '按评论量排序',
                    'favorite'      => '按收藏量排序',
                ),
            ),
            array(
                'title'    => __('排序方式', 'el_language'),
                'id'      => 'order',
                'type'    => 'select',
                'options' => array(
                    'ASC'       => '升序',
                    'DESC'       => '降序',
                ),
                'default'      => 'DESC',
            ),
            array(
                'id'      => 'count',
                'title'   => '最大显示数量',
                'default' => 6,
                'max'     => 20,
                'min'     => 1,
                'step'    => 1,
                'unit'    => '篇',
                'type'    => 'spinner',
            ),
            array(
                'id'          => 'term_id',
                'title'       => '选择分类',
                'desc'        => '',
                'options'     => 'categories',
                'query_args'  => array(
                    'taxonomy' => array('topics', 'category'),
                    'orderby'  => 'taxonomy',
                ),
                'placeholder' => '输入关键词以搜索分类或专题',
                'ajax'        => true,
                'settings'    => array(
                    'min_length' => 2,
                ),
                'chosen'      => true,
                'multiple'    => true,
                'sortable'    => true,
                'type'        => 'select',
            ),
            array(
                'title'   => '新窗口打开',
                'id'      => 'target_blank',
                'type'    => 'switcher',
                'default' => true,
            ),
        )
    ));

    if (!function_exists('el_catpost_widget_cfs')) {
        function el_catpost_widget_cfs($args, $instance)
        {
            $option = array(
                'hide'          =>'',
                'title'         =>'',
                'type'          =>0,
                'orderby'       =>'date',
                'order'         =>'DESC',
                'count'         =>'',
                'term_id'       =>'',
                'target_blank'  =>true
            );
            $option = wp_parse_args((array) $instance, $option);
            //判断配置是否为空
            el_catpost_widget_ui($option);
        }
    }
}
