<?php 
/*
 * @Author       : ZengHao
 * @Date         : 2024-01-03 21:22:54
 * @LastEditors  : ZengHao
 * @LastEditTime : 2024-02-21 20:44:50
 * @FilePath     : /inc/widgets/widget-tiny.php
 * @Description  : 
 * Copyright 2024 www.exehub.com, All Rights Reserved. 
 * 2024-01-03 21:22:54
 */

 if (class_exists('CSF')) {
    CSF::createWidget('el_tiny_widget_cfs', array(
        'title'       => 'EL-分类图文卡片',
        'description' => '将分类展示为图片卡片的形式',
        'fields'      => array(
            array(
                'title'   => __("显示规则", 'el_language') . el_new_badge()['1.1'],
                'id'      => 'hide',
                'type'    => "radio",
                'inline'  => true,
                'options' => array(
                    ''   => '全部显示',
                    'pc' => 'PC端不显示',
                    'sm'  => '移动端不显示',
                ),
                'default' => '',
            ),
            array(
                'id'           => 'title',
                'type'         => 'text',
                'title'        => '标题，可为空',
                'desc'         => '',
                'default'      => '分类图文',
            ),
            array(
                'title'   => __("卡片样式", 'el_language'),
                'id'      => 'type',
                'type'    => 'radio',
                'default' => 'style-3',
                'options' => array(
                    'style-1' => '简单样式',
                    'style-2' => '样式二',
                    'style-3' => '样式三',
                    'style-4' => '样式四',
                ),
            ),
            array(
                'id'      => 'height_scale',
                'class'   => 'compact',
                'title'   => '卡片长宽比例',
                'default' => 60,
                'max'     => 300,
                'min'     => 20,
                'step'    => 5,
                'unit'    => '%',
                'type'    => 'spinner',
            ),
            array(
                'id'      => 'mask_opacity',
                'title'   => '遮罩透明度',
                'help'    => '图片上显示的黑色遮罩层的透明度',
                'default' => 10,
                'class'   => 'compact',
                'max'     => 90,
                'min'     => 0,
                'step'    => 1,
                'unit'    => '%',
                'type'    => 'slider',
            ),
            array(
                'id'       => 'pc_row',
                'title'    => '排列布局',
                'subtitle' => 'PC端单行排列数量',
                'default'  => 4,
                'class'    => 'compact button-mini',
                'options'  => array(
                    1  => '1个',
                    2  => '2个',
                    3  => '3个',
                    4  => '4个',
                    6  => '6个',
                    12 => '12个',
                ),
                'type'     => 'button_set',
            ),
            array(
                'id'       => 'm_row',
                'title'    => ' ',
                'subtitle' => '移动端单行排列数量',
                'decs'     => '请根据此模块放置位置的宽度合理调整单行数量，避免显示不佳',
                'default'  => 2,
                'class'    => 'compact button-mini',
                'options'  => array(
                    1  => '1个',
                    2  => '2个',
                    3  => '3个',
                    4  => '4个',
                    6  => '6个',
                    12 => '12个',
                ),
                'type'     => 'button_set',
            ),
            array(
                'id'          => 'term_id',
                'title'       => '添加分类、专题',
                'desc'        => '选择并排序需要的分类、专题，如选择的分类(专题)下没有文章则不会显示',
                'options'     => 'categories',
                'query_args'  => array(
                    'taxonomy' => array('topics', 'category'),
                    'orderby'  => 'taxonomy',
                ),
                'placeholder' => '输入关键词以搜索分类或专题',
                'ajax'        => true,
                'settings'    => array(
                    'min_length' => 2,
                ),
                'chosen'      => true,
                'multiple'    => true,
                'sortable'    => true,
                'type'        => 'select',
            ),
            array(
                'title'   => '新窗口打开',
                'id'      => 'target_blank',
                'type'    => 'switcher',
                'default' => true,
            ),
        )
    ));

    if (!function_exists('el_tiny_widget_cfs')) {
        function el_tiny_widget_cfs($args, $instance)
        {
            $option = array(
                'hide' => '',
                'title'=>'',
                'type'=>'style-3',
                'height_scale'=>'',
                'mask_opacity'=>'',
                'pc_row'=>0,
                'm_row'=>0,
                'term_id'=>'',
                'target_blank'=>true
            );
            $option = wp_parse_args((array) $instance, $option);
            //判断配置是否为空
            el_tiny_widget_ui($option);
        }
    }
}
