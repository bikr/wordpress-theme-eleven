<?php 
/*
 * @Author       : ZengHao
 * @Date         : 2024-01-07 00:19:42
 * @LastEditors  : ZengHao
 * @LastEditTime : 2024-01-31 21:34:37
 * @FilePath     : /inc/widgets/widget-post.php
 * @Description  : 
 * Copyright 2024 www.exehub.com, All Rights Reserved. 
 * 2024-01-07 00:19:42
 */

 if (class_exists('CSF')) {
    CSF::createWidget('el_post_widget_cfs', array(
        'title'       => 'EL-文章列表',
        'description' => '多样化展示文章列表',
        'fields'      => array(
            array(
                'title'   => __("显示规则", 'el_language') . el_new_badge()['1.1'],
                'id'      => 'hide',
                'type'    => "radio",
                'inline'  => true,
                'options' => array(
                    ''   => '全部显示',
                    'pc' => 'PC端不显示',
                    'sm'  => '移动端不显示',
                ),
                'default' => '',
            ),
            array(
                'id'           => 'title',
                'type'         => 'text',
                'title'        => '标题，可为空',
                'desc'         => '',
                'default'      => '热门文章',
            ),
            array(
                'title'   => __("显示样式", 'el_language'),
                'id'      => 'type',
                'type'    => 'radio',
                'default' => 'style-3',
                'options' => array(
                    'card'    => '卡片',
                    'text'    => '文字',
                    'image'   => '图片',
                ),
            ),
            array(
                'id'      => 'orderby',
                'default' => 'views',
                'title'   => '榜单类型',
                'type'    => "radio",
                'options' => array(
                    'views'         => '热门榜单(按阅读量排序)',
                    'like'          => '超赞榜单(按点赞量排序)',
                    'comment_count' => '话题榜单(按评论量排序)',
                    'favorite'      => '收藏榜单(按收藏量排序)',
                ),
            ),
            array(
                'id'      => 'limit_day',
                'title'   => '限制时间(最近X天)',
                'desc'    => '设置多少天内发布的文章有效，为0则不限制时间',
                'default' => 0,
                'max'     => 999999,
                'min'     => 0,
                'step'    => 1,
                'unit'    => '天',
                'type'    => 'spinner',
            ),
            array(
                'id'      => 'count',
                'title'   => '最大显示数量',
                'default' => 6,
                'max'     => 20,
                'min'     => 1,
                'step'    => 1,
                'unit'    => '篇',
                'type'    => 'spinner',
            ),
            array(
                'id'          => 'term_id',
                'title'       => '排除分类',
                'desc'        => '',
                'options'     => 'categories',
                'query_args'  => array(
                    'taxonomy' => array('topics', 'category'),
                    'orderby'  => 'taxonomy',
                ),
                'placeholder' => '输入关键词以搜索分类或专题',
                'ajax'        => true,
                'settings'    => array(
                    'min_length' => 2,
                ),
                'chosen'      => true,
                'multiple'    => true,
                'sortable'    => true,
                'type'        => 'select',
            ),
            array(
                'title'   => '新窗口打开',
                'id'      => 'target_blank',
                'type'    => 'switcher',
                'default' => true,
            ),
        )
    ));

    if (!function_exists('el_post_widget_cfs')) {
        function el_post_widget_cfs($args, $instance)
        {
            $option = array(
                'hide'          =>'',
                'title'         =>'',
                'type'          =>0,
                'orderby'       =>0,
                'limit_day'     =>'',
                'count'         =>'',
                'term_id'       =>'',
                'target_blank'  =>true
            );
            $option = wp_parse_args((array) $instance, $option);
            //判断配置是否为空
            // el_hotpost_widget_ui($option);
        }
    }
}
