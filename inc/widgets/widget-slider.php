<?php
/*
 * @Author       : ZengHao
 * @Date         : 2023-12-30 22:13:46
 * @LastEditors  : ZengHao
 * @LastEditTime : 2024-01-06 00:21:28
 * @FilePath     : /inc/widgets/widget-slider.php
 * @Description  : 
 * Copyright 2023 www.exehub.com, All Rights Reserved. 
 * 2023-12-30 22:13:46
 */
// Control core classes for avoid errors
if (class_exists('CSF')) {
    $imagepath =  EL_ASSETS . 'img/';
    CSF::createWidget('el_widget_ui_cfs', array(
        'title'       => 'EL-幻灯片',
        'description' => '高级幻灯片',
        'fields'      => array(
            // 这是全局的设置
            array(
                'id'            => 'option',
                'type'          => 'accordion',
                'title'         => '幻灯片设置',
                'default'   => array(
                    'hide'                => '',
                    'direction'           => 'horizontal', // 滚动方向 horizontal 左右滚动 / vertical 上下滚动
                    'show_btn'            => true, // 是否显示按钮，上下滚动不显示
                    'autoplay'            => true, // 是否自动播放，默认是
                    'loop'                => true, // 是否循环播放 / 默认是
                    'pagination'          => true, // 是否显示分页面滚动条
                    'effect'              => true, 
                    'interval'            => 1000, // 定时切换间隔，默认 1 S
                    'slide'               => true, // 切换动画  
                    'spacebetween'        => 15,
                    'pc_height'           => 400,
                    'm_height'            => 200
                ),
                'accordions'    => array(
                    array(
                        'title'     => '幻灯片设置',
                        'icon'      => 'fa fa-fw fa-angle-right',
                        'fields'    => EL_CFS_Module::slide_setting(),
                    )
                )
            ),
            // 这是添加幻灯片的地方
            array(
                'id'        => 'slides',
                'type'      => 'group',
                'title'     => '幻灯片内容',
                'min'   => '1',
                'accordion_title_number'   => true,
                'accordion_title_auto'   => false,
                'accordion_title_prefix' => '幻灯片',
                'button_title' => '添加幻灯片',
                'subtitle' => '添加幻灯片',
                'fields'    => EL_CFS_Module::add_slider(),
                'default'   => array(
                    array(
                        'image' => $imagepath . 'slider-bg.jpg',
                        'link'  => array(
                            'url' => 'https://eleven.exehub.net/',
                            'target' => '_blank',
                        ),
                        'desc'  => '',
                        'title'  => '',
                        'badge'  => '推荐',
                    ),
                ),
            ),
        )
    ));

    if (!function_exists('el_widget_ui_cfs')) {
        function el_widget_ui_cfs($args, $instance)
        {
            $header_slider = $instance['slides'];
            $header_slider_option = $instance['option'];

            //判断配置是否为空
            $header_slider_option['slides'] = $header_slider;
            el_slider_widget_ui($header_slider_option);
        }
    }
}
