<?php 
/*
 * @Author       : ZengHao
 * @Date         : 2024-01-05 23:57:17
 * @LastEditors  : ZengHao
 * @LastEditTime : 2024-02-18 19:09:31
 * @FilePath     : /inc/widgets/widget-hotpost.php
 * @Description  : 
 * Copyright 2024 www.exehub.com, All Rights Reserved. 
 * 2024-01-05 23:57:17
 */

 if (class_exists('CSF')) {
    CSF::createWidget('el_hot_post_widget_cfs', array(
        'title'       => 'EL-热门文章',
        'description' => '多样式显示热门文章',
        'fields'      => array(
            array(
                'title'   => __("显示规则", 'el_language') . el_new_badge()['1.1'],
                'id'      => 'hide',
                'type'    => "radio",
                'inline'  => true,
                'options' => array(
                    ''   => '全部显示',
                    'pc' => 'PC端不显示',
                    'sm'  => '移动端不显示',
                ),
                'default' => '',
            ),
            array(
                'id'           => 'title',
                'type'         => 'text',
                'title'        => '标题，可为空',
                'desc'         => '',
                'default'      => '热门文章',
            ),
            array(
                'id'      => 'orderby',
                'default' => 'views',
                'title'   => '榜单类型',
                'type'    => "radio",
                'options' => array(
                    'views'         => '热门榜单(按阅读量排序)',
                    'like'          => '超赞榜单(按点赞量排序)',
                    'comment_count' => '话题榜单(按评论量排序)',
                    'favorite'      => '收藏榜单(按收藏量排序)',
                ),
            ),
            array(
                'id'      => 'limit_day',
                'title'   => '限制时间(最近X天)',
                'desc'    => '设置多少天内发布的文章有效，为0则不限制时间',
                'default' => 0,
                'max'     => 999999,
                'min'     => 0,
                'step'    => 1,
                'unit'    => '天',
                'type'    => 'spinner',
            ),
            array(
                'id'      => 'count',
                'title'   => '最大显示数量',
                'default' => 6,
                'max'     => 20,
                'min'     => 1,
                'step'    => 1,
                'unit'    => '篇',
                'type'    => 'spinner',
            ),
            array(
                'title'   => '新窗口打开',
                'id'      => 'target_blank',
                'type'    => 'switcher',
                'default' => true,
            ),
        )
    ));

    if (!function_exists('el_hot_post_widget_cfs')) {
        function el_hot_post_widget_cfs($args, $instance)
        {
            el_hotpost_widget_ui($instance);
        }
    }
}
