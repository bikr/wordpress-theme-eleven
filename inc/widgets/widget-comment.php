<?php
/*
 * @Author       : ZengHao
 * @Date         : 2024-02-26 14:33:23
 * @LastEditors  : ZengHao
 * @LastEditTime : 2024-02-26 14:46:01
 * @FilePath     : \inc\widgets\widget-comment.php
 * @Description  : 
 * Copyright 2024 www.exehub.net, All Rights Reserved. 
 * 2024-02-26 14:33:23
 */

// Control core classes for avoid errors
if (class_exists('CSF')) {
    CSF::createWidget('el_comment_widget_cfs', array(
        'title'       => 'EL-最近评论',
        'description' => '多样式展示网站评论等',
        'fields'      => array(
            array(
                'title'   => __("显示规则", 'el_language') . el_new_badge()['1.1'],
                'id'      => 'hide',
                'type'    => "radio",
                'inline'  => true,
                'options' => array(
                    ''   => '全部显示',
                    'pc' => 'PC端不显示',
                    'sm'  => '移动端不显示',
                ),
                'default' => '',
            ),
            array(
                'id'           => 'title',
                'type'         => 'text',
                'title'        => '标题，可为空',
                'desc'         => '',
                'default'      => '最新评论',
            ),
            array(
                'id'      => 'count',
                'title'   => '最大显示数量',
                'default' => 6,
                'max'     => 50,
                'min'     => 1,
                'step'    => 1,
                'unit'    => '条',
                'type'    => 'spinner',
            ),
            array(
                'id'           => 'outer',
                'type'         => 'text',
                'title'        => '排除用户ID',
                'desc'         => '',
                'default'      => '1',
            ),
            array(
                'id'           => 'outpost',
                'type'         => 'text',
                'title'        => '排除文章ID',
                'desc'         => '',
                'default'      => '',
            ),
            array(
                'id'      => 'target_blank',
                'type'    => 'checkbox',
                'title'   => '',
                'label'   => '新窗口打开',
                'desc'    => '如果链接未指定打开方式，则按这个模式打开',
                'default' => true
            ),
        )
    ));

    if (!function_exists('el_comment_widget_cfs')) {
        function el_comment_widget_cfs($args, $instance)
        {
            el_comment_widget_ui($instance);
        }
    }
}
