<?php
/*
 * @Author       : ZengHao
 * @Date         : 2024-02-03 15:00:26
 * @LastEditors  : ZengHao
 * @LastEditTime : 2024-02-03 16:08:42
 * @FilePath     : /inc/widgets/widget-search.php
 * @Description  : 
 * Copyright 2024 www.exehub.com, All Rights Reserved. 
 * 2024-02-03 15:00:26
 */

// Control core classes for avoid errors
if (class_exists('CSF')) {
    CSF::createWidget('el_search_widget_cfs', array(
        'title'       => 'EL-搜索模块',
        'description' => '展示搜索框',
        'fields'      => array(
            array(
                'title'   => __("显示规则", 'el_language') . el_new_badge()['1.1'],
                'id'      => 'hide',
                'type'    => "radio",
                'inline'  => true,
                'options' => array(
                    ''   => '全部显示',
                    'pc' => 'PC端不显示',
                    'sm'  => '移动端不显示',
                ),
                'default' => '',
            ),
            array(
                'id'           => 'title',
                'type'         => 'text',
                'title'        => '标题，可为空',
                'desc'         => '',
                'default'      => '搜索模块',
            ),
            
            // array(
            //     'title'    => __('搜索模式', 'el_language'),
            //     'id'      => 'type',
            //     'type'    => 'select',
            //     'options' => array(
            //         'inner'         => '站内搜索',
            //         'baidu'         => '百度搜索',
            //         'bing'          => '必应搜索',
            //         'google'        => '谷歌搜索',
            //     ),
            //     'default'      => 'inner',
            // ),
        )
    ));

    if (!function_exists('el_search_widget_cfs')) {
        function el_search_widget_cfs($args, $instance)
        {
            $option = array(
                'hide'         =>'',
                'title'        =>'',
                'type'         =>'inner'
            );
            $option = wp_parse_args((array) $instance, $option);
            // print_r($notice_option);
            el_search_widget_ui($option);
        }
    }
}
