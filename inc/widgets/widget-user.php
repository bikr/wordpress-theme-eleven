<?php
/*
 * @Author       : ZengHao
 * @Date         : 2024-01-04 15:20:40
 * @LastEditors  : ZengHao
 * @LastEditTime : 2024-01-06 00:22:08
 * @FilePath     : /inc/widgets/widget-user.php
 * @Description  : 
 * Copyright 2024 www.exehub.net, All Rights Reserved. 
 * 2024-01-04 15:20:40
 */

// 参考ui：https://pandapro.demo.nicetheme.xyz/
if (class_exists('CSF')) {
    CSF::createWidget('el_user_widget_cfs', array(
        'title'       => 'EL-用户卡片',
        'description' => '可做博主信息展示，展示网站信息',
        'fields'      => array(
            array(
                'title'   => __("显示规则", 'el_language') . el_new_badge()['1.1'],
                'id'      => 'hide',
                'type'    => "radio",
                'inline'  => true,
                'options' => array(
                    ''   => '全部显示',
                    'pc' => 'PC端不显示',
                    'sm'  => '移动端不显示',
                ),
                'default' => '',
            ),
            array(
                'id' => 'card_bg',
                'title' => __('封面图', 'el_language'),
                'default' => $imagepath . 'user_t.jpg',
                'library' => 'image',
                'type' => 'upload',
            ),
            array(
                'id'           => 'username',
                'type'         => 'text',
                'title'        => '展示博主名称',
                'desc'         => '',
                'default'      => 'Eleven',
            ),
            array(
                'title' => '个性描述类型',
                'id' => 'sign_type',
                'options' => array(
                    'custom' => '自定义',
                    'sentence' => '一言接口', // https://developer.hitokoto.cn/sentence/
                ),
                'type' => 'select',
                'default' => 'sentence',
            ),
            array(
                'dependency' => array('sign_type', '==', 'custom'),
                'id'           => 'signature',
                'type'         => 'textarea',
                'title'        => '自定义个性签名',
                'desc'         => '',
                'default'      => 'Eleven-自媒体主题',
            ),
            array(
                'title' => __('统计信息', 'el_language'),
                'id' => 'show_stat',
                'desc' => '开启后会展示网站统计信息',
                'type' => "switcher",
                'default' => true,
            ),
            array(
                'title' => __('最新文章', 'el_language'),
                'id' => 'show_post',
                'desc' => '开启后会展示网站最新的文章',
                'type' => "switcher",
                'default' => true,
            ),
            array(
                'dependency' => array('show_post', '==', 'true'),
                'id'          => 'term_id',
                'title'       => '添加分类、专题',
                'desc'        => '选择并排序需要的分类、专题，如选择的分类(专题)下没有文章则不会显示',
                'options'     => 'categories',
                'query_args'  => array(
                    'taxonomy' => array('topics', 'category'),
                    'orderby'  => 'taxonomy',
                ),
                'placeholder' => '输入关键词以搜索分类或专题',
                'ajax'        => true,
                'settings'    => array(
                    'min_length' => 2,
                ),
                'chosen'      => true,
                'multiple'    => true,
                'sortable'    => true,
                'type'        => 'select',
            ),
            array(
                'title' => __('社交信息', 'el_language'),
                'id' => 'show_soc',
                'desc' => '开启后会展示网站社交信息，例如公众号等',
                'type' => "switcher",
                'default' => true,
            ),
            array(
                'dependency' => array('show_soc', '==', 'true'),
                'id'        => 'socialize',
                'type'      => 'group',
                'title'     => '社交信息',
                'min'   => '1',
                'accordion_title_number'   => true,
                'accordion_title_auto'   => false,
                'accordion_title_prefix' => '社交信息',
                'button_title' => '添加社交',
                'subtitle' => '添加社交，建议最多四个',
                'default'   => array(
                    array(
                        'icon'     => 'fa fa-heart',
                        'title'    => 'Eleven',
                        'link_type'   => '',
                        'link'    => array(
                            'url' => 'https://eleven.exehub.net/',
                            'target' => '_blank',
                        ),
                        'soc_img'   => ''
                    ),
                ),
                'fields'    => array(
                    array(
                        'id'           => 'icon',
                        'title'        => ' ',
                        'subtitle'     => '标题图标',
                        'type'         => 'icon',
                        'class'        => 'compact',
                        'default'      => 'fa fa-heart',
                        'button_title' => '选择图标',
                    ),
                    array(
                        'id'           => 'title',
                        'type'         => 'text',
                        'title'        => '图标标题',
                        'desc'         => '展示在图标下方，可无',
                        'default'      => 'Eleven',
                    ),
                    array(
                        'title' => '图标展示类型',
                        'id' => 'link_type',
                        'options' => array(
                            'link' => '链接',
                            'img' => '图片',
                        ),
                        'type' => 'select',
                        'default' => 'link',
                        'desc'  => '链接时，点击图标会跳转，图片时，点击和hover图标，展示图片'
                    ),
                    array(
                        'dependency' => array('link_type', '==', 'link'),
                        'id'           => 'tooltips',
                        'type'         => 'text',
                        'title'        => '提示语',
                        'desc'         => '鼠标移动上去的提示语',
                        'default'      => '',
                    ),
                    array(
                        'dependency' => array('link_type', '==', 'link'),
                        'id'           => 'link',
                        'type'         => 'link',
                        'title'        => '跳转链接',
                        'default'      => array(),
                        'add_title'    => '添加链接',
                        'edit_title'   => '编辑链接',
                        'remove_title' => '删除链接',
                    ),
                    array(
                        'dependency' => array('link_type', '==', 'img'),
                        'id' => 'soc_img',
                        'title' => __('图片', 'el_language'),
                        'default' => '',
                        'library' => 'image',
                        'type' => 'upload',
                    ),
                ),
            ),
        )
    ));

    if (!function_exists('el_user_widget_cfs')) {
        function el_user_widget_cfs($args, $instance)
        {
            $usercard = array(
                'hide'       => '',
                'card_bg'    => '',
                'username'   => '',
                'sign_type'  => '',
                'signature'  => '',
                'show_stat'  => '',
                'show_post'  => '',
                'term_id'    => '',
                'show_soc'   => '',
                'socialize'  => ''
            );
            $usercard = wp_parse_args((array) $instance, $usercard);
            //判断配置是否为空
            // print_r($usercard);  
            el_user_widget_ui($usercard);
        }
    }
}
