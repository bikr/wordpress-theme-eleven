<?php
/*
 * @Author       : ZengHao
 * @Date         : 2024-01-05 12:39:38
 * @LastEditors  : ZengHao
 * @LastEditTime : 2024-01-31 21:32:44
 * @FilePath     : /inc/widgets/widget-link.php
 * @Description  : 
 * Copyright 2024 www.exehub.com, All Rights Reserved. 
 * 2024-01-05 12:39:38
 */

// Control core classes for avoid errors
if (class_exists('CSF')) {
    $options_linkcats_obj = get_terms(['taxonomy' => 'link_category'], ['hide_empty' => false]);
    $options = array();
    foreach ($options_linkcats_obj as $tag) {
        $options[$tag->term_id] = $tag->name;
    }
    CSF::createWidget('el_link_widget_cfs', array(
        'title'       => 'EL-链接模块',
        'description' => '多样式展示网站链接，友链等',
        'fields'      => array(
            array(
                'title'   => __("显示规则", 'el_language') . el_new_badge()['1.1'],
                'id'      => 'hide',
                'type'    => "radio",
                'inline'  => true,
                'options' => array(
                    ''   => '全部显示',
                    'pc' => 'PC端不显示',
                    'sm'  => '移动端不显示',
                ),
                'default' => '',
            ),
            array(
                'id'           => 'title',
                'type'         => 'text',
                'title'        => '标题，可为空',
                'desc'         => '',
                'default'      => '链接模块',
            ),
            array(
                'title'    => __('链接分类', 'el_language'),
                'id'      => 'link_cat',
                'type'    => 'select',
                'options' => $options,
                'default'      => '',
            ),
            array(
                'title'    => __('排序模式', 'el_language'),
                'id'      => 'orderby',
                'type'    => 'select',
                'options' => array(
                    'link_id'         => '链接ID',
                    'name'            => '链接名字',
                    'length'          => '链接名长度',
                    'rand'            => '随机',
                ),
                'default'      => 'rand',
            ),
            array(
                'title'    => __('排序方式', 'el_language'),
                'id'      => 'order',
                'type'    => 'select',
                'options' => array(
                    'ASC'       => '升序',
                    'DESC'       => '降序',
                ),
                'default'      => 'DESC',
            ),
            array(
                'id'           => 'limit',
                'type'         => 'number',
                'title'        => '限制输出数量',
                'subtitle'     => '-1 表示不限制',
                'desc'         => '',
                'default'      => '-1',
            ),
            array(
                'title'    => __('显示模式', 'el_language'),
                'id'      => 'show_type',
                'type'    => 'select',
                'options' => array(
                    'card'       => '卡片',
                    'text'       => '文字',
                    'image'      => '图片',
                ),
            ),
            array(
                'dependency' => array('show_type', '==', 'text'),
                'id'           => 'link_icon',
                'title'        => ' ',
                'subtitle'     => '标题图标',
                'type'         => 'icon',
                'class'        => 'compact',
                'default'      => 'fa fa-link',
                'button_title' => '选择图标',
            ),
            array(
                'dependency' => array('show_type', '!=', 'text'),
                'title'   => __('默认图片', 'el_language'),
                'subtitle'     => '链接威图片模式时，链接未设置图片，则显示该图片',
                'id'      => 'image',
                'default' => '',
                'preview' => true,
                'library' => 'image',
                'type'    => 'upload',
            ),
            array(
                'id'      => 'target_blank',
                'type'    => 'checkbox',
                'title'   => '',
                'label'   => '新窗口打开',
                'desc'    => '如果链接未指定打开方式，则按这个模式打开',
                'default' => true
            ),
        )
    ));

    if (!function_exists('el_link_widget_cfs')) {
        function el_link_widget_cfs($args, $instance)
        {
            $option = array(
                'hide'         =>'',
                'color'        =>'rand',
                'title'        =>'',
                'link_cat'     =>'',
                'orderby'      =>'rand',
                'order'        =>'DESC',
                'limit'        =>'-1',
                'show_type'    =>'card',
                'link_icon'    =>'',
                'image'        =>'',
                'target_blank' =>true,
            );
            $option = wp_parse_args((array) $instance, $option);
            // print_r($notice_option);
            el_link_widget_ui($option);
        }
    }
}
