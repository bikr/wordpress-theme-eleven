<?php
/*
 * @Author       : ZengHao
 * @Date         : 2024-02-26 14:33:23
 * @LastEditors  : ZengHao
 * @LastEditTime : 2024-02-26 23:45:41
 * @FilePath     : /inc/widgets/widget-stat.php
 * @Description  : 
 * Copyright 2024 www.exehub.net, All Rights Reserved. 
 * 2024-02-26 14:33:23
 */

// Control core classes for avoid errors
if (class_exists('CSF')) {
    CSF::createWidget('el_stat_widget_cfs', array(
        'title'       => 'EL-网站统计',
        'description' => '网站统计模块',
        'fields'      => array(
            array(
                'title'   => __("显示规则", 'el_language') . el_new_badge()['1.1'],
                'id'      => 'hide',
                'type'    => "radio",
                'inline'  => true,
                'options' => array(
                    ''   => '全部显示',
                    'pc' => 'PC端不显示',
                    'sm'  => '移动端不显示',
                ),
                'default' => '',
            ),
            array(
                'id'           => 'title',
                'type'         => 'text',
                'title'        => '标题，可为空',
                'desc'         => '',
                'default'      => '最新评论',
            ),
            array(
                'id'         => 'show_mod',
                'type'       => 'checkbox',
                'title'      => '显示模块',
                'desc'       => '选择需要显示哪些统计项目',
                'options'    => array(
                    'count_posts'     => '文章总数',
                    'count_comments'  => '评论总数',
                    'count_tags'      => '标签总数',
                    'count_pages'     => '页面总数',
                    'count_categories'      => '分类总数',
                    'count_users'     => '用户总数',
                    'count_links'     => '链接总数',
                    'last_update'    => '最后更新',
                ),
                'default'    => array('count_posts', 'count_links','count_comments', 'count_tags', 'count_pages', 'count_categories', 'count_users','last_update')
            ),
        )
    ));

    if (!function_exists('el_stat_widget_cfs')) {
        function el_stat_widget_cfs($args, $instance)
        {
            el_stat_widget_ui($instance);
        }
    }
}
