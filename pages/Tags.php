<?php
/*
 * @Author       : ZengHao
 * @Date         : 2024-02-23 19:45:40
 * @LastEditors  : ZengHao
 * @LastEditTime : 2024-02-24 17:50:23
 * @FilePath     : /pages/Tags.php
 * @Description  : 
 * Copyright 2024 www.exehub.com, All Rights Reserved. 
 * 2024-02-23 19:45:40
 */
/*Template Name: 标签页*/

if (!defined('ABSPATH'))
    exit;
// 处理数据
function el_tags_list()
{
    //准备数据
    $tag_args = array(
        'taxonomy' => 'post_tag',
        'orderby' => 'count',
        'order' => 'DESC',
        'hide_empty' => true,
        'count' => true,
    );
    $tags = ECache::get('el_tags_list');
    if (false === $tags) {
        $tags = get_terms($tag_args);
        ECache::set('el_tags_list', $tags);
    }
    $html = '<div class="tags-page"><h2>所有标签</h2>';
    $html .= '<div class="all-tags">';
    $html .= '<ul class="tag-list" itemprop="keywords">';
    $tagh = '';
    foreach ($tags  as $tag) {
        $url = esc_url(get_term_link(intval($tag->term_id), $tag->taxonomy));
        $tagh .= '<li class="tag-list-item">';
        $tagh .= '<a class="tag-list-link" href="'.$url.'" target="_blank" rel="tag" >' . $tag->name . '</a>';
        $tagh .= '<span class="tag-list-count">' . $tag->count . '</span>';
        $tagh .= '</li>';
    }
    $html .= $tagh;
    $html .= '</ul></div></div>';
    echo $html;
}
get_header();
?>
<main class="el-main container">
    <div class="row">
        <div class="col-lg-12 col-md-12 ">
            <?php
            if (function_exists('dynamic_sidebar')) {
                dynamic_sidebar('all_top_fluid');
                dynamic_sidebar('all_top_fluid');
            }
            ?>
        </div>
        <div class="<?php echo !el_is_show_sidebar() ? 'col-lg-12' : 'col-lg-8' ?> main-container">
            <!-- 正上方的小组件 -->
            <?php
            if (function_exists('dynamic_sidebar')) {
                dynamic_sidebar('all_top_content');
            }
            ?>
            <div class="post-loop-default mb-3">
                <!-- TOTO: iscard判断-->
                <ul class="post-ul <?php echo iscard('home'); ?>">
                    <?php el_tags_list(); ?>
                </ul>
                <?php el_paging(); ?>
            </div>
            <!-- 正文下面的小组件 -->
            <?php
            if (function_exists('dynamic_sidebar')) {
                dynamic_sidebar('all_bottom_content');
            }
            ?>
        </div>
        <div class="col-lg-4 slidebar-container">
            <?php get_sidebar(); ?>
        </div>
        <div class="col-lg-12 bottom-full-container">
            <?php
            if (function_exists('dynamic_sidebar')) {
                dynamic_sidebar('all_bottom_fluid');
            }
            ?>
        </div>
    </div>
</main>
<?php
get_footer();
?>