<?php
/*
 * @Author       : ZengHao
 * @Date         : 2024-02-23 19:45:40
 * @LastEditors  : ZengHao
 * @LastEditTime : 2024-02-24 18:04:30
 * @FilePath     : /pages/Category.php
 * @Description  : 
 * Copyright 2024 www.exehub.com, All Rights Reserved. 
 * 2024-02-23 19:45:40
 */
/*Template Name: 分类页*/

if (!defined('ABSPATH'))
    exit;
// 处理数据
function el_categories_list()
{
    //准备数据
    $cat_args = array(
        'orderby' => 'name',
        'order' => 'DESC',
        'hide_empty' => 1,
        'hierarchical' => 1,
        'taxonomy' => 'category',
        'pad_counts' => false
    );

    $cats = ECache::get('el_cats_list');
    if (false === $cats) {
        $cats = get_categories( $cat_args );
        ECache::set('el_cats_list', $cats);
    }
    // print_r($cats);
    $html = '<div class="cats-box mb-3">';
    $html .= '<h2>所有分类</h2>';
    $html .= '<div class="all-cats">';
    $cath ='';
    foreach ($cats as $cat) {
        // print_r($cat);
        $cath .= '<a class="" rel="category" href="'.esc_url(get_category_link($cat->term_id)).'" target="_blank">';
        $cath .= '<span>'.el_icon('folder-minus').'&nbsp;&nbsp;'.$cat->name.'</span>';
        $cath .= '<span class="title">('.$cat->count.')</span>';
        $cath .= '</a>';
    }
    $html .= $cath;
    $html .= '</div></div>';


    echo $html;
}
get_header();
?>
<main class="el-main container">
    <div class="row">
        <div class="col-lg-12 col-md-12 ">
            <?php
            if (function_exists('dynamic_sidebar')) {
                dynamic_sidebar('all_top_fluid');
            }
            ?>
        </div>
        <div class="<?php echo !el_is_show_sidebar() ? 'col-lg-12' : 'col-lg-8' ?> main-container">
            <!-- 正上方的小组件 -->
            <?php
            if (function_exists('dynamic_sidebar')) {
                dynamic_sidebar('all_top_content');
            }
            ?>
            <div class="post-loop-default mb-3">
                <!-- TOTO: iscard判断-->
                <ul class="post-ul <?php echo iscard('home'); ?>">
                    <?php el_categories_list(); ?>
                </ul>
                <?php el_paging(); ?>
            </div>
            <!-- 正文下面的小组件 -->
            <?php
            if (function_exists('dynamic_sidebar')) {
                dynamic_sidebar('all_bottom_content');
            }
            ?>
        </div>
        <div class="col-lg-4 slidebar-container">
            <?php get_sidebar(); ?>
        </div>
        <div class="col-lg-12 bottom-full-container">
            <?php
            if (function_exists('dynamic_sidebar')) {
                dynamic_sidebar('all_bottom_fluid');
            }
            ?>
        </div>
    </div>
</main>
<?php
get_footer();
?>