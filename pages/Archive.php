<?php
/*Template Name: 文章归档*/

function el_archives_list() {
    if( !$output =  ECache::get('el_archives_post') ){
		$output = '<div id="archives"><p><a id="al_expand_collapse" href="#">全部展开/收缩</a> <em>(注: 点击月份可以展开)</em></p>';
		$args = array(
			'post_type' => array('archives', 'post', 'zsay'),
			'posts_per_page' => -1, //全部 posts
			'ignore_sticky_posts' => 1 //忽略 sticky posts

		);
		$the_query = new WP_Query( $args );
		$posts_rebuild = array();
		$year = $mon = 0;
		while ( $the_query->have_posts() ) : $the_query->the_post();
			$post_year = get_the_time('Y');
			$post_mon = get_the_time('m');
			$post_day = get_the_time('d');
			if ($year != $post_year) $year = $post_year;
			if ($mon != $post_mon) $mon = $post_mon;
			$posts_rebuild[$year][$mon][] = '<li>'. get_the_time('d日: ') .'<a href="'. get_permalink() .'">测试'. get_the_title() .'</a> <em>('. get_post_view_count() .')</em></li>';
		endwhile;
		wp_reset_postdata();

		foreach ($posts_rebuild as $key_y => $y) {
			$y_i = 0; $y_output = '';
			foreach ($y as $key_m => $m) {
				$posts = ''; $i = 0;
				foreach ($m as $p) {
					++$i; ++$y_i;
					$posts .= $p;
				}
				$y_output .= '<li><span class="al_mon">'. $key_m .' 月 <em>( '. $i .' 篇文章 )</em></span><ul class="al_post_list">'; //输出月份
				$y_output .= $posts; //输出 posts
				$y_output .= '</ul></li>';
			}
			$output .= '<h3 class="al_year">'. $key_y .' 年 <em>( '. $y_i .' 篇文章 )</em></h3><ul class="al_mon_list">'; //输出年份
			$output .= $y_output;
			$output .= '</ul>';
		}

		$output .= '</div>';
        ECache::set('el_archives_post', $output, EL_CACHE);
	}
	echo $output;
}
function clear_db_cache_archives_list() {
    ECache::delete('el_archives_post');
	update_option('el_db_cache_archives_list', ''); // 清空 el_archives_list
}
add_action('save_post', 'clear_db_cache_archives_list'); // 新发表文章/修改文章时

get_header();
?>
<main class="el-main container">
    <div class="row">
        <div class="col-lg-12 col-md-12 ">
            <?php
            if (function_exists('dynamic_sidebar')) {
                dynamic_sidebar('all_top_fluid');
                dynamic_sidebar('all_top_fluid');
            }
            ?>
        </div>
        <div class="<?php echo !el_is_show_sidebar() ? 'col-lg-12' : 'col-lg-8' ?> main-container">
            <!-- 正上方的小组件 -->
            <?php
            if (function_exists('dynamic_sidebar')) {
                dynamic_sidebar('all_top_content');
            }
            ?>
            <div class="post-loop-default mb-3">
                <!-- TOTO: iscard判断-->
                <?php el_archives_list(); ?>
            </div>
            <!-- 正文下面的小组件 -->
            <?php
            if (function_exists('dynamic_sidebar')) {
                dynamic_sidebar('all_bottom_content');
            }
            ?>
        </div>
        <div class="col-lg-4 slidebar-container">
            <?php get_sidebar(); ?>
        </div>
        <div class="col-lg-12 bottom-full-container">
            <?php
            if (function_exists('dynamic_sidebar')) {
                dynamic_sidebar('all_bottom_fluid');
            }
            ?>
        </div>
    </div>
</main>
<?php
get_footer();
?>