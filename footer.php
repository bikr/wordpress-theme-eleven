<?php 
/*
 * @Author       : ZengHao
 * @Date         : 2023-12-23 14:14:26
 * @LastEditors  : ZengHao
 * @LastEditTime : 2024-01-28 22:59:36
 * @FilePath     : /footer.php
 * @Description  : 
 * Copyright 2023 www.exehub.com, All Rights Reserved. 
 * 2023-12-23 14:14:26
 */
?>
<footer class="footer">
	<?php if (function_exists('dynamic_sidebar')) {
    dynamic_sidebar('all_footer');
}?>
	<div class="">
		<?php do_action('el_footer_conter');?>
	</div>
</footer>
<?php
wp_footer();
?>

</body>
</html>