<?php
/*
 * @Author       : ZengHao
 * @Date         : 2023-12-23 14:14:26
 * @LastEditors  : ZengHao
 * @LastEditTime : 2024-01-28 20:27:23
 * @FilePath     : /single.php
 * @Description  : 
 * Copyright 2023 www.exehub.com, All Rights Reserved. 
 * 2023-12-23 14:14:26
 */
get_header();
?>
<main class="el-main">
    <section class="position-relative">
        <div class="container" data-sticky-container>
            <div class="row">
                <div class="col-md-12 home_top_fluid">
                    <!-- 顶部全宽度的小组件 -->
                    <div class="el-top">
                        <?php
                        if (function_exists('dynamic_sidebar')) {
                            dynamic_sidebar('all_top_fluid');
                            dynamic_sidebar('single_top_fluid');
                        }
                        ?>
                    </div>
                </div>
                <div class="col-lg-8 home_content">
                    <div class="el-content">
                        <?php
                        if (function_exists('dynamic_sidebar')) {
                            dynamic_sidebar('all_top_content');
                            dynamic_sidebar('single_top_content');
                        }
                        ?>
                        <div class="main-content mb-3">
                            <?php 
                            el_single(); 
                            comments_template('/component/comments.php', true);
                            // comments_template( '', true );
                            ?>
                        </div>
                        <!-- 首页正文下面的小组件 -->
                        <?php
                        if (function_exists('dynamic_sidebar')) {
                            dynamic_sidebar('all_bottom_content');
                            dynamic_sidebar('single_bottom_content');
                        }
                        ?>
                    </div>
                <div class="col-lg-4">
                    <?php get_sidebar(); ?>
                </div>
                <div class="col-md-12 home_bottom_fluid">
                    <!-- 顶部全宽度的小组件 -->
                    <div class="el-top">
                        <?php
                        if (function_exists('dynamic_sidebar')) {
                            dynamic_sidebar('all_bottom_fluid');
                            dynamic_sidebar('single_bottom_fluid');
                        }
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </section>
</main>
<?php
get_footer();
?>