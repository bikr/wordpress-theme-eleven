<?php 
/*
 * @Author       : ZengHao
 * @Date         : 2023-12-23 14:14:26
 * @LastEditors  : ZengHao
 * @LastEditTime : 2024-02-24 18:51:11
 * @FilePath     : /search.php
 * @Description  : 
 * Copyright 2023 www.exehub.com, All Rights Reserved. 
 * 2023-12-23 14:14:26
 */
get_header();
 ?>
 <main class="el-main container">
		 <div class="row">
			 <div class="col-lg-12 col-md-12 ">
				 <?php
				 if (function_exists('dynamic_sidebar')) {
					dynamic_sidebar('all_top_fluid');
					dynamic_sidebar('search_top_fluid');
				 }
				 ?>
			 </div>
			 <div class="<?php echo !el_is_show_sidebar()?'col-lg-12':'col-lg-9' ?> main-container">
				 <!-- 首页正上方的小组件 -->
				 <?php
				 if (function_exists('dynamic_sidebar')) {
					dynamic_sidebar('all_top_content');
					dynamic_sidebar('search_top_content');
				 }
				 ?>
				 <div class="post-loop-default main-post-list">
					 <ul class="post-ul <?php echo iscard('search') ?>">
						 <?php el_posts_list(); ?>
					 </ul>
					 <?php el_paging(); ?>
				 </div>
				 <!-- 首页正文下面的小组件 -->
				 <?php
				 if (function_exists('dynamic_sidebar')) {
					dynamic_sidebar('all_bottom_content');
					dynamic_sidebar('search_bottom_content');
				 }
				 ?>
			 </div>
			 <div class="col-lg-3 slidebar-container">
				 <?php get_sidebar(); ?>
			 </div>
			 <div class="col-lg-12 bottom-full-container">
				 <?php
				 if (function_exists('dynamic_sidebar')) {
					dynamic_sidebar('all_bottom_fluid');
					dynamic_sidebar('search_bottom_fluid');
				 }
				 ?>
			 </div>
		 </div>
 </main>
 <?php
 get_footer();
 ?>
