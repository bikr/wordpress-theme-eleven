<?php
/*
 * @Author       : ZengHao
 * @Date         : 2024-01-14 00:51:24
 * @LastEditors  : ZengHao
 * @LastEditTime : 2024-01-14 00:58:29
 * @FilePath     : /expand/yiyan/yiyan.php
 * @Description  : 
 * Copyright 2024 www.exehub.com, All Rights Reserved. 
 * 2024-01-14 00:51:24
 */

//  TODO:增加是否是本域名调用

//获取句子文件的绝对路径
$path = dirname(__FILE__, 4) . "/yiyan.txt";
if (!file_exists($path)) {
    $path = dirname(__FILE__) . "/yiyan.txt";
}
$file = file($path);

if (count($file) < 2) {
    exit('一言文件：yiyan.txt，内容为空/&/');
}

//随机读取一行
$arr     = mt_rand(0, count($file) - 2);
$arr     = ($arr % 2 === 0) ? $arr + 1 : $arr;
$content = trim($file[$arr]) . '/&/' . trim($file[$arr + 1]);
//编码判断，用于输出相应的响应头部编码
if (isset($_GET['charset']) && !empty($_GET['charset'])) {
    $charset = $_GET['charset'];
    if (strcasecmp($charset, "gbk") == 0) {
        $content = mb_convert_encoding($content, 'gbk', 'utf-8');
    }
} else {
    $charset = 'utf-8';
}
header("Content-Type: text/html; charset=$charset");
echo $content;
