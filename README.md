

![screenshot](https://gitlab.com/blikr/blog_img/-/raw/main/pictures/2024/02/202402272055039.png)

<div align="center">
    <h1>WordPress Theme - Eleven</h1>
    <p>一款基于WordPress开发的高颜值的自适应主题，支持白天与黑夜模式。</p>
      <a target="_blank" href="https://gitee.com/bikr/wordpress-theme-eleven/releases/latest">
        <img src="https://img.shields.io/badge/release-V1.0.0-blue?style=flat&logo=git
" alt="Release-Version">
      </a>
    <a target="_blank" href="https://gitee.com/bikr/wordpress-theme-eleven">
        <img src="https://img.shields.io/badge/WordPress-V5.0%2B-LightSlateGray?style=flat&logo=WordPress&labelColor=%23778899
" alt="WordPress-Version">
      </a>
    <a target="_blank" href="https://gitee.com/bikr/wordpress-theme-eleven">
        <img src="https://img.shields.io/badge/PHP-V7.4+-666699.svg?logo=php" alt="PHP-Version">
      </a>
    <br>
    <a target="_blank" href="https://afdian.net/a/eleven-theme">
        <img src="https://img.shields.io/badge/赞赏-开发不易-CC3333.svg?logo=Buy-Me-A-Coffee" alt="赞赏支持">
      </a>
    <a target="_blank" href="https://www.exehub.net/sponsor">
        <img src="https://img.shields.io/badge/捐赠-支付宝-00a2ef.svg?logo=AliPay" alt="支付宝捐赠">
      </a>
    <a target="_blank" href="https://www.exehub.net/sponsor">
        <img src="https://img.shields.io/badge/捐赠-微信-68b600.svg?logo=WeChat" alt="微信捐赠">
      </a>
    <br>
</div>


## 🪢 预览地址




## 🎉 主题安装

1. 下载 **[最新发行版本](https://gitee.com/bikr/wordpress-theme-eleven/releases/latest )** 的 .zip 格式安装包；
2. 登录 WordPress 管理员后台，进入 `外观` > `主题` 页面，然后点击 `添加` 按钮；
3. 进入添加主题页面之后，再点击 `上传主题` 按钮；
4. 点击 `选择文件`, 找到下载的 .zip 格式安装包，然后点击 `现在安装` 按钮；
5. 进入 `外观` > `主题` 页面，找到 `Eleven` 主题，然后点击 `启用` 即可。

> **提示：为了防止主题不兼容，请在安装主题前进行数据备份，防止数据字段重复覆盖等情况发生。**

> **重要：请不要直接克隆或直接下载仓库进行使用，请到[发行版](https://gitee.com/bikr/wordpress-theme-eleven/releases/latest)中进行下载使用**

## 🐬 环境要求

- WordPress `>=5.0`
- PHP `>=7.4`
- MySQL `>=5.6`

## 🍄 后台截图

![eleven-admin-setting-1](https://gitlab.com/blikr/blog_img/-/raw/main/pictures/2024/02/202402222058275.png)

![eleven-admin-setting-2](https://gitlab.com/blikr/blog_img/-/raw/main/pictures/2024/02/202402222110871.png)

![eleven-admin-setting-3](https://gitlab.com/blikr/blog_img/-/raw/main/pictures/2024/02/202402222135824.png)

![image-20240227205734678](https://gitlab.com/blikr/blog_img/-/raw/main/pictures/2024/02/202402272057268.png)

### 后台功能简介

- LOGO图像 设置
- SEO 设置
- 显示布局 设置
- 悬浮按钮 设置
- Email邮件 设置
- 自定义代码 设置
- 导航栏 设置
- 底部页脚 设置
- 文章缩略图 设置
- 文章列表 设置
- 文章页面 设置
- 后台展示优化 设置
- 后台功能优化 设置
- 系统功能 设置
- 主题备份 设置
- 主题更新 设置
- 文档教程

## ☃️ 前台展示

![image-20240227205653623](https://gitlab.com/blikr/blog_img/-/raw/main/pictures/2024/02/202402272056354.png)

## 🐱 主题特性

### 当前功能

- 支持白天与暗黑模式
- 全局缓存，优化查询
- 内置WP优化策略
- 自动缩略图
- 全局顶部滚动公告
- 自定义SMTP支持
- 网页压缩、主题静态资源压缩
- 后台防恶意登录
- 内置出色的SEO功能
- 文章多种列表显示
- 文章点赞、打赏、分享、收藏
- 丰富的广告位
- 丰富的小工具
- 更便捷的短代码插入方式
- 自动百度链接提交
- 智能摘要结构化数据
- 自动接入网站统计分析
- 外链重定向
- 文章图片自动添加alt
- 图片懒加载
- 静态资源按需引入
- 自定义代码高亮
- 主题设置备份与导入
- 主题在线更新
- 后台主界面优化
- 部分AJAX刷新
- 更多特性更新请查阅版本发布说明：releases
- 更多功能等你的提议

### 后续功能

-  全局无刷新加载
-  前台用户中心
- 支持QQ / Github / Gitee / 微博登录
-  图形及极验验证码支持
-  一键全站变灰
- 众多页面模板（读者墙 / 归档 / 书籍推荐/ 站点导航 / 标签 / 站点地图等）
- 众多短代码支持（下载 / 评论后可见 / 登录后可见 / 登录且验证邮箱可见 / 多种提示框 / Github卡片等）
- 支持Dplayer播放器
-  侧边栏粘性滚动
-  自定义主色调
-  LOGO扫光动画
-  文章多级目录生成
-  更多功能请帮忙提议~

## 🦁 文档

- 主题使用文档：立即使用
- 建议或BUG反馈：立即进入
- QQ/微信交流群：点我加入 （此群皆为大家提供交流使用的地方，有BUG请直接提交ISSUE）
- **若您有任何建议或BUG发现，并且您也有解决或实现的思路，欢迎直接提交PR！**

## 🐰 支持

- 捐赠我们以支持更好的发展：点我进入

##  🦄 鸣谢

- [VS Code](https://code.visualstudio.com/)
- [WordPress](https://wordpress.org/)
- [PHP](https://www.php.net/)

## 🪭 开源协议

- [GPL V3.0](https://gitee.com/bikr/wordpress-theme-eleven/blob/master/LICENSE)
- 请遵守开源协议，保留主题底部的署名