<?php if (get_comments_number() == 0 && !comments_open() || _opz('comment_closed')) : echo '';
else : ?>
    <?php add_filter('pk_rb_float_actions', function ($content) {
        return $content . '<div data-to-area="#comments" class=""><i class="fa-regular fa-comments puock-text"></i></div>';
    }) ?>
    <div class="el-widget mb-3" id="comments">
        <h2 class="widget-title">
            <?php _e('评论', 'el_language') ?>（<?php comments_number() ?>）
            <span class="widget-border"></span>
        </h2>
        <!-- <div class="comment-title mb-3">
            <span class=""><i data-feather="message-square"></i> </span>
        </div> -->

        <?php if (comments_open()) : ?>
            <?php if (_opz('comment_registration', '0') == '1' && !is_user_logged_in()) : //登录后才可以评论 
            ?>
                <div class="mt20 clearfix" id="comment-form-box">
                    <form class="mt10" id="comment-form" method="post">
                        <div class="form-group">
                            <textarea placeholder="<?php _e('您必须要登录之后才可以进行评论', 'el_language') ?>" disabled id="comment" name="comment" class="form-control form-control-sm t-sm mb-3" rows="4"></textarea>
                        </div>
                    </form>
                    <?php // if (pk_oauth_platform_count() > 0): 
                    ?>
                    <?php if (_opz('need_login', true)) : ?>
                        <div>
                            <button class="btn btn-primary btn-sm pk-modal-toggle" type="button" data-id="front-login" data-once-load="true" title="快捷登录" data-url="<?php echo pk_ajax_url('pk_font_login_page', ['redirect' => get_permalink()]) ?>">
                                <i class="fa fa-right-to-bracket"></i>&nbsp;<?php _e('快捷登录', 'el_language') ?>
                            </button>
                        </div>
                    <?php endif; ?>
                </div>
            <?php else : ?>
                <div class="mt20 clearfix" id="comment-form-box">
                    <form class="mt10" id="comment-form" method="post" action="<?php echo admin_url() . 'admin-ajax.php?action=comment_ajax' ?>">
                        <div class="form-group">
                            <textarea placeholder="<?php _e('世界这么大发表一下你的看法~', 'el_language') ?>" id="comment" name="comment" class="form-control form-control-sm t-sm mb-3" rows="4"></textarea>
                        </div>
                        <div class="row row-cols-1 comment-info">
                            <!-- 启用评论验证 -->
                            <?php $commentInfoCol = _opz('vd_comment', true) ? 3 : 4; ?>
                            <?php if (!is_user_logged_in()) : ?>
                                <input type="text" value="0" hidden name="comment-logged" id="comment-logged">
                                <div class="col-12 col-sm-<?php echo $commentInfoCol ?>"><input type="text" id="comment_author" name="author" class="form-control form-control-sm t-sm" placeholder="<?php _e('昵称（必填）', 'el_language') ?>">
                                </div>
                                <div class="col-12 col-sm-<?php echo $commentInfoCol ?>"><input type="email" id="comment_email" name="email" class="form-control form-control-sm t-sm" placeholder="<?php _e('邮箱（必填）', 'el_language') ?>">
                                </div>
                                <div class="col-12 col-sm-<?php echo $commentInfoCol ?>"><input type="text" id="comment_url" name="url" class="form-control form-control-sm t-sm" placeholder="<?php _e('网站', 'el_language') ?>">
                                </div>

                            <?php endif; ?>
                            <?php if (_opz('vd_comment', false) && _opz('vd_type', 'img') === 'img') : ?>
                                <div class="col-12 col-sm-3">
                                    <div class="row flex-row justify-content-end">
                                        <div class="col-8 col-sm-7 text-end pl15">
                                            <input type="text" value="" placeholder="验证码" maxlength="4" class="form-control form-control-sm t-sm" name="comment-vd" autocomplete="off" id="comment-vd">
                                        </div>
                                        <div class="col-4 col-sm-5 pr15" id="comment-captcha-box">
                                            <img class="comment-captcha captcha" src="<?php echo pk_captcha_url('comment', 100, 28) ?>" alt="验证码">
                                        </div>
                                    </div>
                                </div>
                            <?php endif; ?>
                        </div>

                        <input type="text" hidden name="comment_post_ID" value="<?php echo $post->ID ?>">
                        <input type="text" hidden id="comment_parent" name="comment_parent" value="">
                        <div class="p-flex-sbc mb-3">
                            <div>
                                <?php if (is_user_logged_in()) : $user = wp_get_current_user(); ?>
                                    <div class="puock-text t-sm">
                                        <input type="text" value="1" hidden name="comment-logged" id="comment-logged">
                                        <span><strong><?php echo $user->data->display_name ?></strong>，<a data-no-instant class="ta3 a-link" href="<?php echo wp_logout_url(get_the_permalink()) ?>"><?php _e('登出', 'el_language') ?></a></span>
                                    </div>
                                <?php endif; ?>
                                <?php if (!is_user_logged_in() && pk_oauth_platform_count() > 0) : ?>
                                    <div class="d-inline-block">
                                        <button class="btn btn-primary btn-sm pk-modal-toggle" type="button" data-once-load="true" data-id="front-login" title="快捷登录" data-url="<?php echo pk_ajax_url('pk_font_login_page', ['redirect' => get_permalink()]) ?>">
                                            <i class="fa fa-right-to-bracket"></i>&nbsp;<?php _e('快捷登录', 'el_language') ?>
                                        </button>
                                    </div>
                                <?php endif; ?>
                            </div>
                            <div>
                                <button id="comment-cancel" type="button" class="btn btn-outline-dark d-none btn-sm"><?php _e('取消', 'el_language') ?></button>
                                <button id="comment-smiley" class="btn btn-outline-secondary btn-sm pk-modal-toggle" type="button" title="表情" data-once-load="true" data-url="<?php echo pk_ajax_url('pk_ajax_dialog_smiley') ?>">
                                    <i data-feather="smile"></i></button>
                                <button id="comment-submit" type="submit" class="btn btn-primary btn-sm"><i data-feather="send"></i>&nbsp;<?php _e('发布评论', 'el_language') ?>
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            <?php endif; ?>
        <?php endif; ?>
        <?php if (_opz('comment_ajax', true)) : ?>
            <div id="comment-ajax-load" class="text-center mt20 d-none">
                <?php echo pk_skeleton('comment', 3) ?>
            </div>
        <?php endif; ?>
        <div id="post-comments">
            <?php
            if (get_comments_number() > 0) :
                wp_list_comments(array(
                    'type' => 'comment',
                    'callback' => 'pk_comment_callback',
                ));
                echo '</div>';
            endif;
            ?>

            <div class="mt-1 p-flex-s-right" <?php echo _opz('comment_ajax') ? 'data-no-instant' : '' ?>>
                <ul class="pagination comment-ajax-load">
                    <?php
                    paginate_comments_links(array(
                        'prev_text' => '&laquo;',
                        'next_text' => '&raquo;',
                        'format' => '<li>%1</li>'
                    ));
                    ?>
                </ul>
            </div>

        </div>
    </div>
<?php endif; ?>