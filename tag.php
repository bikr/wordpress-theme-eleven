<?php
/*
 * @Author       : ZengHao
 * @Date         : 2024-01-09 23:12:42
 * @LastEditors  : ZengHao
 * @LastEditTime : 2024-02-24 18:50:59
 * @FilePath     : /tag.php
 * @Description  : 
 * Copyright 2024 www.exehub.com, All Rights Reserved. 
 * 2024-01-09 23:12:42
 */

 get_header();
 ?>
 <main class="el-main container">
		 <div class="row">
			 <div class="col-lg-12 col-md-12 ">
				 <?php
				 if (function_exists('dynamic_sidebar')) {
					dynamic_sidebar('all_top_fluid');
					dynamic_sidebar('tag_top_fluid');
				 }
				 ?>
			 </div>
			 <div class="<?php echo !el_is_show_sidebar()?'col-lg-12':'col-lg-8' ?> main-container">
				 <!-- 首页正上方的小组件 -->
				 <?php
				 if (function_exists('dynamic_sidebar')) {
					dynamic_sidebar('all_top_content');
					dynamic_sidebar('tag_top_content');
				 }
				 ?>
				 <div class="post-loop-default main-post-list">
					 <ul class="post-ul <?php echo iscard('tag') ?>">
						 <?php el_posts_list(); ?>
					 </ul>
					 <?php el_paging(); ?>
				 </div>
				 <!-- 首页正文下面的小组件 -->
				 <?php
				 if (function_exists('dynamic_sidebar')) {
					dynamic_sidebar('all_bottom_content');
					dynamic_sidebar('tag_bottom_content');
				 }
				 ?>
			 </div>
			 <div class="col-lg-4 slidebar-container">
				 <?php get_sidebar(); ?>
			 </div>
			 <div class="col-lg-12 bottom-full-container">
				 <?php
				 if (function_exists('dynamic_sidebar')) {
					dynamic_sidebar('all_bottom_fluid');
					dynamic_sidebar('tag_bottom_fluid');
				 }
				 ?>
			 </div>
		 </div>
 </main>
 <?php
 get_footer();
 ?>