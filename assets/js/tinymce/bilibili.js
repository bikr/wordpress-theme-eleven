/*
 * @Author       : ZengHao
 * @Date         : 2024-01-23 23:23:26
 * @LastEditors  : ZengHao
 * @LastEditTime : 2024-01-27 17:58:31
 * @FilePath     : /assets/js/bilibili.js
 * @Description  :
 * Copyright 2024 www.exehub.com, All Rights Reserved.
 * 2024-01-23 23:23:26
 */

(function ($) {
    tinymce.PluginManager.add("ebilibili", function (editor, url) {
        var is_edit = false;
        function on_submit(e) {
            var viedehm = $.trim(tinymce.html.Entities.encodeAllRaw(e.data.viedehm.replace(/\r\n/gmi, '\n')));
            var lx = e.data.videolx;
            var h = '600', w = '100%';
            if (viedehm) {
                if (lx == 'av') {
                    var iframe_url = 'https://player.bilibili.com/player.html?aid=' + viedehm + '&amp;high_quality=1';
                    editor.insertContent('<iframe src="' + iframe_url + '" width="'+ w+ '" height="'+ h +'" frameborder="no" scrolling="no" sandbox="allow-top-navigation allow-same-origin allow-forms allow-scripts" allowfullscreen="allowfullscreen"></iframe>');
                } else {
                    var iframe_url = 'https://player.bilibili.com/player.html?bvid=' + viedehm + '&amp;high_quality=1';
                    editor.insertContent('<iframe src="' + iframe_url + '" width="'+ w+ '" height="'+ h +'" frameborder="no" scrolling="no" sandbox="allow-top-navigation allow-same-origin allow-forms allow-scripts" allowfullscreen="allowfullscreen"></iframe>');
                }
            }
        }
        $('.add-shortcode[data-key="bilibili"]').click(function () {
            var w = Math.min(window.innerWidth);
            var h = Math.min(window.innerHeight);
            var vals = {
                videolx: 'av',
                viedehm: '',
                viedeh:600,
                viedew:'100%'
            }
            if (w > 800) {
                w = w * 0.5
            } else if (w > 640 && w < 801) {
                w = w * 0.7
            } else if (w < 641) {
                w = w - 20
            }

            var window_title = 'B站视频';
            editor.windowManager.open({
                title: window_title,
                width: w,
                height: h * 0.6,
                body: [{
                    type: 'listbox',
                    name: 'videolx',
                    label: '选择类型',
                    value: vals.videolx,
                    values: [{
                        text: 'AV号',
                        value: 'av'
                    }, {
                        text: 'BV号',
                        value: 'bv'
                    }],
                }, {
                    type: 'textbox',
                    name: 'viedehm',
                    label: '号码：',
                    value: vals.viedehm,
                    multiline: true,
                },{
                    type: 'textbox',
                    name: 'viedeh',
                    label: '高度：',
                    value: vals.viedeh,
                    multiline: true,
                },{
                    type: 'textbox',
                    name: 'viedew',
                    label: '宽度：',
                    value: vals.viedew,
                    multiline: true,
                }],
                onsubmit: on_submit
            });
        });
    });
})(jQuery);
