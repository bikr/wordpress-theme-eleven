/*
 * @Author       : ZengHao
 * @Date         : 2024-01-23 23:23:26
 * @LastEditors  : ZengHao
 * @LastEditTime : 2024-01-28 00:00:05
 * @FilePath     : /assets/js/precode.js
 * @Description  :
 * Copyright 2024 www.exehub.com, All Rights Reserved.
 * 2024-01-23 23:23:26
 */

(function ($) {
  tinymce.PluginManager.add("elcode", function (editor, url) {
    console.log(editor);
    var is_edit = $('#tinymce').attr('contenteditable')?$('#tinymce').attr('contenteditable'):false;
    console.log('is_editL='+is_edit);
    function on_submit(e) {
        var codenr = $.trim(tinymce.html.Entities.encodeAllRaw(e.data.codenr.replace(/\r\n/gmi, '\n'))),
            tm = e.data.theme == 'qj' ? '' : '&nbsp;data-enlighter-theme="' + e.data.theme + '"',
            yy = e.data.codeyy == 'generic' ? '' : '&nbsp;data-enlighter-language="' + e.data.codeyy + '"';

        if (codenr) {
            editor.insertContent('<pre contenteditable="'+is_edit+'" class="enlighter-pre"><code class="gl"' + yy + tm + '>' + codenr + '</code></pre>' + (is_edit ? '' : '<p></p>'));
        }
    }
    $('.add-shortcode[data-key="code"]').click(function () {
        var w = Math.min(window.innerWidth);
        var h = Math.min(window.innerHeight);
        var vals = {
            codeyy: 'generic',
            theme: 'qj',
            codenr: '',
        }
        if (w > 800) {
            w = w * 0.5
        } else if (w > 640 && w < 801) {
            w = w * 0.7
        } else if (w < 641) {
            w = w - 20
        }

        var window_title ='高亮代码';
        editor.windowManager.open({
            title: window_title,
            width: w,
            height: h * 0.6,
            body: [{
                type: 'listbox',
                name: 'codeyy',
                label: '选择语言',
                value: vals.codeyy,
                values: [{
                    text: 'yaml',
                    value: 'yaml'
                }, {
                    text: 'xml/html',
                    value: 'xml'
                }, {
                    text: 'visualbasic',
                    value: 'visualbasic'
                }, {
                    text: 'vhdl',
                    value: 'vhdl'
                }, {
                    text: 'typescript',
                    value: 'typescript'
                }, {
                    text: 'swift',
                    value: 'swift'
                }, {
                    text: 'squirrel',
                    value: 'squirrel'
                }, {
                    text: 'sql',
                    value: 'sql'
                }, {
                    text: 'shell',
                    value: 'shell'
                }, {
                    text: 'scss/sass',
                    value: 'scss'
                }, {
                    text: 'rust',
                    value: 'rust'
                }, {
                    text: 'ruby',
                    value: 'ruby'
                }, {
                    text: 'raw',
                    value: 'raw'
                }, {
                    text: 'python',
                    value: 'python'
                }, {
                    text: 'prolog',
                    value: 'prolog'
                }, {
                    text: 'powershell',
                    value: 'powershell'
                }, {
                    text: 'php',
                    value: 'php'
                }, {
                    text: 'nsis',
                    value: 'nsis'
                }, {
                    text: 'matlab',
                    value: 'matlab'
                }, {
                    text: 'markdown',
                    value: 'markdown'
                }, {
                    text: 'lua',
                    value: 'lua'
                }, {
                    text: 'less',
                    value: 'less'
                }, {
                    text: 'kotlin',
                    value: 'kotlin'
                }, {
                    text: 'json',
                    value: 'json'
                }, {
                    text: 'javascript',
                    value: 'javascript'
                }, {
                    text: 'java',
                    value: 'java'
                }, {
                    text: 'ini/conf',
                    value: 'ini'
                }, {
                    text: 'groovy',
                    value: 'groovy'
                }, {
                    text: 'go/golang',
                    value: 'go'
                }, {
                    text: 'docker',
                    value: 'dockerfile'
                }, {
                    text: 'diff',
                    value: 'diff'
                }, {
                    text: 'cordpro',
                    value: 'cordpro'
                }, {
                    text: 'cython',
                    value: 'cython'
                }, {
                    text: 'css',
                    value: 'css'
                }, {
                    text: 'csharp',
                    value: 'csharp'
                }, {
                    text: 'Cpp/C++/C',
                    value: 'cpp'
                }, {
                    text: 'avrassembly',
                    value: 'avrassembly'
                }, {
                    text: 'assembly',
                    value: 'assembly'
                }, {
                    text: '通用高亮',
                    value: 'generic'
                }],
            }, {
                type: "listbox",
                name: "theme",
                label: "主题",
                value: vals.theme,
                values: [{
                    text: 'enlighter',
                    value: 'enlighter'
                }, {
                    text: 'classic',
                    value: 'classic'
                }, {
                    text: 'beyond',
                    value: 'beyond'
                }, {
                    text: 'mowtwo',
                    value: 'mowtwo'
                }, {
                    text: 'eclipse',
                    value: 'eclipse'
                }, {
                    text: 'droide',
                    value: 'droide'
                }, {
                    text: 'minimal',
                    value: 'minimal'
                }, {
                    text: 'atomic',
                    value: 'atomic'
                }, {
                    text: 'dracula',
                    value: 'dracula'
                }, {
                    text: 'bootstrap4',
                    value: 'bootstrap4'
                }, {
                    text: 'rowhammer',
                    value: 'rowhammer'
                }, {
                    text: 'godzilla',
                    value: 'godzilla'
                }, {
                    text: '跟随全局设置',
                    value: 'qj'
                }],
            }, {
                type: 'textbox',
                name: 'codenr',
                label: '代码：',
                value: vals.codenr,
                multiline: true,
                minHeight: h * 0.6 - 115
            }],
            onsubmit: on_submit
        });
    });
  });
})(jQuery);
