/*
 * @Author       : ZengHao
 * @Date         : 2024-01-28 12:55:06
 * @LastEditors  : ZengHao
 * @LastEditTime : 2024-01-28 13:25:21
 * @FilePath     : /assets/js/tinymce/edownload.js
 * @Description  : 
 * Copyright 2024 www.exehub.com, All Rights Reserved. 
 * 2024-01-28 12:55:06
 */

(function ($) {
    tinymce.PluginManager.add("edownload", function (editor, url) {
        var ajax_url = $('.add-shortcode[data-key="download"]').attr('data-action');
        var is_edit = false;
        function on_submit(e) {
            console.log(e);
            var html = '<div class="el-down-box">';
            html += '<div class="mb-2"><i class="fa fa-file-zipper"></i>&nbsp;<span>文件名称：'+e.data.filename+'</span></div>';
            html += '<div class="mb-2"><i class="fa fa-download"></i>&nbsp;<span>文件大小：'+e.data.filesize+'</span></div>';
            html += '<div class="mb-2"><i class="fa fa-link"></i><span>下载地址：'+e.data.fileurl+'</span></div>';
            html += '<div><i class="fa-regular fa-bell"></i>&nbsp;<span>下载声明：'+e.data.filetips+'</span></div>';
            html += '</div>';
            editor.insertContent(html);
        }
        $('.add-shortcode[data-key="download"]').click(function (e) {
            var w = Math.min(window.innerWidth);
            var h = Math.min(window.innerHeight);
            if (w > 800) {
                w = w * 0.5
            } else if (w > 640 && w < 801) {
                w = w * 0.7
            } else if (w < 641) {
                w = w - 20
            }
            var down_tips = '';
            $.get(
                ajax_url,
                {
                    action: "get_down_tips",
                },
                (res) => {
                    console.log(res);
                    down_tips = res.data;
                }
            );
            var window_title = '文件下载';
            editor.windowManager.open({
                title: window_title,
                width: w,
                height: h * 0.6,
                body: [{
                    type: 'textbox',
                    name: 'filename',
                    label: '文件名称',
                    value: '',
                },{
                    type: 'textbox',
                    name: 'filesize',
                    label: '文件大小',
                    value: '',
                },{
                    type: 'textbox',
                    name: 'fileurl',
                    label: '下载地址',
                    value: '',
                },{
                    type: 'textbox',
                    name: 'filetips',
                    label: '下载声明',
                    value: down_tips,
                }],
                onsubmit: on_submit
            });
        });
    });
})(jQuery);
