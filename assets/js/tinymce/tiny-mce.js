/*
 * @Author       : ZengHao
 * @Date         : 2024-01-24 14:40:22
 * @LastEditors  : ZengHao
 * @LastEditTime : 2024-01-28 12:46:12
 * @FilePath     : /assets/js/tinymce/tiny-mce.js
 * @Description  :
 * Copyright 2024 www.exehub.net, All Rights Reserved.
 * 2024-01-24 14:40:22
 */
(function ($) {
  $("head").append(`
    <style>
    .el-media-wrap {
        background: #fff;
        border: 1px solid #ccc;
        box-shadow: 2px 2px 3px rgba(0, 0, 0, 0.24);
        padding: 10px;
        position: absolute;
        top: 60px;
        width: 380px;
      }
      .cur.el-media-wrap {
        display: block !important;
      }
      #insert-smiley-wrap img {
        width: 20px;
        cursor: pointer;
      }
      #insert-smiley-wrap img:hover {
        opacity: 0.7;
        transition: all 0.3s;
      }
    </style>
    `);
  var w = Math.min(window.innerWidth);
  var h = Math.min(window.innerHeight);
  var vals = {
    codeyy: "generic",
    theme: "qj",
    codenr: "",
  };
  if (w > 800) {
    w = w * 0.5;
  } else if (w > 640 && w < 801) {
    w = w * 0.7;
  } else if (w < 641) {
    w = w - 20;
  }

  $(document).on("click", "#insert-shortcode-button", function () {
    const shortCodeWrapEl = $("#insert-shortcode-wrap");
    if (shortCodeWrapEl.hasClass("cur")) {
      shortCodeWrapEl.removeClass("cur");
    } else {
      shortCodeWrapEl.addClass("cur");
    }
  });

//   $(document).on("click", ".add-shortcode", function () {
//     const _this = $(this)
//     const key = _this.attr("data-key");
//     if (key == 'code') {
//         return;
//     }
//     const attrStr = _this.attr("data-attr")
//     const content = _this.attr("data-content")
//     let out = `[${key}`
//     if (attrStr) {
//         const attr = JSON.parse(attrStr)
//         for (const attrKey in attr) {
//             out += ` ${attrKey}='${attr[attrKey]}'`
//         }
//     }
//     out += `]${content}[/${key}]`
//     putTextToEditor(out)
//     $('#insert-shortcode-wrap').removeClass('cur')
//     function putTextToEditor(content) {
//         wp.media.editor.insert(content)
//     }
// });
})(jQuery);
