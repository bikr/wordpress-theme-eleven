/*
 * @Author       : ZengHao
 * @Date         : 2024-01-28 00:09:08
 * @LastEditors  : ZengHao
 * @LastEditTime : 2024-01-28 12:51:18
 * @FilePath     : /assets/js/tinymce/epost.js
 * @Description  : 
 * Copyright 2024 www.exehub.com, All Rights Reserved. 
 * 2024-01-28 00:09:08
 */

(function ($) {
    tinymce.PluginManager.add("epost", function (editor, url) {
        var ajax_url = $('.add-shortcode[data-key="post"]').attr('data-action');
        var is_edit =false;
        function on_submit(e) {
            console.log(e);
            var pid = e.data.pid;
            console.log(pid);
            $.get(
                ajax_url,
                {
                    action: "set_shortcode_post",
                    'pid': pid
                },
                (res) => {
                    var dat = JSON.parse(res.data);
                    console.log(dat);
                    var html = '';
                    html += '<div class="el-post-card" contenteditable="'+is_edit+'">';
                    html += '<div class="post-con">';
                    html += '<h3><a href="' + dat.link + '" rel="post" target="_blank">' + dat.title + '</a></h3>';
                    html += '</div>';
                    html += '<div class="post-meta">';
                    html += '<span><i data-feather="clock"></i>' + dat.time_ago + '</span>';
                    html += '<span><i data-feather="eye"></i>' + dat.views + '</span>';
                    html += '</div>';
                    html += '</div>'+ (is_edit ? '' : '<p></p>');
                    editor.insertContent(html);
                }
            );

        }
        $('.add-shortcode[data-key="post"]').click(function (e) {
            var w = Math.min(window.innerWidth);
            var h = Math.min(window.innerHeight);
            if (w > 800) {
                w = w * 0.5
            } else if (w > 640 && w < 801) {
                w = w * 0.7
            } else if (w < 641) {
                w = w - 20
            }

            var window_title = '文章卡片';
            editor.windowManager.open({
                title: window_title,
                width: w,
                height: h * 0.6,
                body: [{
                    type: 'textbox',
                    name: 'pid',
                    label: '文章ID',
                    value: '',
                },],
                onsubmit: on_submit
            });
        });
    });
})(jQuery);
