/*
 * @Author       : ZengHao
 * @Date         : 2024-01-27 19:11:28
 * @LastEditors  : ZengHao
 * @LastEditTime : 2024-01-28 12:52:20
 * @FilePath     : /assets/js/tinymce/elcat.js
 * @Description  : 
 * Copyright 2024 www.exehub.com, All Rights Reserved. 
 * 2024-01-27 19:11:28
 */

(function ($) {
    tinymce.PluginManager.add("elcat", function (editor, url) {
        var ajax_url = $('.add-shortcode[data-key="cat"]').attr('data-action');
        var is_edit = false;
        function on_submit(e) {
            console.log(e);
            var cid = e.data.catid;
            console.log(cid);
            $.get(
                ajax_url,
                {
                    action: "set_shortcode_cat",
                    'cat_id':cid
                },
                (res) => {
                    var dat = JSON.parse(res.data);
                    console.log();
                    var html = '';
                    html += '<div class="el-post-card" contenteditable="'+is_edit+'">';
                    html += '<div class="post-con">';
                    html += '<h3><a href="' + dat.link + '" rel="post" target="_blank">' + dat.title + '</a></h3>';
                    html += '</div>';
                    html += '<div class="post-meta">';
                    html += '<span><i data-feather="eye"></i>' + dat.views + '</span>';
                    html += '</div>'+ (is_edit ? '' : '<p></p>');
                    console.log(html);
                    editor.insertContent(html);
                }
            );

        }
        $('.add-shortcode[data-key="cat"]').click(function (e) {
            var w = Math.min(window.innerWidth);
            var h = Math.min(window.innerHeight);
            if (w > 800) {
                w = w * 0.5
            } else if (w > 640 && w < 801) {
                w = w * 0.7
            } else if (w < 641) {
                w = w - 20
            }
            var flvas = [];
            console.log(ajax_url);
            $.get(
                ajax_url,
                {
                    action: "admin_get_cat_id",
                },
                (res) => {
                    // var data = JSON.parse(res.data);
                    console.log(res);
                    res.data.forEach(re => {
                        flvas.push({
                            text: re.cat_name,
                            value: re.cat_ID
                        });
                    });
                }
            );
            var window_title = '分类卡片';
            editor.windowManager.open({
                title: window_title,
                width: w,
                height: h * 0.6,
                body: [{
                    type: 'listbox',
                    name: 'catid',
                    label: '选择分类',
                    values: flvas,
                },],
                onsubmit: on_submit
            });
        });
    });
})(jQuery);
