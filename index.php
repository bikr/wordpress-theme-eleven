<?php
/*
 * @Author       : ZengHao
 * @Date         : 2023-12-23 14:14:26
 * @LastEditors  : ZengHao
 * @LastEditTime : 2024-02-25 23:59:39
 * @FilePath     : /index.php
 * @Description  : 
 * Copyright 2023 www.exehub.com, All Rights Reserved. 
 * 2023-12-23 14:14:26
 */
/*
 *                        _oo0oo_
 *                       o8888888o
 *                       88" . "88
 *                       (| -_- |)
 *                       0\  =  /0
 *                     ___/`---'\___
 *                   .' \\|     |// '.
 *                  / \\|||  :  |||// \
 *                 / _||||| -:- |||||- \
 *                |   | \\\  - /// |   |
 *                | \_|  ''\---/''  |_/ |
 *                \  .-\__  '-'  ___/-. /
 *              ___'. .'  /--.--\  `. .'___
 *           ."" '<  `.___\_<|>_/___.' >' "".
 *          | | :  `- \`.;`\ _ /`;.`/ - ` : | |
 *          \  \ `_.   \_ __\ /__ _/   .-` /  /
 *      =====`-.____`.___ \_____/___.-`___.-'=====
 *                        `=---='
 * 
 * 
 *      ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * 
 *            佛祖保佑     永不宕机     永无BUG
 */

if (!defined('ABSPATH'))
    exit;

get_header();
?>
<main class="el-main container">
        <div class="row">
            <div class="col-lg-12 col-md-12 ">
                <?php
                if (function_exists('dynamic_sidebar')) {
                    dynamic_sidebar('all_top_fluid');
                    dynamic_sidebar('home_top_fluid');
                }
                ?>
            </div>
            <div class="<?php echo !el_is_show_sidebar()?'col-lg-12':'col-lg-8' ?> main-container">
                <!-- 首页正上方的小组件 -->
                <?php
                if (function_exists('dynamic_sidebar')) {
                    dynamic_sidebar('all_top_content');
                    dynamic_sidebar('home_top_content');
                }
                ?>
                <div class="post-loop-default mb-3">
                <?php  //el_ajax_post_tabs() ?>
                    <!-- <div class="axaj-opt">
                    </div> -->
                    <!-- TOTO: iscard判断-->
                    <ul class="post-ul <?php echo iscard('home');?>">
                        <?php el_posts_list(); ?>
                    </ul>
                    <?php   el_paging(); ?>
                </div>
                <!-- 首页正文下面的小组件 -->
                <?php
                if (function_exists('dynamic_sidebar')) {
                    dynamic_sidebar('all_bottom_content');
                    dynamic_sidebar('home_bottom_content');
                }
                ?>
            </div>
            <div class="col-lg-4 slidebar-container">
                <?php get_sidebar(); ?>
            </div>
            <div class="col-lg-12 bottom-full-container">
                <?php
                if (function_exists('dynamic_sidebar')) {
                    dynamic_sidebar('all_bottom_fluid');
                    dynamic_sidebar('home_bottom_fluid');
                }
                ?>
            </div>
        </div>
</main>
<?php
get_footer();
?>