<?php
/*
 * @Author       : ZengHao
 * @Date         : 2024-01-09 22:56:54
 * @LastEditors  : ZengHao
 * @LastEditTime : 2024-01-28 20:27:34
 * @FilePath     : /category.php
 * @Description  : 
 * Copyright 2024 www.exehub.com, All Rights Reserved. 
 * 2024-01-09 22:56:54
 */

 get_header();
 ?>
 <main class="el-main container">
		 <div class="row">
			 <div class="col-lg-12 col-md-12 ">
				 <?php
				 if (function_exists('dynamic_sidebar')) {
					dynamic_sidebar('all_top_fluid');
					dynamic_sidebar('cat_top_fluid');
				 }
				 ?>
			 </div>
			 <div class="<?php echo !el_is_show_sidebar()?'col-lg-12':'col-lg-8' ?> main-container">
				 <!-- 首页正上方的小组件 -->
				 <?php
				 if (function_exists('dynamic_sidebar')) {
					dynamic_sidebar('cat_top_content');
				 }
				 ?>
				 <div class="post-loop-default main-post-list">
					 <!-- <div class="axaj-opt"> -->
						 <?php //el_ajax_post_tabs() ?>
					 <!-- </div> -->
					 <ul class="post-ul <?php echo iscard('cat') ?>">
						 <?php el_posts_list(); ?>
					 </ul>
					 <?php el_paging(); ?>
				 </div>
				 <!-- 首页正文下面的小组件 -->
				 <?php
				 if (function_exists('dynamic_sidebar')) {
					dynamic_sidebar('cat_bottom_content');
				 }
				 ?>
			 </div>
			 <div class="col-lg-4 slidebar-container">
				 <?php get_sidebar(); ?>
			 </div>
			 <div class="col-lg-12 bottom-full-container">
				 <?php
				 if (function_exists('dynamic_sidebar')) {
					dynamic_sidebar('cat_bottom_fluid');
					dynamic_sidebar('all_bottom_fluid');
				 }
				 ?>
			 </div>
		 </div>
 </main>
 <?php
 get_footer();
 ?>