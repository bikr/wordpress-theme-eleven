<?php
/*
 * @Author       : ZengHao
 * @Date         : 2023-12-23 14:14:26
 * @LastEditors  : ZengHao
 * @LastEditTime : 2024-02-24 19:26:06
 * @FilePath     : /header.php
 * @Description  : 
 * Copyright 2023 www.exehub.com, All Rights Reserved. 
 * 2023-12-23 14:14:26
 */

?>
<!DOCTYPE HTML>
<html <?php echo 'lang="' . esc_attr(get_bloginfo('language')) . '"'; ?>>
<head>
	<meta charset="UTF-8">
	<link rel="dns-prefetch" href="//apps.bdimg.com">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta http-equiv="Cache-Control" content="no-transform" />
	<meta http-equiv="Cache-Control" content="no-siteapp" />
	<?php el_head();?>
	<?php el_head_css();?>
	<?php wp_head();?>
</head>
<body <?php body_class(_bodyclass());?>>
<?php el_header();?>