/*
 * @Author       : ZengHao
 * @Date         : 2023-12-23 14:14:26
 * @LastEditors  : ZengHao
 * @LastEditTime : 2023-12-24 22:47:32
 * @FilePath     : /readme.txt
 * @Description  : 
 * Copyright 2023 www.exehub.com, All Rights Reserved. 
 * 2023-12-23 14:14:26
 */

--- 20231212
开始主题的研发

--- 20231220
主题后台框架基本搭建完成

--- 20231224
前台页面的规划

--- 20231231
前台的导航栏、幻灯片组件完成

--- 20240102
探索WordPress缓存机制，将主题设置缓存到数据库中，加快获取，推荐使用：对象缓存插件



